---
openapi: 3.0.3
info:
  title: Redstu Cockpit API
  description: 'API of the Redstu engine Cockpit'
  contact:
    name: Mario Junior
    url: https://www.linkedin.com/in/mariofjunior/
    email: francisco.junior.mario@gmail.com
  version: 1.0.0-SNAPSHOT
servers:
  - url: http://localhost:4849/
    description: Local instance
tags:
- name: State
  description: State management operations
- name: Signals
  description: Signalling operations
- name: Server
  description: Server related endpoints
- name: Project
  description: Project related endpoints
paths:
  /project:
    get:
      summary: Gets project info
      description: Gets the current project information
      operationId: GetProjectInfo
      tags:
        - Project
      security:
        - AccessKey: []
      responses:
        '401':
          description: Unauthorized
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Problem'
              examples:
                InternalError:
                  value:
                    type: /problem/unauthorized
                    status: 401
                    title: Request unauthorized
                    detail: API-Key missing
        '403':
          description: Forbidden
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Problem'
              examples:
                InternalError:
                  value:
                    type: /problem/forbidden
                    status: 403
                    title: Access forbidden
                    detail: Invalid API-Key provided
        "200":
          description: Project Info retrieved successfully
          content:
            application/json:
              schema:
                type: object
                properties:
                  name:
                    type: string
                    description: The name of the project
                    example: Lorem Ipsum
                  author:
                    type: object
                    description: Project author information
                    properties:
                      name:
                        type: string
                        description: Name of the author
                        example: John Doe
                      email:
                        type: string
                        description: Email address of the author
                        format: email
                        example: john.doe@domain.com
                  engine:
                      type: string
                      description: The version of the engine being used by the project
                      example: 1.0.0
  /servers/cockpit/security:
    get:
      summary: Gets security scheme of the cockpit
      operationId: GetCockpitSecurity
      tags:
        - Server
      responses:
        "200":
          description: The cockpit server security scheme
          content:
            text/plain:
              schema:
                type: string
                enum:
                  - None
                  - ApiKey

  /servers/mock/responders:
    get:
      summary: Lists the HTTP Responders of the mock server
      description: Gets the list of HTTP Responders on the mock server
      operationId: GetResponders
      tags:
        - Server
      security:
        - AccessKey: []
      responses:
        "200":
          description: Responders retrieved successfully
          content:
            application/json:
              schema:
                type: array
                items:
                  type: object
                  properties:
                    verb:
                      type: string
                      description: The HTTP Verb that the responder answers
                      enum: [GET, POST, PUT, PATCH, DELETE, HEAD]
                    path:
                      type: string
                      description: The path on which the responder is listenning
                      example: /person/{id}
                  required:
                    - verb
                    - path
        '401':
          description: Unauthorized
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Problem'
              examples:
                InternalError:
                  value:
                    type: /problem/unauthorized
                    status: 401
                    title: Request unauthorized
                    detail: API-Key missing
        '403':
          description: Forbidden
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Problem'
              examples:
                InternalError:
                  value:
                    type: /problem/forbidden
                    status: 403
                    title: Access forbidden
                    detail: Invalid API-Key provided
        '500':
          description: Internal server error
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Problem'
              examples:
                InternalError:
                  value:
                    type: /problem/internal
                    status: 500
                    title: Internal server error
                    detail: The engine encountered an internal problem.
  /signals:
    get:
      tags:
        - Signals
      summary: Gets signals List
      description: Lists all the signals supported by the mock server
      operationId: GetSignals
      security:
        - AccessKey: []
      responses:
        "200":
          description: Signals retrieved successfully
          content:
            application/json:
              schema:
                type: array
                items:
                  type: object
                  properties:
                    name:
                      type: string
                      description: The simple name used to refer to the signal
                      example: Lorem
                    description:
                      type: string
                      description: Presents the goal/purpose of the signal
                      example: Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis at tincidunt tellus
                    parameterized:
                      type: boolean
                      description: Tells whether a signal expects a parameter or not
                      example: true
                  required:
                    - name
                    - parameterized
        '401':
          description: Unauthorized
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Problem'
              examples:
                InternalError:
                  value:
                    type: /problem/unauthorized
                    status: 401
                    title: Request unauthorized
                    detail: API-Key missing
        '403':
          description: Forbidden
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Problem'
              examples:
                InternalError:
                  value:
                    type: /problem/forbidden
                    status: 403
                    title: Access forbidden
                    detail: Invalid API-Key provided
        '500':
          description: Internal server error
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Problem'
              examples:
                InternalError:
                  value:
                    type: /problem/internal
                    status: 500
                    title: Internal server error
                    detail: The engine encountered an internal problem.

  /signals/{name}:
    post:
      tags:
        - Signals
      summary: Emits a signal
      description: Emits a signal to the mock project
      operationId: EmitSignal
      security:
        - AccessKey: []
      parameters:
        - in: path
          name: name
          schema:
            type: string
            example: Lorem
          required: true
          description: The name of the signal to emit
      requestBody:
        content:
          application/json:
            schema:
              type: object
              description: The signal parameter object
      responses:
        "200":
          description: Signal emitted successfully
          content:
            application/json:
              schema:
                type: object
        '401':
          description: Unauthorized
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Problem'
              examples:
                InternalError:
                  value:
                    type: /problem/unauthorized
                    status: 401
                    title: Request unauthorized
                    detail: API-Key missing
        '403':
          description: Forbidden
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Problem'
              examples:
                InternalError:
                  value:
                    type: /problem/forbidden
                    status: 403
                    title: Access forbidden
                    detail: Invalid API-Key provided
        '500':
          description: Internal server error
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Problem'
              examples:
                InternalError:
                  value:
                    type: /problem/internal
                    status: 500
                    title: Internal server error
                    detail: The engine encountered an internal problem.
        '404':
          description: Unknown signal
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Problem'
              examples:
                SignalNotFound:
                  value:
                    type: /problem/signal/not-found
                    status: 404
                    title: Signal not found
                    detail: The is no signal with the provided name
  /state:
    get:
      tags:
        - State
      summary: Get the current state
      description: Gets the current mock project in-memory state
      operationId: GetState
      security:
        - AccessKey: []
      responses:
        "200":
          description: State retrieved successfully
          content:
            application/json:
              schema:
                type: object
        '401':
          description: Unauthorized
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Problem'
              examples:
                InternalError:
                  value:
                    type: /problem/unauthorized
                    status: 401
                    title: Request unauthorized
                    detail: API-Key missing
        '403':
          description: Forbidden
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Problem'
              examples:
                InternalError:
                  value:
                    type: /problem/forbidden
                    status: 403
                    title: Access forbidden
                    detail: Invalid API-Key provided
        '500':
          description: Internal server error
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Problem'
              examples:
                InternalError:
                  value:
                    type: /problem/internal
                    status: 500
                    title: Internal server error
                    detail: The engine encountered an internal problem.
    put:
      tags:
      - State
      summary: Set/Update state
      description: Updates/overrides the current mock project in-memory state
      operationId: SetState
      security:
        - AccessKey: []
      requestBody:
        content:
          application/json:
            schema:
              type: object
      responses:
        "200":
          description: State successfully set
        '401':
          description: Unauthorized
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Problem'
              examples:
                InternalError:
                  value:
                    type: /problem/unauthorized
                    status: 401
                    title: Request unauthorized
                    detail: API-Key missing
        '403':
          description: Forbidden
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Problem'
              examples:
                InternalError:
                  value:
                    type: /problem/forbidden
                    status: 403
                    title: Access forbidden
                    detail: Invalid API-Key provided
        '500':
          description: Internal server error
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Problem'
              examples:
                InternalError:
                  value:
                    type: /problem/internal
                    status: 500
                    title: Internal server error
                    detail: The engine encountered an internal problem.

components:
  securitySchemes:
    AccessKey:
      type: apiKey
      in: header
      name: key
  schemas:
    Problem:
      type: object
      description: Represents a system problem
      properties:
        type:
          type: string
          format: uri
          description: |
            An absolute URI that identifies the problem type.  When dereferenced,
            it SHOULD provide human-readable documentation for the problem type
            (e.g., using HTML).
          default: 'about:blank'
          example: 'https://zalando.github.io/problem/constraint-violation'
        title:
          type: string
          description: |
            A short, summary of the problem type. Written in english and readable
            for engineers (usually not suited for non technical stakeholders and
            not localized); example: Service Unavailable
        status:
          type: integer
          format: int32
          description: |
            The HTTP status code generated by the origin server for this occurrence
            of the problem.
          minimum: 100
          maximum: 600
          exclusiveMaximum: true
          example: 503
        detail:
          type: string
          description: |
            A human readable explanation specific to this occurrence of the
            problem.
          example: Connection to database timed out
        instance:
          type: string
          format: uri
          description: |
            An absolute URI that identifies the specific occurrence of the problem.
            It may or may not yield further information if dereferenced.
