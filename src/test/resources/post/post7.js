import { Respond } from 'redstu.js'

Respond.POST("/mock/post7", async (req, resp) => {
    try {
        let form = await req.body.form();
        let attachments = form["attachments"];
        let filesArray = attachments.array.flatMap(value => {
            let length = value.binary().size;
            let filename = value.filename;
            return {filename, length};
        });
        resp.sendArray(filesArray);
    }catch (e) {
        console.error(e);
    }
});


