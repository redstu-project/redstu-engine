import { Respond } from 'redstu.js'

Respond.POST("/mock/post8", async (req, resp) => {
    try {
        let binary = await req.body.binary();
        resp.sendText(binary.size.toString());
        console.log("all good here");
    }catch (e) {
        console.error(e);
    }
});