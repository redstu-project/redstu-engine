import { Respond } from 'redstu.js'

Respond.POST("/mock/post9",async (req, resp) => {
    try {
        let object = await req.body.object();
        resp.sendText(object.contact.email);
    }catch (e) {
        e.printStackTrace();
    }
});