import { Respond } from 'redstu.js'

Respond.POST("/mock/post10",async (req, resp) => {
    let object = await req.body.object();
    resp.sendText(object.contact.phone);
});