import { Respond, MediaTypes } from 'redstu.js'

Respond.GET("/mock/get1",((req, resp) => {
    resp.setStatus(200);
    resp.setContentType(MediaTypes.TEXT_PLAIN);
    resp.sendText("Hello world");
}));