package org.mock.redstu.api.oas3;

import io.restassured.http.Header;
import io.restassured.path.xml.element.Node;
import io.restassured.path.xml.element.PathElement;
import io.restassured.response.Response;
import io.restassured.response.ValidatableResponse;
import io.restassured.specification.RequestSpecification;
import org.apache.commons.lang3.StringUtils;
import org.hamcrest.BaseMatcher;
import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mock.redstu.api.MediaTypes;
import org.mock.redstu.api.Respond;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.UUID;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.notNullValue;

class SubmitFurnitureXmlFromWebTest extends AbstractSubmitFurnitureTest {

    private final String content;

    public SubmitFurnitureXmlFromWebTest() throws IOException {
        String filename = super.getYamlSource();
        this.content = Files.readString(Path.of(System.getProperty("project.dir.path"),filename));
    }

    @Test
    @Override
    void server_must_return_400_due_to_body_fields_input_violations() throws IOException{
        RequestSpecification req = given()
                .body(readFile("scenario-3"))
                .accept(getContentType())
                .contentType(getContentType())
                .cookies(newDeviceCookie(UUID.randomUUID().toString()))
                .header(new Header("authorization", newBearerToken()));

        if (isTraceEnabled()) {
            print(req);
        }

        ValidatableResponse resp = req.put("/houses/neighbor/rooms/9/content/TV")
                .then();

        if (isDebugEnabled() || isTraceEnabled()) {
            resp = print(resp);
        }

        resp.statusCode(400)
                .contentType(getProblemContentType())
                .body(getProblemStatusProperty(), equalTo(toResponseValue(400)))
                .body(getProblemTitleProperty(), equalTo(toResponseValue(CONSTRAINT_VIOLATION_TITLE)))
                .body(getProblemTypeProperty(), equalTo(toResponseValue(CONSTRAINT_VIOLATION_TYPE)))
                .body(getProblemDetailProperty(), equalTo(toResponseValue(CONSTRAINT_VIOLATION_DETAIL)))
                .body(getProblemCauseProperty(), notNullValue())
                .body(getProblemCausePropertySize(), equalTo(11))
                .body(getProblemCausePropertyAt(0), asCause("name", ReqPart.Body, OAS3Violations.MINIMUM_SIZE_VIOLATION_FOR_FIELD))
                .body(getProblemCausePropertyAt(1), asCause("weight", ReqPart.Body, OAS3Violations.INVALID_INPUT_FOR_DOUBLE_FIELD))
                .body(getProblemCausePropertyAt(2), asCause("height", ReqPart.Body, OAS3Violations.INVALID_INPUT_FOR_DOUBLE_FIELD))
                .body(getProblemCausePropertyAt(3), asCause("width", ReqPart.Body, OAS3Violations.INVALID_INPUT_FOR_DOUBLE_FIELD))
                .body(getProblemCausePropertyAt(4), asCause("cost", ReqPart.Body, OAS3Violations.INVALID_INPUT_FOR_FIELD))
                .body(getProblemCausePropertyAt(5), asCause("provider.id", ReqPart.Body, OAS3Violations.INVALID_INPUT_FOR_XML_ATTRIBUTE))
                .body(getProblemCausePropertyAt(6), asCause("bought", ReqPart.Body, OAS3Violations.INVALID_INPUT_FOR_BOOLEAN_FIELD))
                .body(getProblemCausePropertyAt(7), asCause("colors", ReqPart.Body, OAS3Violations.INVALID_INPUT_FOR_ARRAY_FIELD))
                .body(getProblemCausePropertyAt(8), asCause("brand", ReqPart.Body, OAS3Violations.INVALID_INPUT_FOR_FIELD))
                .body(getProblemCausePropertyAt(9), asCause("barcode", ReqPart.Body, OAS3Violations.INVALID_INPUT_FOR_XML_FIELD))
                .body(getProblemCausePropertyAt(10), asCause("additional_information", ReqPart.Body, OAS3Violations.INVALID_INPUT_FOR_FIELD));
    }

    @Test
    void server_must_return_400_due_to_body_multiple_violations_third() throws IOException {
        RequestSpecification req = given()
                .body(readFile("scenario-6"))
                .accept(getContentType())
                .contentType(getContentType())
                .cookies(newDeviceCookie(UUID.randomUUID().toString()))
                .header(new Header("authorization", newBearerToken()));

        if (isTraceEnabled()) {
            print(req);
        }

        ValidatableResponse resp = req.put("/houses/neighbor/rooms/9/content/TV")
                .then();

        if (isDebugEnabled() || isTraceEnabled()) {
            resp = print(resp);
        }

        resp.statusCode(400)
                .contentType(getProblemContentType())
                .body(getProblemStatusProperty(), equalTo(toResponseValue(400)))
                .body(getProblemTitleProperty(), equalTo(toResponseValue(CONSTRAINT_VIOLATION_TITLE)))
                .body(getProblemTypeProperty(), equalTo(toResponseValue(CONSTRAINT_VIOLATION_TYPE)))
                .body(getProblemDetailProperty(), equalTo(toResponseValue(CONSTRAINT_VIOLATION_DETAIL)))
                .body(getProblemCauseProperty(), notNullValue())
                .body(getProblemCausePropertySize(), equalTo(12))
                .body(getProblemCausePropertyAt(0), asCause("name", ReqPart.Body, OAS3Violations.MAXIMUM_SIZE_VIOLATION_FOR_FIELD))
                .body(getProblemCausePropertyAt(1), asCause("weight", ReqPart.Body, OAS3Violations.MAXIMUM_LIMIT_VIOLATION_FOR_FIELD))
                .body(getProblemCausePropertyAt(2), asCause("height", ReqPart.Body, OAS3Violations.MAXIMUM_LIMIT_VIOLATION_FOR_FIELD))
                .body(getProblemCausePropertyAt(3), asCause("width", ReqPart.Body, OAS3Violations.MINIMUM_LIMIT_VIOLATION_FOR_FIELD))
                .body(getProblemCausePropertyAt(4), asCause("cost.amount", ReqPart.Body, OAS3Violations.MAXIMUM_LIMIT_VIOLATION_FOR_FIELD))
                .body(getProblemCausePropertyAt(5), asCause("cost.currency", ReqPart.Body, OAS3Violations.MISSING_INPUT_FOR_REQUIRED_FIELD))
                .body(getProblemCausePropertyAt(6), asCause("provider", ReqPart.Body, OAS3Violations.MINIMUM_SIZE_VIOLATION_FOR_FIELD))
                .body(getProblemCausePropertyAt(7), asCause("provider.id", ReqPart.Body, OAS3Violations.MISSING_INPUT_FOR_REQUIRED_FIELD))
                .body(getProblemCausePropertyAt(8), asCause("bought", ReqPart.Body, OAS3Violations.MISSING_INPUT_FOR_REQUIRED_FIELD))
                .body(getProblemCausePropertyAt(9), asCause("colors", ReqPart.Body, OAS3Violations.INVALID_INPUT_FOR_XML_WRAPPED_ARRAY_NAME_FIELD))
                .body(getProblemCausePropertyAt(10), asCause("brand.id", ReqPart.Body, OAS3Violations.MISSING_INPUT_FOR_REQUIRED_FIELD))
                .body(getProblemCausePropertyAt(11), asCause("barcode", ReqPart.Body, OAS3Violations.MINIMUM_SIZE_VIOLATION_FOR_FIELD));
    }

    @Override
    protected String getProblemStatusProperty() {
        return problemPathOf(super.getProblemStatusProperty());
    }

    @Override
    protected String getProblemTitleProperty() {
        return problemPathOf(super.getProblemTitleProperty());
    }

    @Override
    protected String getProblemTypeProperty() {
        return problemPathOf(super.getProblemTypeProperty());
    }

    @Override
    protected String getProblemDetailProperty() {
        return problemPathOf(super.getProblemDetailProperty());
    }

    @Override
    protected String getProblemCauseProperty() {
        return problemPathOf(super.getProblemCauseProperty());
    }

    @Override
    protected String getProblemCausePropertySize() {
        return getProblemCauseProperty() + ".children().size()";
    }

    @Override
    protected String getProblemCausePropertyAt(int index) {
        return getProblemCauseProperty()+".item["+index+"]";
    }

    @Override
    protected Matcher<?> asCause(String name, ReqPart part, OAS3Violations violation) {
        return new BaseMatcher<Object>() {

            private Object object;

            @Override
            public boolean matches(Object actual) {

                this.object = actual;

                if (!(actual instanceof PathElement)) {
                    return false;
                }

                PathElement xml = (PathElement) actual;

                String value = getValue(xml,"key");

                if (name == null && value != null) {
                    return false;
                }

                if (!StringUtils.equals(name, value)) {
                    return false;
                }

                value = getValue(xml,"part");

                if (part == null && value != null) {
                    return false;
                }

                if (!StringUtils.equals(part == null ? null : part.name, value)) {
                    return false;
                }

                value = getValue(xml,"message");
                return StringUtils.equals(violation == null ? null : violation.message, value);
            }

            @Override
            public void describeTo(Description description) {

                if (this.object == null) {
                    description.appendValue("{key:null, part:null, message:null }");
                    return;
                }

                if (!(this.object instanceof PathElement)) {
                    description.appendText("Invalid Matcher input:" + object.getClass());
                    return;
                }

                Node xml = (Node) this.object;
                description.appendText("{key:" + getValue(xml,"key") + " , part:" + getValue(xml,"part") + " , message:" + getValue(xml,"message") + " }");
            }

            protected String getValue(PathElement element, String path) {

                if (element == null || path == null) {
                    return null;
                }

                Node node = element.getNode(path);

                if (node == null) {
                    return null;
                }

                return node.value();
            }

        };
    }

    @Override
    protected Object toResponseValue(Object value) {
        return value.toString();
    }

    @Override
    @BeforeEach
    void afterPropertiesSet() throws IOException {

        Respond.getRequest("/test/specification.yaml", (req, resp) -> {
            resp.setStatus(200);
            resp.setContentType("application/x-yaml");
            resp.sendText(content);
        });

        super.afterPropertiesSet();
    }

    @Override
    protected String getYamlSource() {
        return "http://localhost:8085/test/specification.yaml";
    }

    @Override
    protected String getContentType() {
        return MediaTypes.APPLICATION_XML;
    }

    @Override
    protected String getProblemContentType() {
        return MediaTypes.APPLICATION_PROBLEM_XML;
    }

    @Override
    protected void withStatus200(Response response) {
        response.then().assertThat()
                .body("Forniture.@id", equalTo("c72192a6-801f-4728-a8f5-6d18d503b155"))
                .body("Forniture.@name", equalTo("ASHOMELI White Comforter for King Bed"))
                .body("Forniture.@type", equalTo("BED"))
                .body("Forniture.@width", equalTo("2"))
                .body("Forniture.@weight", equalTo("4"))
                .body("Forniture.@height", equalTo("0.5"))
                //.body("@cost.size()", equalTo(2))
                .body("Forniture.cost.@amount", equalTo("120"))
                .body("Forniture.cost.@currency", equalTo("USD"))
                //.body("provider.size()", equalTo(3))
                .body("Forniture.provider.@id", equalTo("1821d410-7078-4ac5-bea9-2e046fb3e702"))
                .body("Forniture.provider.@name", equalTo("AMAZON"))
                .body("Forniture.provider.@thumbnail", equalTo("https://1000marche.net/wp-content/uploads/2020/03/Amazon-logo.png"))
                .body("Forniture.@bought", equalTo("true"))
                .body("Forniture.colors.size()", equalTo(1))
                .body("Forniture.colors[0].color", equalTo("RED"))
                //.body("brand.size()", equalTo(4))
                .body("Forniture.brand.@id", equalTo("1ca007db-b7ff-46d6-882b-77c87bff2569"))
                .body("Forniture.brand.@name", equalTo("ASHOMELI"))
                .body("Forniture.brand.@since", equalTo("2020"))
                .body("Forniture.brand.@authorized", equalTo("true"))
                .body("Forniture.barcode", equalTo("A-0010-Z"));
    }

    @Override
    protected String readFile(String filename) throws IOException {

        if(filename!=null && filename.equals("scenario-2")){
            return "<Forniture/>";
        }

        return super.readFile(filename);
    }

    @Override
    protected String getReadFileExtension() {
        return "xml";
    }

    @Override
    protected boolean isDebugEnabled() {
        return false;
    }

    protected String problemPathOf(String property) {

        if (property == null) {
            return null;
        }

        return "Problem." + property;
    }

}
