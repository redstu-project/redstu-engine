package org.mock.redstu.api.oas3;

import com.fasterxml.jackson.databind.node.ObjectNode;
import io.swagger.v3.core.util.Json;
import org.mock.redstu.api.MediaTypes;

class SubmitFurnitureJsonFromFileTest extends AbstractSubmitFurnitureTest {

    @Override
    protected String getContentType() {
        return MediaTypes.APPLICATION_JSON;
    }

    @Override
    protected String getProblemContentType() {
        return MediaTypes.APPLICATION_PROBLEM_JSON;
    }

    @Override
    protected boolean isDebugEnabled() {
        return false;
    }

}
