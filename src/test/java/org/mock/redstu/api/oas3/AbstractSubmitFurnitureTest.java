package org.mock.redstu.api.oas3;

import http.BaseHttpRequestTest;
import io.restassured.filter.log.RequestLoggingFilter;
import io.restassured.filter.log.ResponseLoggingFilter;
import io.restassured.http.Cookie;
import io.restassured.http.Cookies;
import io.restassured.http.Header;
import io.restassured.response.Response;
import io.restassured.response.ValidatableResponse;
import io.restassured.specification.RequestSpecification;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.io.IoBuilder;
import org.hamcrest.Matcher;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mock.redstu.api.MediaTypes;
import org.mock.redstu.api.OAS3;
import org.mock.redstu.api.Respond;
import org.mock.redstu.api.oas3.annotations.Endpoint;
import org.mock.redstu.server.Redish;

import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import java.nio.charset.Charset;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.notNullValue;

@Endpoint(path = "/houses/{id}/rooms/{number}/content/{item}", operation = "put-content-into-house-room")
abstract class AbstractSubmitFurnitureTest extends BaseHttpRequestTest {

    protected static final String ID_PROPERTY = "id";

    protected static final String TYPE_PROPERTY = "type";
    protected static final String TITLE_PROPERTY = "title";
    protected static final String CAUSE_PROPERTY = "cause";
    protected static final String STATUS_PROPERTY = "status";
    protected static final String DETAIL_PROPERTY = "detail";

    protected static final String CONSTRAINT_VIOLATION_TYPE = "/extensions/oas3/problems";
    protected static final String CONSTRAINT_VIOLATION_TITLE = "OAS3 CONSTRAINT(S) VIOLATION(S) DISCOVERED";
    protected static final String CONSTRAINT_VIOLATION_DETAIL = "HTTP Request doesn't comply with the OAS3 specification";

    private static Redish server;
    private final Logger LOGGER = LogManager.getLogger(this.getClass());


    @BeforeAll
    static void initialize() {
        server = BaseHttpRequestTest.createServer();
        server.setCurrent();
        server.start();
    }

    @BeforeEach
    void afterPropertiesSet() throws IOException {

        APIDefinition spec = OAS3.spec(getYamlSource());

        Respond.putRequest(getEndpoint(), ((req, resp) -> {

            APIOperation operation = spec.getOperations().get(getOperation());

            if (operation == null) {
                throw new IllegalStateException("Cannot find operation");
            }

            req.body().binary((content, exception) -> {

                ValidationResult result = operation.validate(req, content);

                if (exception != null) {
                    resp.setStatus(500);
                    resp.setContentType("application/problem");
                } else if (result.anyErrors()) {
                    result.sendErrors(req, resp);
                } else {
                    operation.sendExample(200, "default", req, resp);
                }

            });

        }));

    }

    @AfterAll
    static void teardown() {
        if (server.isActive())
            server.stop();
    }

    protected String getYamlSource() {

        if (System.getProperty("project.dir.path") == null) {
            System.setProperty("project.dir.path", Paths.get("src", "test", "resources", "oas3").toFile().getAbsolutePath());
        }

        return "specification.yaml";
    }

    protected String getEndpoint() {
        return Optional.ofNullable(this.getClass().getAnnotation(Endpoint.class))
                .map(Endpoint::path).filter(each -> !each.isEmpty()).orElse("/");
    }

    protected String getOperation() {
        return Optional.ofNullable(this.getClass().getAnnotation(Endpoint.class)).map(Endpoint::operation)
                .filter(each -> !each.isEmpty()).orElseThrow(() -> new IllegalStateException("Cannot identify operation"));
    }

    protected String getUnsupportedMediaType() {
        return MediaTypes.APPLICATION_PROBLEM_JSON;
    }

    protected boolean isDebugEnabled() {
        return false;
    }

    protected boolean isTraceEnabled() {
        return false;
    }

    /**
     * Submits a fully compliant HTTP Request and expect an 204 HTTP Response
     */
    @Test
    void server_must_return_200() throws IOException {
        RequestSpecification req = given()
                .body(readFile("scenario-1"))
                .accept(getContentType())
                .contentType(getContentType())
                .cookies(newDeviceCookie(UUID.randomUUID().toString()))
                .header(new Header("authorization", newBearerToken()));

        if (isTraceEnabled()) {
            print(req);
        }

        Response response = req.put("/houses/neighbor/rooms/1/content/TV");

        ValidatableResponse resp = response.then();

        if (isDebugEnabled() || isTraceEnabled()) {
            resp = print(resp);
        }

        resp.statusCode(200).contentType(getContentType());

        withStatus200(response);
    }

    protected void withStatus200(Response response) {
        response.then().body(ID_PROPERTY, equalTo(toResponseValue("c72192a6-801f-4728-a8f5-6d18d503b155")))
                .body("name", equalTo(toResponseValue("ASHOMELI White Comforter for King Bed")))
                .body("type", equalTo(toResponseValue("BED")))
                .body("width", equalTo(toResponseValue(2)))
                .body("weight", equalTo(toResponseValue(4)))
                .body("height", equalTo(toResponseValue(0.5)))
                .body("cost.size()", equalTo(toResponseValue(2)))
                .body("cost.amount", equalTo(toResponseValue(120)))
                .body("cost.currency", equalTo(toResponseValue("USD")))
                .body("provider.size()", equalTo(toResponseValue(3)))
                .body("provider.id", equalTo(toResponseValue("1821d410-7078-4ac5-bea9-2e046fb3e702")))
                .body("provider.name", equalTo(toResponseValue("AMAZON")))
                .body("provider.thumbnail", equalTo(toResponseValue("https://1000marche.net/wp-content/uploads/2020/03/Amazon-logo.png")))
                .body("bought", equalTo(toResponseValue(true)))
                .body("colors", hasSize(1))
                .body("colors[0]", equalTo(toResponseValue("RED")))
                .body("brand.size()", equalTo(toResponseValue(4)))
                .body("brand.id", equalTo(toResponseValue("1ca007db-b7ff-46d6-882b-77c87bff2569")))
                .body("brand.name", equalTo(toResponseValue("ASHOMELI")))
                .body("brand.since", equalTo(toResponseValue(2020)))
                .body("brand.authorized", equalTo(toResponseValue(true)))
                .body("barcode", equalTo(toResponseValue("A-0010-Z")))
                .body("size()", equalTo(toResponseValue(12)));
    }

    /**
     * Submits a fully compliant HTTP Request and expect an 204 HTTP Response
     */
    @Test
    void server_must_return_400_due_to_empty_authorization_header() throws IOException {
        RequestSpecification req = given()
                .body(readFile("scenario-1"))
                .accept(getContentType())
                .contentType(getContentType())
                .cookies(newDeviceCookie(UUID.randomUUID().toString()))
                .header(new Header("authorization", null));

        if (isTraceEnabled()) {
            print(req);
        }

        ValidatableResponse resp = req.put("/houses/neighbor/rooms/1/content/TV").then();

        if (isDebugEnabled() || isTraceEnabled()) {
            resp = print(resp);
        }

        resp.statusCode(400)
                .contentType(getProblemContentType())
                .body(getProblemStatusProperty(), equalTo(toResponseValue(400)))
                .body(getProblemTitleProperty(), equalTo(toResponseValue(CONSTRAINT_VIOLATION_TITLE)))
                .body(getProblemTypeProperty(), equalTo(toResponseValue(CONSTRAINT_VIOLATION_TYPE)))
                .body(getProblemDetailProperty(), equalTo(toResponseValue(CONSTRAINT_VIOLATION_DETAIL)))
                .body(getProblemCauseProperty(), notNullValue())
                .body(getProblemCausePropertySize(), equalTo(1))
                .body(getProblemCausePropertyAt(0), asCause("authorization", ReqPart.Header, OAS3Violations.MISSING_INPUT_FOR_REQUIRED_FIELD));
    }

    /**
     * Submits an HTTP Request without device cookie and expect an 400 HTTP Response
     */
    @Test
    void server_must_return_400_due_to_missing_device_cookie() throws IOException {
        RequestSpecification req = given()
                .body(readFile("scenario-1"))
                .accept(getContentType())
                .contentType(getContentType())
                .header(new Header("authorization", newBearerToken()));

        if (isTraceEnabled()) {
            print(req);
        }

        ValidatableResponse resp = req.put("/houses/neighbor/rooms/1/content/TV")
                .then();

        if (isDebugEnabled() || isTraceEnabled()) {
            resp = print(resp);
        }

        resp.statusCode(400)
                .contentType(getProblemContentType())
                .body(getProblemStatusProperty(), equalTo(toResponseValue(400)))
                .body(getProblemTitleProperty(), equalTo(toResponseValue(CONSTRAINT_VIOLATION_TITLE)))
                .body(getProblemTypeProperty(), equalTo(toResponseValue(CONSTRAINT_VIOLATION_TYPE)))
                .body(getProblemDetailProperty(), equalTo(toResponseValue(CONSTRAINT_VIOLATION_DETAIL)))
                .body(getProblemCauseProperty(), notNullValue())
                .body(getProblemCausePropertySize(), equalTo(1))
                .body(getProblemCausePropertyAt(0), asCause("device", ReqPart.Cookie, OAS3Violations.MISSING_INPUT_FOR_REQUIRED_FIELD));
    }

    /**
     * Submits an HTTP Request without authorization header and expect an 400 HTTP Response
     */
    @Test
    void server_must_return_400_due_to_missing_authorization_header() throws IOException {
        RequestSpecification req = given()
                .body(readFile("scenario-1"))
                .accept(getContentType())
                .contentType(getContentType())
                .cookies(newDeviceCookie(UUID.randomUUID().toString()));

        if (isTraceEnabled()) {
            print(req);
        }

        ValidatableResponse resp = req.put("/houses/neighbor/rooms/1/content/TV")
                .then();

        if (isDebugEnabled() || isTraceEnabled()) {
            resp = print(resp);
        }

        resp.statusCode(400)
                .contentType(getProblemContentType())
                .body(getProblemStatusProperty(), equalTo(toResponseValue(400)))
                .body(getProblemTitleProperty(), equalTo(toResponseValue(CONSTRAINT_VIOLATION_TITLE)))
                .body(getProblemTypeProperty(), equalTo(toResponseValue(CONSTRAINT_VIOLATION_TYPE)))
                .body(getProblemDetailProperty(), equalTo(toResponseValue(CONSTRAINT_VIOLATION_DETAIL)))
                .body(getProblemCauseProperty(), notNullValue())
                .body(getProblemCausePropertySize(), equalTo(1))
                .body(getProblemCausePropertyAt(0), asCause("authorization", ReqPart.Header, OAS3Violations.MISSING_INPUT_FOR_REQUIRED_FIELD));
    }

    /**
     * Submits an HTTP Request without authorization header and expect an 400 HTTP Response
     */
    @Test
    void server_must_return_400_due_to_missing_authorization_header_and_device_cookie() throws IOException {
        RequestSpecification req = given()
                .body(readFile("scenario-1"))
                .accept(getContentType())
                .contentType(getContentType());

        if (isTraceEnabled()) {
            print(req);
        }

        ValidatableResponse resp = req.put("/houses/neighbor/rooms/1/content/TV")
                .then();

        if (isDebugEnabled() || isTraceEnabled()) {
            resp = print(resp);
        }

        resp.statusCode(400)
                .contentType(getProblemContentType())
                .body(getProblemStatusProperty(), equalTo(toResponseValue(400)))
                .body(getProblemTitleProperty(), equalTo(toResponseValue(CONSTRAINT_VIOLATION_TITLE)))
                .body(getProblemTypeProperty(), equalTo(toResponseValue(CONSTRAINT_VIOLATION_TYPE)))
                .body(getProblemDetailProperty(), equalTo(toResponseValue(CONSTRAINT_VIOLATION_DETAIL)))
                .body(getProblemCauseProperty(), notNullValue())
                .body(getProblemCausePropertySize(), equalTo(2))
                .body(getProblemCausePropertyAt(0), asCause("device", ReqPart.Cookie, OAS3Violations.MISSING_INPUT_FOR_REQUIRED_FIELD))
                .body(getProblemCausePropertyAt(1), asCause("authorization", ReqPart.Header, OAS3Violations.MISSING_INPUT_FOR_REQUIRED_FIELD))
        ;
    }

    /**
     * Submits an HTTP Request with invalid room number 'A' and item 'TV' expect an 400 HTTP Response
     */
    @Test
    void server_must_return_400_due_to_invalid_room_number_and_item() throws IOException {
        RequestSpecification req = given()
                .body(readFile("scenario-1"))
                .accept(getContentType())
                .contentType(getContentType())
                .cookies(newDeviceCookie(UUID.randomUUID().toString()))
                .header(new Header("authorization", newBearerToken()));

        if (isTraceEnabled()) {
            print(req);
        }

        ValidatableResponse resp = req.put("/houses/neighbor/rooms/A/content/LK")
                .then();

        if (isDebugEnabled() || isTraceEnabled()) {
            resp = print(resp);
        }

        resp.statusCode(400)
                .contentType(getProblemContentType())
                .body(getProblemStatusProperty(), equalTo(toResponseValue(400)))
                .body(getProblemTitleProperty(), equalTo(toResponseValue(CONSTRAINT_VIOLATION_TITLE)))
                .body(getProblemTypeProperty(), equalTo(toResponseValue(CONSTRAINT_VIOLATION_TYPE)))
                .body(getProblemDetailProperty(), equalTo(toResponseValue(CONSTRAINT_VIOLATION_DETAIL)))
                .body(getProblemCauseProperty(), notNullValue())
                .body(getProblemCausePropertySize(), equalTo(2))
                .body(getProblemCausePropertyAt(1), asCause("item", ReqPart.Path, OAS3Violations.INVALID_INPUT_FOR_ENUM_FIELD))
                .body(getProblemCausePropertyAt(0), asCause("number", ReqPart.Path, OAS3Violations.INVALID_INPUT_FOR_INTEGER_FIELD));
    }

    /**
     * Submits an HTTP Request with invalid room number '0' and version '-1.0' expect an 400 HTTP Response
     */
    @Test
    void server_must_return_400_due_to_minimum_version_violation_and_minimum_number_path_violation() throws IOException {
        RequestSpecification req = given()
                .body(readFile("scenario-1"))
                .accept(getContentType())
                .contentType(getContentType())
                .queryParam("version", "-1.0")
                .cookies(newDeviceCookie(UUID.randomUUID().toString()))
                .header(new Header("authorization", newBearerToken()));

        if (isTraceEnabled()) {
            print(req);
        }

        ValidatableResponse resp = req.put("/houses/neighbor/rooms/0/content/TV")
                .then();

        if (isDebugEnabled() || isTraceEnabled()) {
            resp = print(resp);
        }

        resp.statusCode(400)
                .contentType(getProblemContentType())
                .body(getProblemStatusProperty(), equalTo(toResponseValue(400)))
                .body(getProblemTitleProperty(), equalTo(toResponseValue(CONSTRAINT_VIOLATION_TITLE)))
                .body(getProblemTypeProperty(), equalTo(toResponseValue(CONSTRAINT_VIOLATION_TYPE)))
                .body(getProblemDetailProperty(), equalTo(toResponseValue(CONSTRAINT_VIOLATION_DETAIL)))
                .body(getProblemCauseProperty(), notNullValue())
                .body(getProblemCausePropertySize(), equalTo(2))
                .body(getProblemCausePropertyAt(1), asCause("version", ReqPart.Query, OAS3Violations.MINIMUM_LIMIT_VIOLATION_FOR_FIELD))
                .body(getProblemCausePropertyAt(0), asCause("number", ReqPart.Path, OAS3Violations.MINIMUM_LIMIT_VIOLATION_FOR_FIELD))
        ;
    }

    /**
     * Submits an HTTP Request with invalid header and version '11.0' expect an 400 HTTP Response
     */
    @Test
    void server_must_return_400_due_to_maximum_number_path_violation_and_authorization_header_pattern_violation() throws IOException {
        RequestSpecification req = given()
                .body(readFile("scenario-1"))
                .accept(getContentType())
                .contentType(getContentType())
                .cookies(newDeviceCookie(UUID.randomUUID().toString()))
                .header(new Header("authorization", "Bearer" + UUID.randomUUID()));

        if (isTraceEnabled()) {
            print(req);
        }

        ValidatableResponse resp = req.put("/houses/neighbor/rooms/11/content/TV")
                .then();

        if (isDebugEnabled() || isTraceEnabled()) {
            resp = print(resp);
        }

        resp.statusCode(400)
                .contentType(getProblemContentType())
                .body(getProblemStatusProperty(), equalTo(toResponseValue(400)))
                .body(getProblemTitleProperty(), equalTo(toResponseValue(CONSTRAINT_VIOLATION_TITLE)))
                .body(getProblemTypeProperty(), equalTo(toResponseValue(CONSTRAINT_VIOLATION_TYPE)))
                .body(getProblemDetailProperty(), equalTo(toResponseValue(CONSTRAINT_VIOLATION_DETAIL)))
                .body(getProblemCauseProperty(), notNullValue())
                .body(getProblemCausePropertySize(), equalTo(2))
                .body(getProblemCausePropertyAt(0), asCause("number", ReqPart.Path, OAS3Violations.MAXIMUM_LIMIT_VIOLATION_FOR_FIELD))
                .body(getProblemCausePropertyAt(1), asCause("authorization", ReqPart.Header, OAS3Violations.INVALID_INPUT_FOR_PATTERN_FIELD))
        ;
    }

    /**
     * Submits an HTTP Request with invalid format for number path and expect an 400 HTTP Response
     */
    @Test
    void server_must_return_400_due_to_invalid_format_number_path_violation() throws IOException {
        RequestSpecification req = given()
                .body(readFile("scenario-1"))
                .accept(getContentType())
                .contentType(getContentType())
                .cookies(newDeviceCookie(UUID.randomUUID().toString()))
                .header(new Header("authorization", newBearerToken()));

        if (isTraceEnabled()) {
            print(req);
        }

        ValidatableResponse resp = req.put("/houses/neighbor/rooms/9.0/content/TV")
                .then();

        if (isDebugEnabled() || isTraceEnabled()) {
            resp = print(resp);
        }

        resp.statusCode(400)
                .contentType(getProblemContentType())
                .body(getProblemStatusProperty(), equalTo(toResponseValue(400)))
                .body(getProblemTitleProperty(), equalTo(toResponseValue(CONSTRAINT_VIOLATION_TITLE)))
                .body(getProblemTypeProperty(), equalTo(toResponseValue(CONSTRAINT_VIOLATION_TYPE)))
                .body(getProblemDetailProperty(), equalTo(toResponseValue(CONSTRAINT_VIOLATION_DETAIL)))
                .body(getProblemCauseProperty(), notNullValue())
                .body(getProblemCausePropertySize(), equalTo(1))
                .body(getProblemCausePropertyAt(0), asCause("number", ReqPart.Path, OAS3Violations.INVALID_INPUT_FOR_INTEGER_FIELD))
        ;
    }

    /**
     * Submits an HTTP Request with invalid Content-Type and expect an 400 HTTP Response
     */
    @Test
    void server_must_return_400_due_to_invalid_content_type() throws IOException {
        RequestSpecification req = given()
                .body(readFile("scenario-1"))
                .accept(getContentType())
                .contentType(getUnsupportedMediaType())
                .cookies(newDeviceCookie(UUID.randomUUID().toString()))
                .header(new Header("authorization", newBearerToken()));

        if (isTraceEnabled()) {
            print(req);
        }

        ValidatableResponse resp = req.put("/houses/neighbor/rooms/9/content/TV")
                .then();

        if (isDebugEnabled() || isTraceEnabled()) {
            resp = print(resp);
        }

        resp.statusCode(400)
                .contentType(getProblemContentType())
                .body(getProblemStatusProperty(), equalTo(toResponseValue(400)))
                .body(getProblemTitleProperty(), equalTo(toResponseValue(CONSTRAINT_VIOLATION_TITLE)))
                .body(getProblemTypeProperty(), equalTo(toResponseValue(CONSTRAINT_VIOLATION_TYPE)))
                .body(getProblemDetailProperty(), equalTo(toResponseValue(CONSTRAINT_VIOLATION_DETAIL)))
                .body(getProblemCauseProperty(), notNullValue())
                .body(getProblemCausePropertySize(), equalTo(1))
                .body(getProblemCausePropertyAt(0), asCause(getUnsupportedMediaType(), ReqPart.MediaType,
                        OAS3Violations.INVALID_INPUT_FOR_ENUM_FIELD))
        ;
    }

    /**
     * Submits an HTTP Request with invalid Content-Type and expect an 400 HTTP Response
     */
    @Test
    void server_must_return_400_due_to_empty() {
        RequestSpecification req = given()
                .accept(getContentType())
                .contentType(getContentType())
                .cookies(newDeviceCookie(UUID.randomUUID().toString()))
                .header(new Header("authorization", newBearerToken()));

        if (isTraceEnabled()) {
            print(req);
        }

        ValidatableResponse resp = req.put("/houses/neighbor/rooms/9/content/TV")
                .then();

        if (isDebugEnabled() || isTraceEnabled()) {
            resp = print(resp);
        }

        resp.statusCode(400)
                .contentType(getProblemContentType())
                .body(getProblemStatusProperty(), equalTo(toResponseValue(400)))
                .body(getProblemTitleProperty(), equalTo(toResponseValue(CONSTRAINT_VIOLATION_TITLE)))
                .body(getProblemTypeProperty(), equalTo(toResponseValue(CONSTRAINT_VIOLATION_TYPE)))
                .body(getProblemDetailProperty(), equalTo(toResponseValue(CONSTRAINT_VIOLATION_DETAIL)))
                .body(getProblemCauseProperty(), notNullValue())
                .body(getProblemCausePropertySize(), equalTo(1))
                .body(getProblemCausePropertyAt(0), asCause(ReqPart.Body, OAS3Violations.MISSING_INPUT_FOR_REQUIRED_FIELD))
        ;
    }

    /**
     * Submits an HTTP Request with Empty Request and expect an 400 HTTP Response
     */
    @Test
    void server_must_return_400_due_to_no_fields_getBody() throws IOException {
        RequestSpecification req = given()
                .accept(getContentType())
                .body(readFile("scenario-2"))
                .contentType(getContentType())
                .cookies(newDeviceCookie(UUID.randomUUID().toString()))
                .header(new Header("authorization", newBearerToken()));

        if (isTraceEnabled()) {
            print(req);
        }

        ValidatableResponse resp = req.put("/houses/neighbor/rooms/9/content/TV")
                .then();

        if (isDebugEnabled() || isTraceEnabled()) {
            resp = print(resp);
        }

        resp.statusCode(400)
                .contentType(getProblemContentType())
                .body(getProblemStatusProperty(), equalTo(toResponseValue(400)))
                .body(getProblemTitleProperty(), equalTo(toResponseValue(CONSTRAINT_VIOLATION_TITLE)))
                .body(getProblemTypeProperty(), equalTo(toResponseValue(CONSTRAINT_VIOLATION_TYPE)))
                .body(getProblemDetailProperty(), equalTo(toResponseValue(CONSTRAINT_VIOLATION_DETAIL)))
                .body(getProblemCauseProperty(), notNullValue())
                .body(getProblemCausePropertySize(), equalTo(9))
                .body(getProblemCausePropertyAt(0), asCause("name", ReqPart.Body, OAS3Violations.MISSING_INPUT_FOR_REQUIRED_FIELD))
                .body(getProblemCausePropertyAt(1), asCause("weight", ReqPart.Body, OAS3Violations.MISSING_INPUT_FOR_REQUIRED_FIELD))
                .body(getProblemCausePropertyAt(2), asCause("height", ReqPart.Body, OAS3Violations.MISSING_INPUT_FOR_REQUIRED_FIELD))
                .body(getProblemCausePropertyAt(3), asCause("width", ReqPart.Body, OAS3Violations.MISSING_INPUT_FOR_REQUIRED_FIELD))
                .body(getProblemCausePropertyAt(4), asCause("cost", ReqPart.Body, OAS3Violations.MISSING_INPUT_FOR_REQUIRED_FIELD))
                .body(getProblemCausePropertyAt(5), asCause("provider", ReqPart.Body, OAS3Violations.MISSING_INPUT_FOR_REQUIRED_FIELD))
                .body(getProblemCausePropertyAt(6), asCause("bought", ReqPart.Body, OAS3Violations.MISSING_INPUT_FOR_REQUIRED_FIELD))
                .body(getProblemCausePropertyAt(7), asCause("brand", ReqPart.Body, OAS3Violations.MISSING_INPUT_FOR_REQUIRED_FIELD))
                .body(getProblemCausePropertyAt(8), asCause("barcode", ReqPart.Body, OAS3Violations.MISSING_INPUT_FOR_REQUIRED_FIELD));
    }

    /**
     * Submits an HTTP Request with Values type violation on body  and expect an 400 HTTP Response
     */
    @Test
    void server_must_return_400_due_to_body_fields_input_violations() throws IOException {
        RequestSpecification req = given()
                .body(readFile("scenario-3"))
                .accept(getContentType())
                .contentType(getContentType())
                .cookies(newDeviceCookie(UUID.randomUUID().toString()))
                .header(new Header("authorization", newBearerToken()));

        if (isTraceEnabled()) {
            print(req);
        }

        ValidatableResponse resp = req.put("/houses/neighbor/rooms/9/content/TV")
                .then();

        if (isDebugEnabled() || isTraceEnabled()) {
            resp = print(resp);
        }

        resp.statusCode(400)
                .contentType(getProblemContentType())
                .body(getProblemStatusProperty(), equalTo(toResponseValue(400)))
                .body(getProblemTitleProperty(), equalTo(toResponseValue(CONSTRAINT_VIOLATION_TITLE)))
                .body(getProblemTypeProperty(), equalTo(toResponseValue(CONSTRAINT_VIOLATION_TYPE)))
                .body(getProblemDetailProperty(), equalTo(toResponseValue(CONSTRAINT_VIOLATION_DETAIL)))
                .body(getProblemCauseProperty(), notNullValue())
                .body(getProblemCausePropertySize(), equalTo(11))
                .body(getProblemCausePropertyAt(0), asCause("name", ReqPart.Body, OAS3Violations.INVALID_INPUT_FOR_TEXT_FIELD))
                .body(getProblemCausePropertyAt(1), asCause("weight", ReqPart.Body, OAS3Violations.INVALID_INPUT_FOR_DOUBLE_FIELD))
                .body(getProblemCausePropertyAt(2), asCause("height", ReqPart.Body, OAS3Violations.INVALID_INPUT_FOR_DOUBLE_FIELD))
                .body(getProblemCausePropertyAt(3), asCause("width", ReqPart.Body, OAS3Violations.INVALID_INPUT_FOR_DOUBLE_FIELD))
                .body(getProblemCausePropertyAt(4), asCause("cost", ReqPart.Body, OAS3Violations.INVALID_INPUT_FOR_FIELD))
                .body(getProblemCausePropertyAt(5), asCause("provider.id", ReqPart.Body, OAS3Violations.INVALID_INPUT_FOR_TEXT_FIELD))
                .body(getProblemCausePropertyAt(6), asCause("bought", ReqPart.Body, OAS3Violations.INVALID_INPUT_FOR_BOOLEAN_FIELD))
                .body(getProblemCausePropertyAt(7), asCause("colors", ReqPart.Body, OAS3Violations.INVALID_INPUT_FOR_ARRAY_FIELD))
                .body(getProblemCausePropertyAt(8), asCause("brand", ReqPart.Body, OAS3Violations.INVALID_INPUT_FOR_FIELD))
                .body(getProblemCausePropertyAt(9), asCause("barcode", ReqPart.Body, OAS3Violations.INVALID_INPUT_FOR_TEXT_FIELD))
                .body(getProblemCausePropertyAt(10), asCause("additional_information", ReqPart.Body, OAS3Violations.INVALID_INPUT_FOR_FIELD));
    }

    /**
     * Submits an HTTP Request with Values type violation on body  and expect an 400 HTTP Response
     * name - minimum size violation
     * weight - minimum value violation
     * height - minimum value violation
     * width - maximum value violation
     * bought - missing value violation
     * barcode - maximum size violation
     * cost.amount - minimum value violation
     * cost.currency - unknown value violation
     * brand.name -  disallow additional properties violation
     * brand.id - missing value violation
     * provider.name - disallow additional properties violation
     * additional_information - minimum properties violation
     * colors - minimum size violation
     */
    @Test
    void server_must_return_400_due_to_body_multiple_violations() throws IOException {
        RequestSpecification req = given()
                .body(readFile("scenario-4"))
                .accept(getContentType())
                .contentType(getContentType())
                .cookies(newDeviceCookie(UUID.randomUUID().toString()))
                .header(new Header("authorization", newBearerToken()));

        if (isTraceEnabled()) {
            print(req);
        }

        ValidatableResponse resp = req.put("/houses/neighbor/rooms/9/content/TV")
                .then();

        if (isDebugEnabled() || isTraceEnabled()) {
            resp = print(resp);
        }

        resp.statusCode(400)
                .contentType(getProblemContentType())
                .body(getProblemStatusProperty(), equalTo(toResponseValue(400)))
                .body(getProblemTitleProperty(), equalTo(toResponseValue(CONSTRAINT_VIOLATION_TITLE)))
                .body(getProblemTypeProperty(), equalTo(toResponseValue(CONSTRAINT_VIOLATION_TYPE)))
                .body(getProblemDetailProperty(), equalTo(toResponseValue(CONSTRAINT_VIOLATION_DETAIL)))
                .body(getProblemCauseProperty(), notNullValue())
                .body(getProblemCausePropertySize(), equalTo(13))
                .body(getProblemCausePropertyAt(0), asCause("name", ReqPart.Body, OAS3Violations.MINIMUM_SIZE_VIOLATION_FOR_FIELD))
                .body(getProblemCausePropertyAt(1), asCause("weight", ReqPart.Body, OAS3Violations.MINIMUM_LIMIT_VIOLATION_FOR_FIELD))
                .body(getProblemCausePropertyAt(2), asCause("height", ReqPart.Body, OAS3Violations.MINIMUM_LIMIT_VIOLATION_FOR_FIELD))
                .body(getProblemCausePropertyAt(3), asCause("width", ReqPart.Body, OAS3Violations.MAXIMUM_LIMIT_VIOLATION_FOR_FIELD))
                .body(getProblemCausePropertyAt(4), asCause("cost.amount", ReqPart.Body, OAS3Violations.MINIMUM_LIMIT_VIOLATION_FOR_FIELD))
                .body(getProblemCausePropertyAt(5), asCause("cost.currency", ReqPart.Body, OAS3Violations.INVALID_INPUT_FOR_ENUM_FIELD))
                .body(getProblemCausePropertyAt(6), asCause("provider.name", ReqPart.Body, OAS3Violations.DISALLOWED_PROPERTIES_FOR_FIELD))
                .body(getProblemCausePropertyAt(7), asCause("bought", ReqPart.Body, OAS3Violations.MISSING_INPUT_FOR_REQUIRED_FIELD))
                .body(getProblemCausePropertyAt(8), asCause("colors", ReqPart.Body, OAS3Violations.MINIMUM_SIZE_VIOLATION_FOR_FIELD))
                .body(getProblemCausePropertyAt(9), asCause("brand.id", ReqPart.Body, OAS3Violations.MISSING_INPUT_FOR_REQUIRED_FIELD))
                .body(getProblemCausePropertyAt(10), asCause("brand.name", ReqPart.Body, OAS3Violations.DISALLOWED_PROPERTIES_FOR_FIELD))
                .body(getProblemCausePropertyAt(11), asCause("barcode", ReqPart.Body, OAS3Violations.MAXIMUM_SIZE_VIOLATION_FOR_FIELD))
                .body(getProblemCausePropertyAt(12), asCause("additional_information", ReqPart.Body, OAS3Violations.MINIMUM_SIZE_VIOLATION_FOR_FIELD))
        ;
    }

    /**
     * Submits an HTTP Request with Values type violation on body  and expect an 400 HTTP Response
     * name - maximum size violation
     * weight - maximum value violation
     * height - maximum value violation
     * width - minimum value violation
     * bought - missing value violation
     * barcode - minimum size violation
     * cost.amount - maximum value violation
     * cost.currency - missing value violation
     * brand.id - missing value violation
     * provider - minimum properties violation
     * colors - maximum size violation
     * colors[1] - violate unique value definition
     * colors[3] - violate unique value definition
     * colors[0] - enum value violation
     * colors[2] - enum value violation
     */
    @Test
    void server_must_return_400_due_to_body_multiple_violations_second() throws IOException {
        RequestSpecification req = given()
                .body(readFile("scenario-5"))
                .accept(getContentType())
                .contentType(getContentType())
                .cookies(newDeviceCookie(UUID.randomUUID().toString()))
                .header(new Header("authorization", newBearerToken()));

        if (isTraceEnabled()) {
            print(req);
        }

        ValidatableResponse resp = req.put("/houses/neighbor/rooms/9/content/TV")
                .then();

        if (isDebugEnabled() || isTraceEnabled()) {
            resp = print(resp);
        }

        resp.statusCode(400)
                .contentType(getProblemContentType())
                .body(getProblemStatusProperty(), equalTo(toResponseValue(400)))
                .body(getProblemTitleProperty(), equalTo(toResponseValue(CONSTRAINT_VIOLATION_TITLE)))
                .body(getProblemTypeProperty(), equalTo(toResponseValue(CONSTRAINT_VIOLATION_TYPE)))
                .body(getProblemDetailProperty(), equalTo(toResponseValue(CONSTRAINT_VIOLATION_DETAIL)))
                .body(getProblemCauseProperty(), notNullValue())
                .body(getProblemCausePropertySize(), equalTo(16))
                .body(getProblemCausePropertyAt(0), asCause("name", ReqPart.Body, OAS3Violations.MAXIMUM_SIZE_VIOLATION_FOR_FIELD))
                .body(getProblemCausePropertyAt(1), asCause("weight", ReqPart.Body, OAS3Violations.MAXIMUM_LIMIT_VIOLATION_FOR_FIELD))
                .body(getProblemCausePropertyAt(2), asCause("height", ReqPart.Body, OAS3Violations.MAXIMUM_LIMIT_VIOLATION_FOR_FIELD))
                .body(getProblemCausePropertyAt(3), asCause("width", ReqPart.Body, OAS3Violations.MINIMUM_LIMIT_VIOLATION_FOR_FIELD))
                .body(getProblemCausePropertyAt(4), asCause("cost.amount", ReqPart.Body, OAS3Violations.MAXIMUM_LIMIT_VIOLATION_FOR_FIELD))
                .body(getProblemCausePropertyAt(5), asCause("cost.currency", ReqPart.Body, OAS3Violations.MISSING_INPUT_FOR_REQUIRED_FIELD))
                .body(getProblemCausePropertyAt(6), asCause("provider", ReqPart.Body, OAS3Violations.MINIMUM_SIZE_VIOLATION_FOR_FIELD))
                .body(getProblemCausePropertyAt(7), asCause("provider.id", ReqPart.Body, OAS3Violations.MISSING_INPUT_FOR_REQUIRED_FIELD))
                .body(getProblemCausePropertyAt(8), asCause("bought", ReqPart.Body, OAS3Violations.MISSING_INPUT_FOR_REQUIRED_FIELD))
                .body(getProblemCausePropertyAt(9), asCause("colors", ReqPart.Body, OAS3Violations.MAXIMUM_SIZE_VIOLATION_FOR_FIELD))
                .body(getProblemCausePropertyAt(10), asCause("colors[1]", ReqPart.Body, OAS3Violations.UNIQUE_VALUE_VIOLATION_FOR_FIELD))
                .body(getProblemCausePropertyAt(11), asCause("colors[3]", ReqPart.Body, OAS3Violations.UNIQUE_VALUE_VIOLATION_FOR_FIELD))
                .body(getProblemCausePropertyAt(12), asCause("colors[0]", ReqPart.Body, OAS3Violations.INVALID_INPUT_FOR_ENUM_FIELD))
                .body(getProblemCausePropertyAt(13), asCause("colors[2]", ReqPart.Body, OAS3Violations.INVALID_INPUT_FOR_ENUM_FIELD))
                .body(getProblemCausePropertyAt(14), asCause("brand.id", ReqPart.Body, OAS3Violations.MISSING_INPUT_FOR_REQUIRED_FIELD))
                .body(getProblemCausePropertyAt(15), asCause("barcode", ReqPart.Body, OAS3Violations.MINIMUM_SIZE_VIOLATION_FOR_FIELD));
    }

    protected String readFile(String filename) throws IOException {

        if (filename == null) {
            throw new IllegalArgumentException("Cannot readFile:null");
        }

        if (filename.equals("scenario-2")) {
            return "{}";
        }

        InputStream io = getClass().getResourceAsStream("/oas3/scenarios/" + filename + "." + getReadFileExtension());

        if (io == null) {
            throw new IllegalArgumentException("Cannot readFile:" + filename);
        }

        return StringUtils.replace(IOUtils.toString(io, Charset.defaultCharset()), "\n", "");
    }

    protected String getReadFileExtension() {
        return "json";
    }

    protected String getProblemCausePropertyAt(int index) {
        return getProblemCauseProperty() + "[" + index + "]";
    }

    protected Object toResponseValue(Object value) {
        return value;
    }

    protected String getProblemStatusProperty() {
        return STATUS_PROPERTY;
    }

    protected String getProblemTitleProperty() {
        return TITLE_PROPERTY;
    }

    protected String getProblemTypeProperty() {
        return TYPE_PROPERTY;
    }

    protected String getProblemDetailProperty() {
        return DETAIL_PROPERTY;
    }

    protected String getProblemCauseProperty() {
        return CAUSE_PROPERTY;
    }

    protected String getProblemCausePropertySize() {
        return getProblemCauseProperty() + ".size()";
    }

    protected void print(RequestSpecification req) {
        PrintStream stream = IoBuilder.forLogger(LOGGER).buildPrintStream();
        req.filter(RequestLoggingFilter.logRequestTo(stream))
                .filter(ResponseLoggingFilter.logResponseTo(stream)).log().all();
    }

    protected ValidatableResponse print(ValidatableResponse resp) {
        return resp.log().all();
    }

    protected abstract String getContentType();

    protected abstract String getProblemContentType();

    protected Matcher<?> asCause(String name, ReqPart part, OAS3Violations v) {
        return equalTo(toResponseValue(toCauseValue(name, part, v.message)));
    }

    protected Matcher<?> asCause(ReqPart part, OAS3Violations v) {
        return asCause(null, part, v);
    }

    protected Object toCauseValue(String name, ReqPart part, String message) {
        return toCauseValue(name, part == null ? null : part.name, message);
    }

    private Object toCauseValue(String name, String part, String message) {
        Map<String, Object> object = new HashMap<>();

        if (name != null) {
            object.put("key", name);
        }

        if (part != null) {
            object.put("part", part);
        }

        if (message != null) {
            object.put("message", message);
        }

        return object;
    }

    protected String newBearerToken() {
        return "Bearer " + RandomStringUtils.randomAlphanumeric(32);
    }

    protected Cookies newDeviceCookie(String value) {
        return Cookies.cookies(new Cookie.Builder("device", value)
                .setExpiryDate(toDate(LocalDateTime.now().plusSeconds(10)))
                .setMaxAge(1).setHttpOnly(false).setSecured(false).build());
    }

    private Date toDate(LocalDateTime instant) {
        return Date.from(instant.atZone(ZoneId.systemDefault()).toInstant());
    }

}
