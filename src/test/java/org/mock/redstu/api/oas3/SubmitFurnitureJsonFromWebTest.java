package org.mock.redstu.api.oas3;

import org.junit.jupiter.api.BeforeEach;
import org.mock.redstu.api.MediaTypes;
import org.mock.redstu.api.Respond;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

class SubmitFurnitureJsonFromWebTest extends AbstractSubmitFurnitureTest {

    private final String content;

    public SubmitFurnitureJsonFromWebTest() throws IOException {
        String filename = super.getYamlSource();
        this.content = Files.readString(Path.of(System.getProperty("project.dir.path"), filename));
    }

    @Override
    @BeforeEach
    void afterPropertiesSet() throws IOException {

        Respond.getRequest("/test/specification.yaml", (req, resp) -> {
            resp.setStatus(200);
            resp.setContentType("application/x-yaml");
            resp.sendText(content);
        });

        super.afterPropertiesSet();
    }

    @Override
    protected String getYamlSource() {
        return "http://localhost:8085/test/specification.yaml";
    }

    @Override
    protected String getContentType() {
        return MediaTypes.APPLICATION_JSON;
    }

    @Override
    protected String getProblemContentType() {
        return MediaTypes.APPLICATION_PROBLEM_JSON;
    }

    @Override
    protected boolean isDebugEnabled() {
        return false;
    }

}
