
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mock.redstu.server.commons.HttpPath;
import org.mock.redstu.server.commons.PathParams;

public class HttpPathTest {

    @Test
    public void url_must_be_deemed_to_not_match_when_it_contains_additional_chars_at_end_end(){
        HttpPath httpPath = new HttpPath("/user/{name}");
        Assertions.assertFalse(httpPath.matches("/user/mario/secrets"));
    }

    @Test
    public void must_return_empty_params_when_url_does_not_match(){
        HttpPath httpPath = new HttpPath("/user/{name}");
        Assertions.assertTrue(httpPath.getParams("/user/mario/secrets").isEmpty());
    }

    @Test
    public void name_and_id_params_must_be_found_in_url(){
        HttpPath httpPath = new HttpPath("/user/{name}/secrets/{id}");
        PathParams pathParams = httpPath.getParams("/user/mario/secrets/101").orElseThrow(
                () -> new RuntimeException("url did not match"));
        Assertions.assertEquals("mario",pathParams.getValue("name"));
        Assertions.assertEquals("101",pathParams.getValue("id"));
    }

    @Test
    public void name_id_params_must_be_found_in_url(){
        HttpPath httpPath = new HttpPath("/user/{name}/secrets/{id}");
        PathParams pathParams = httpPath.getParams("/user/mario/secrets/101").orElseThrow(
                () -> new RuntimeException("url did not match"));
        Assertions.assertEquals("mario",pathParams.getValue("name"));
        Assertions.assertEquals("101",pathParams.getValue("id"));
    }

    @Test
    void url_encoded_param_must_be_matched(){
        HttpPath httpPath = new HttpPath("/countries/{countryId}/provinces/{provinceId}");
        PathParams pathParams = httpPath.getParams("/countries/MZ/provinces/Maputo%2DCity").orElseThrow(
                () -> new RuntimeException("url did not match"));
        Assertions.assertEquals("Maputo%2DCity",pathParams.getValue("provinceId"));
    }

    @Test
    void path_with_wildcard_in_the_middle_only_must_be_matched(){

        HttpPath httpPath = new HttpPath("/locations/*/provinces/{provinceId}");
        Assertions.assertTrue(httpPath.matches("/locations/Africa/MZ/provinces/MPT"));

    }

    @Test
    void path_with_wildcard_in_the_middle_only_must_not_be_matched(){

        HttpPath httpPath = new HttpPath("/locations/*/provinces/{provinceId}");
        Assertions.assertFalse(httpPath.matches("/locations/Africa/MZ/provinces/MPT/neighbourhoods/Baixa"));

    }

    @Test
    void path_with_wildcard_in_the_end_only_must_be_matched(){

        HttpPath httpPath = new HttpPath("/locations/*");
        Assertions.assertTrue(httpPath.matches("/locations/Africa/MZ/provinces/MPT"));


    }

    @Test
    void path_with_wildcard_in_the_middle_and_end_must_be_matched(){

        HttpPath httpPath = new HttpPath("/locations/*/provinces/{provinceId}/*");
        Assertions.assertTrue(httpPath.matches("/locations/Africa/MZ/provinces/MPT/neighbourhoods/Baixa"));

    }

    @Test
    void wildcard_matches_any_path(){

        HttpPath httpPath = new HttpPath("*");
        Assertions.assertTrue(httpPath.matches("a"));
        Assertions.assertTrue(httpPath.matches("/locations"));
        Assertions.assertTrue(httpPath.matches("/locations/Africa"));
        Assertions.assertTrue(httpPath.matches("MPT/neighbourhoods"));

    }



}
