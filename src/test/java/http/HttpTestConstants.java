package http;

public class HttpTestConstants {

    public static final String HEADER_SET_COOKIE = "Set-Cookie";
    public static final String COOKIE_ATTR_SECURE = "secure";
    public static final String COOKIE_ATTR_HTTP_ONLY = "HttpOnly";

}
