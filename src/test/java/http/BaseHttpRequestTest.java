package http;

import io.restassured.RestAssured;
import io.restassured.config.JsonConfig;
import io.restassured.config.XmlConfig;
import io.restassured.parsing.Parser;
import io.restassured.path.json.config.JsonPathConfig;
import org.eclipse.jetty.client.HttpClient;
import org.mock.redstu.api.MediaTypes;
import org.mock.redstu.server.Redish;
import org.mock.redstu.server.config.ServerConfig;

import java.io.File;

public abstract class BaseHttpRequestTest {

    private static final int DEFAULT_PORT = 8085;

    public static Redish createServer(){
        return createServer(DEFAULT_PORT);
    }

    public static Redish createServer(int port){
        RestAssured.baseURI = "http://localhost";
        RestAssured.port = DEFAULT_PORT;
        RestAssured.defaultParser = Parser.JSON;
        RestAssured.registerParser(MediaTypes.APPLICATION_XML, Parser.XML);
        RestAssured.registerParser(MediaTypes.APPLICATION_PROBLEM_XML, Parser.XML);

        JsonConfig jsonConfig = JsonConfig.jsonConfig()
                .numberReturnType(JsonPathConfig.NumberReturnType.DOUBLE);

        RestAssured.config = RestAssured.config()
                .jsonConfig(jsonConfig)
                .xmlConfig(XmlConfig.xmlConfig().namespaceAware(true));

        ServerConfig config = new ServerConfig(port);
        return new Redish("test",config);
    }

    public static HttpClient newClient(){
        try {
            HttpClient client = new HttpClient();
            client.start();
            return client;
        }catch (Exception ex){
            throw new RuntimeException("error setting up client",
                    ex);
        }
    }

    protected File testResource(String path){
        return new File("src/test/resources/"+path);
    }


}
