package http;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.mock.redstu.api.MediaTypes;
import org.mock.redstu.api.filter.FilterChain;
import org.mock.redstu.api.filter.HttpFilter;
import org.mock.redstu.api.req.HttpReq;
import org.mock.redstu.api.resp.HttpResp;
import org.mock.redstu.js.JsRuntime;
import org.mock.redstu.server.Redish;

import java.io.File;
import java.util.*;

import static io.restassured.RestAssured.given;

class RespondPostTest extends BaseHttpRequestTest {

    static class FileInfo {

        private String filename;
        private String contentType;
        private String length;

        public String getFilename() {
            return filename;
        }

        public void setFilename(String name) {
            this.filename = name;
        }

        public String getContentType() {
            return contentType;
        }

        public void setContentType(String contentType) {
            this.contentType = contentType;
        }

        public String getLength() {
            return length;
        }

        public void setLength(String length) {
            this.length = length;
        }
    }

    static class MultipleUploadResponse {

        private List<FileInfo> infos = new ArrayList<>();

        public List<FileInfo> getInfos() {
            return infos;
        }
    }

    private static Redish server;

    @BeforeAll
    public static void initialize(){
        server = BaseHttpRequestTest.createServer();
        server.setCurrent();
        server.start();
    }


    @Test
    void url_encoded_form_must_be_parsed_successfully_by_server(){


    }

    @Test
    void url_encoded_form_with_path_param_must_be_parsed_successfully_by_server(){

    }

    @Test
    void url_encoded_form_with_query_param_must_be_parsed_successfully_by_server(){

    }

    @Test
    void multipart_encoded_form_must_be_parsed_successfully_by_server(){

    }

    @Test
    void multipart_encoded_form_with_query_params_must_be_parsed_successfully_by_server(){

    }

    @Test
    void multipart_encoded_form_with_path_params_must_be_parsed_successfully_by_server(){

    }

    @Test
    void multipart_encoded_form_with_one_file_must_be_parsed_successfully_by_server(){

    }

    @Test
    void multipart_encoded_form_with_multiple_files_must_be_parsed_successfully_by_server(){

        JsRuntime runtime = JsRuntime.fromTempDirectory();
        try {
            runtime.mountFile(testResource("post/post7.js"), Constants.TEST_MOUNT_PATH);
            runtime.start();
            runtime.evalFile(Constants.TEST_MOUNT_PATH);

            File pomXmlFile = new File("pom.xml");
            File gitIgnoreFile = new File(".gitignore");
            FileInfo[] array = given()
                    .multiPart(Constants.ATTACHMENTS_KEY, pomXmlFile)
                    .multiPart(Constants.ATTACHMENTS_KEY, gitIgnoreFile)
                    .queryParam(Constants.NAME_KEY, Constants.NAME_MICHAEL)
                    .contentType(MediaTypes.FORM_MULTIPART_ENCODED)
                    .post("/mock/post7")
                    .then()
                    .contentType(MediaTypes.APPLICATION_JSON)
                    .statusCode(200)
                    .extract().as(FileInfo[].class);
            List<FileInfo> fileInfos = Arrays.asList(array);
            Assertions.assertEquals(2, fileInfos.size());
            Iterator<FileInfo> iterator = fileInfos.iterator();
            FileInfo fileInfo = iterator.next();
            Assertions.assertEquals(pomXmlFile.getName(), fileInfo.filename);
            Assertions.assertEquals(String.valueOf(pomXmlFile.length()), fileInfo.length);

            fileInfo = iterator.next();
            Assertions.assertEquals(gitIgnoreFile.getName(), fileInfo.filename);
            Assertions.assertEquals(String.valueOf(gitIgnoreFile.length()), fileInfo.length);
        }finally {
            runtime.shutdown();
        }

    }



    @Test
    void octet_stream_file_upload_must_be_parsed_successfully_by_server(){

        JsRuntime runtime = JsRuntime.fromTempDirectory();
        try {
            runtime.mountFile(testResource("post/post8.js"), Constants.TEST_MOUNT_PATH);
            runtime.start();
            runtime.evalFile(Constants.TEST_MOUNT_PATH);

            File pomXmlFile = new File("pom.xml");
            String body = given()
                    .body(pomXmlFile)
                    .contentType(MediaTypes.APPLICATION_OCTET_STREAM)
                    .post("/mock/post8")
                    .then()
                    .contentType(MediaTypes.TEXT_PLAIN)
                    .extract().body().asString();
            Assertions.assertEquals(String.valueOf(pomXmlFile.length()), body);

        }finally {
            runtime.shutdown();
        }

    }

    @Test
    void json_body_must_be_parsed_successfully_by_server(){

        JsRuntime runtime = JsRuntime.fromTempDirectory();
        try {
            runtime.mountFile(testResource("post/post9.js"), Constants.TEST_MOUNT_PATH);
            runtime.start();
            runtime.evalFile(Constants.TEST_MOUNT_PATH);

            String body = given()
                    .contentType(MediaTypes.APPLICATION_JSON)
                    .body(Constants.personJohnDoe())
                    .post("/mock/post9")
                    .then()
                    .contentType(MediaTypes.TEXT_PLAIN)
                    .extract().body().asString();

            Assertions.assertEquals(Constants.EMAIL_JOHN_DOE, body);
        }finally {
            runtime.shutdown();
        }

    }

    @Test
    void xml_body_must_be_parsed_successfully_by_server(){
        JsRuntime runtime = JsRuntime.fromTempDirectory();
        try {
            runtime.mountFile(testResource("post/post10.js"), Constants.TEST_MOUNT_PATH);
            runtime.start();
            runtime.evalFile(Constants.TEST_MOUNT_PATH);

            String body = given()
                    .contentType(MediaTypes.APPLICATION_XML)
                    .body(testResource("post/post10.xml"))
                    .post("/mock/post10")
                    .then()
                    .contentType(MediaTypes.TEXT_PLAIN)
                    .extract().body().asString();

            System.out.println(body);
            Assertions.assertEquals(Constants.PHONE_JOHN_DOE, body);
        }finally {
            runtime.shutdown();
        }
    }

    @Test
    void server_must_respond_with_request_headers(){

    }

    @AfterAll
    public static void teardown(){
        if(server.isActive())
            server.stop();
    }

}
