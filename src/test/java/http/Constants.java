package http;

import java.util.HashMap;
import java.util.Map;

public class Constants {

    public static final String HELLO_WORLD = "Hello world";
    public static final String NAME_JOHN = "John";
    public static final String NAME_MICHAEL = "Michael";
    public static final String SURNAME_DOE = "Doe";
    public static final String SURNAME_JORDAN = "Jordan";
    public static final String EMAIL_JOHN_DOE = "john.doe@redstu.com";
    public static final String EMAIL_MICHAEL_JORDAN = "michael.jordan@redstu.com";
    public static final String PHONE_MICHAEL_JORDAN = "+25810299940";
    public static final String PHONE_JOHN_DOE = "2930293";
    public static final String MESSAGE_KEY = "message";
    public static final String WIDTH_KEY = "width";
    public static final int WIDTH = 1024;
    public static final String WEIGHT_KEY = "weight";
    public static final double WEIGHT = 57.10;
    public static final String SUCCESS_KEY = "success";
    public static final String NAME_KEY ="name";
    public static final String SURNAME_KEY ="surname";
    public static final String ATTACHMENT_KEY  = "attachment";
    public static final String ATTACHMENTS_KEY  = "attachments";
    public static final String FILENAME_KEY  = "filename";
    public static final String SIZE_KEY  = "size";
    public static final String FILE_LENGTH_KEY  = "length";

    public static final String TEST_MOUNT_PATH = "test.js";

    public static Map<String,Object> personJohnDoe(){
        Map<String,Object> contact = new HashMap<>();
        contact.put("email",EMAIL_JOHN_DOE);
        contact.put("phone",PHONE_JOHN_DOE);

        Map<String,Object> person = new HashMap<>();
        person.put("name",NAME_JOHN);
        person.put("surname",SURNAME_DOE);
        person.put("contact",contact);

        return person;

    }

    public static Map<String,Object> personObjectWithDetails(String name, String surname, String email, String phone){
        Map<String,Object> contact = new HashMap<>();
        contact.put("email",email);
        contact.put("phone",phone);

        Map<String,Object> person = new HashMap<>();
        person.put("name",name);
        person.put("surname",surname);
        person.put("contact",contact);

        return person;

    }

    public static Map<String,Object> personObjectWithFullName(String name, String surname){
        Map<String,Object> person = new HashMap<>();
        person.put("name",name);
        person.put("surname",surname);
        return person;
    }


}
