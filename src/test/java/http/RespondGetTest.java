package http;

import org.junit.jupiter.api.Test;


class RespondGetTest extends BaseHttpRequestTest {


    @Test
    void server_must_return_hello_world_text_response_with_status_200() throws Exception {

    }

    @Test
    void server_must_set_non_secure_and_non_http_only_cookie() throws Exception{

    }

    @Test
    void server_must_set_secure_cookie() throws Exception{

    }

    @Test
    void server_must_return_http_only_cookie() throws Exception{

    }

    @Test
    void server_must_support_string_query_param() throws Exception{

    }


    @Test
    void server_must_support_string_path_param() throws Exception{

    }


    @Test
    void server_must_return_empty_body_404() throws Exception{

    }

    @Test
    void server_must_return_non_empty_404() throws Exception{

    }

    @Test
    void server_must_return_empty_body_500() throws Exception{

    }

    @Test
    void server_must_return_non_empty_500() throws Exception{

    }

    @Test
    void server_must_respond_with_custom_header() {

    }

}
