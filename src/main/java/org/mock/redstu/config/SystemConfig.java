package org.mock.redstu.config;

import org.apache.logging.log4j.Level;
import org.mock.redstu.logging.LoggingConfig;

/**
 * System configurations enum
 */
public enum SystemConfig {

    PROJECT_DIRECTORY("PROJECT_DIR","project.dir.path",""),
    PROJECT_INITIAL_STATE_FILE("PROJECT_PATHS_INITIAL_STATE_FILE","project.paths.initialStateFile","src/state.json"),
    //Mock server configs
    MOCK_SERVER_PORT("MOCK_SERVER_PORT","mock.server.port","8081"),
    MOCK_SERVER_HOSTNAME("MOCK_SERVER_HOSTNAME","mock.server.hostname","0.0.0.0"),
    MOCK_SERVER_CORS_ALLOW_METHODS("MOCK_SERVER_CORS_ALLOW_METHODS","mock.server.cors.allow.methods","*"),
    MOCK_SERVER_CORS_ALLOW_HEADERS("MOCK_SERVER_CORS_ALLOW_HEADERS","mock.server.cors.allow.headers","*"),
    MOCK_SERVER_CORS_EXPOSE_HEADERS("MOCK_SERVER_CORS_EXPOSE_HEADERS","mock.server.cors.expose.headers","*"),
    MOCK_SERVER_CORS_MAX_AGE("MOCK_SERVER_CORS_MAX_AGE","mock.server.cors.max.age","86400"),
    MOCK_SERVER_CORS_ALLOW_ORIGIN("MOCK_SERVER_CORS_ALLOW_ORIGIN","mock.server.cors.allow.origin","*"),
    //Cockpit server configs
    COCKPIT_SERVER_PORT("COCKPIT_SERVER_PORT","cockpit.server.port","4849"),
    COCKPIT_SERVER_HOSTNAME("COCKPIT_SERVER_PORT","cockpit.server.hostname","0.0.0.0"),
    COCKPIT_ENABLE("COCKPIT_ENABLE","cockpit.enable","false"),
    COCKPIT_API_KEY("COCKPIT_API_KEY","cockpit.apikey",""),
    //Git configs
    GIT_REPOSITORY_URL("GIT_REPOSITORY_URL","git.repository.url"),
    GIT_BRANCH("GIT_BRANCH","git.branch","master"),
    GIT_PROJECT_PATH("GIT_PROJECT_PATH","git.project.path"),
    GIT_USERNAME("GIT_USERNAME","git.username"),
    GIT_PASSWORD("GIT_PASSWORD","git.password"),
    //Log configs
    LOG_LEVEL("LOG_LEVEL","log.level", Level.INFO.name()),
    LOG_MODE("LOG_MODE","log.mode", LoggingConfig.Mode.LIVE.name());

    private String environmentVariableName;
    private String systemPropertyKey;
    private String defaultValue;

    SystemConfig(String environmentVariableName, String systemPropertyKey){
        this(environmentVariableName,systemPropertyKey,null);
    }

    SystemConfig(String environmentVariableName, String systemPropertyKey, String defaultValue){
        this.environmentVariableName = environmentVariableName;
        this.systemPropertyKey = systemPropertyKey;
        this.defaultValue = defaultValue;
    }

    public String value(){
        String value = System.getProperty(systemPropertyKey);
        if(value==null||value.isEmpty())
            value = System.getenv(environmentVariableName);
        return (value!=null ? value : defaultValue);
    }

    public int asInt(){
        try{
            return Integer.parseInt(value());
        }catch (NumberFormatException ex){
            return Integer.parseInt(defaultValue);
        }
    }

    public boolean asBoolean(){
        return Boolean.parseBoolean(value());
    }

    public long asLong(){
        try{
            return Long.parseLong(value());
        }catch (NumberFormatException ex){
            return Long.parseLong(defaultValue);
        }
    }

    public boolean isPresent(){
        String value = System.getProperty(systemPropertyKey);
        if(value==null)
            return System.getenv(environmentVariableName)!=null;
        return true;
    }

}
