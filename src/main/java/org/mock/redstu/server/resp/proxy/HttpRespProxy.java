package org.mock.redstu.server.resp.proxy;

import org.graalvm.polyglot.Value;
import org.mock.redstu.api.common.HttpCookie;
import org.mock.redstu.api.common.HttpCookieBuilder;
import org.mock.redstu.api.resp.HttpResp;
import org.mock.redstu.js.AbstractProxyObject;
import org.mock.redstu.js.JSObject;

import java.time.Instant;

public class HttpRespProxy extends AbstractProxyObject {

    private HttpResp resp;

    private class SetProxy {
        private Value object;
        SetProxy(Value object){
            this.object = object;
        }
        HttpCookie buildCookie(){
            if(!object.hasMember("name")||!object.hasMember("value"))
                throw new IllegalArgumentException("object must have name & value properties");
            HttpCookieBuilder builder = HttpCookie.builder(object.getMember("name") .asString(), object.getMember("value").asString());
            if(object.hasMember("httpOnly"))
                builder.httpOnly(object.getMember("httpOnly").asBoolean());
            if(object.hasMember("domain"))
                builder.domain(object.getMember("domain").asString());
            if(object.hasMember("path"))
                builder.path(object.getMember("path").asString());
            if(object.hasMember("secure"))
                builder.secure(object.getMember("secure").asBoolean());
            if(object.hasMember("sameSite"))
                builder.sameSiteMode(object.getMember("sameSite").asString());
            if(object.hasMember("maxAge"))
                builder.maxAge(object.getMember("maxAge").asInt());
            if(object.hasMember("expires"))
                builder.expires(Instant.from(object.getMember("expires").asTime()));
            if(object.hasMember("comment"))
                builder.comment(object.getMember("comment").asString());
            return builder.build();
        }
    }

    public HttpRespProxy(HttpResp resp){
        this.resp = resp;
        executable("setCookie",this::setCookie);
        executable("setHeader",this::setHeader);
        executable("setStatus",this::setStatus);
        executable("setContentType",this::setContentType);
        executable("sendText",this::sendText);
        executable("sendBytes",this::sendBytes);
        executable("sendObject",this::sendObject);
        executable("sendArray",this::sendArray);
        executable("send",this::send);
    }

    public HttpResp getObject() {
        return resp;
    }

    private Object setCookie(Value... args){
        if(args==null||args.length<1)
            throw new IllegalArgumentException("one argument is required");
        resp.setCookie(new SetProxy(args[0]).buildCookie());
        return null;
    }

    private Object setHeader(Value... args){
        if(args==null||args.length<2)
            throw new IllegalArgumentException("two arguments are required");
        Value headerName = args[0];
        Value headerVal = args[1];
        if(headerVal.isString()) {
            resp.setHeader(headerName.asString(), headerVal.asString());
        }else if(headerVal.hasArrayElements()){
            String[] array = new String[(int)headerVal.getArraySize()];
            for(int i=0; i<headerVal.getArraySize();i++){
                array[i] = headerVal.getArrayElement(i).asString();
            }
            resp.setHeader(headerName.asString(),array);
        }
        else throw new IllegalArgumentException("second argument must be either a string or an array");
        return null;
    }

    private Object setStatus(Value... args){
        if(args==null||args.length<1)
            throw new IllegalArgumentException("one argument is required");
        Value status = args[0];
        if(!status.fitsInShort())
            throw new IllegalArgumentException("argument must be a short integer");
        resp.setStatus(status.asInt());
        return null;
    }

    private Object setContentType(Value... args){
        if(args==null||args.length<1)
            throw new IllegalArgumentException("one argument is required");
        resp.setContentType(args[0].asString());
        return null;
    }


    private Value sendBytes(Value... args){
        if(args==null||args.length<1)
            throw new IllegalArgumentException("one argument is required");
        Value bytesArg = args[0];
        if(!bytesArg.hasArrayElements())
            throw new IllegalArgumentException("argument must be an array of bytes");
        byte[] data = new byte[(int) bytesArg.getArraySize()];
        for(int i=0; i<bytesArg.getArraySize();i++){
            data[i] = bytesArg.getArrayElement(i).asByte();
        }
        resp.sendBytes(data);
        return null;
    }

    private Value sendText(Value... args){
        if(args==null||args.length<1)
            throw new IllegalArgumentException("one argument is required");
        resp.sendText(args[0].asString());
        return null;
    }

    private Value sendObject(Value... args){
        if(args==null||args.length<1)
            throw new IllegalArgumentException("one argument is required");
        Value objectVal = args[0];
        resp.sendObject(JSObject.asHost(objectVal));
        return null;
    }

    private Value sendArray(Value... args){
        if(args==null||args.length<1)
            throw new IllegalArgumentException("one argument is required");
        Value array = args[0];
        if(!array.hasArrayElements())
            throw new IllegalArgumentException("argument must be an array");
        Value[] values = args[0].as(Value[].class);
        Object[] objectArray = new Object[values.length];
        for(int i=0;i<values.length;i++){
            objectArray[i] = JSObject.asHost(values[i]);
        }
        resp.sendArray(objectArray);
        return null;
    }

    private Value send(Value... args){
        resp.send();
        return null;
    }

}
