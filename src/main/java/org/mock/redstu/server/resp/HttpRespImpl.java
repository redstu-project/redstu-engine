package org.mock.redstu.server.resp;

import io.undertow.server.HttpServerExchange;
import io.undertow.util.HeaderValues;
import io.undertow.util.Headers;
import io.undertow.util.HttpString;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.mock.redstu.api.ContentType;
import org.mock.redstu.api.MediaTypes;
import org.mock.redstu.api.common.HttpCookie;
import org.mock.redstu.api.resp.HttpResp;
import org.mock.redstu.server.Constants;
import org.mock.redstu.server.util.CookieHelper;
import org.mock.redstu.util.JSON;
import org.mock.redstu.util.XML;

import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

public class HttpRespImpl implements HttpResp {

    private HttpServerExchange exchange;

    private static final Logger LOGGER = LogManager.getLogger(HttpRespImpl.class);

    public HttpRespImpl(HttpServerExchange exchange){
        this.exchange = exchange;
    }

    @Override
    public void sendObject(Object object) {
        if(contentTypeNotSet()) {
            LOGGER.info("Setting default object content type: "+MediaTypes.APPLICATION_JSON);
            setContentType(MediaTypes.APPLICATION_JSON);
        }
        setDefaultStatus(200);
        ContentType type = ContentType.which(getContentType());
        if(type==ContentType.APPLICATION_JSON) {
            this.sendBytes(JSON.bytes(object));
        }else if(type==ContentType.APPLICATION_XML) {
            this.sendBytes(XML.bytes(object));
        }else {
            throw new InvalidContentTypeException("expected either XML or JSON content type, instead found: "+
                    getContentType());
        }
        this.exchange.getResponseSender().close();
    }

    @Override
    public void sendArray(Object[] array) {
        LOGGER.info("Setting default array content type: "+MediaTypes.APPLICATION_JSON);
        setContentType(MediaTypes.APPLICATION_JSON);
        setDefaultStatus(200);
        this.sendBytes(JSON.bytes(array));
        this.exchange.getResponseSender().close();
    }

    @Override
    public void sendText(String text) {
        if(contentTypeNotSet())
            setContentType(MediaTypes.TEXT_PLAIN);
        setDefaultStatus(200);
        this.exchange.getResponseSender().send(text);
        this.exchange.getResponseSender().close();
    }

    @Override
    public void sendText(String text, Charset charset) {
        if(contentTypeNotSet()) {
            LOGGER.info("Setting default text content type: "+MediaTypes.TEXT_PLAIN);
            setContentType(MediaTypes.TEXT_PLAIN);
        }
        setDefaultStatus(200);
        this.exchange.getResponseSender().send(text,charset);
        this.exchange.getResponseSender().close();
    }

    @Override
    public void send() {
        setDefaultStatus(204);
        this.exchange.getResponseSender().close();
    }

    @Override
    public void setHeader(String name, String... values) {
        this.exchange.getResponseHeaders().putAll(
                HttpString.tryFromString(name), Arrays.asList(values));
    }

    @Override
    public void setStatus(int status) {
        this.exchange.setStatusCode(status);
    }


    @Override
    public void setContentType(String contentType) {
        this.setHeader(Constants.HEADER_CONTENT_TYPE,contentType);
    }

    @Override
    public void setCookie(HttpCookie cookie) {
        this.exchange.setResponseCookie(CookieHelper.toUndertow(cookie));
    }

    @Override
    public void sendBytes(byte[] bytes) {
        if(contentTypeNotSet()) {
            LOGGER.info("Setting default binary content type: "+MediaTypes.APPLICATION_OCTET_STREAM);
            setContentType(MediaTypes.APPLICATION_OCTET_STREAM);
        }
        this.exchange.getResponseSender().send(ByteBuffer.wrap(bytes));
        this.exchange.getResponseSender().close();
    }

    private boolean contentTypeNotSet(){
        HeaderValues headerValues = exchange.getResponseHeaders().get(Headers.CONTENT_TYPE);
        return headerValues == null || headerValues.isEmpty();
    }

    private String getContentType(){
        HeaderValues headerValues = exchange.getResponseHeaders().get(Headers.CONTENT_TYPE);
        if(headerValues!=null && !headerValues.isEmpty())
            return headerValues.getLast();
        return "";
    }

    private void setDefaultStatus(int status){
        if(exchange.getStatusCode()<=0)
            exchange.setStatusCode(status);
    }

}
