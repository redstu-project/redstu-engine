package org.mock.redstu.server.resp;

public class InvalidContentTypeException extends HttpResponseException {

    public InvalidContentTypeException(String message) {
        super(message);
    }

}
