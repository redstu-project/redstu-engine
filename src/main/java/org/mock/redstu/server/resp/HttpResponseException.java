package org.mock.redstu.server.resp;

import org.mock.redstu.server.RedishException;

public class HttpResponseException extends RedishException {

    public HttpResponseException(String message) {
        super(message);
    }

    public HttpResponseException(String message, Throwable cause) {
        super(message, cause);
    }
}
