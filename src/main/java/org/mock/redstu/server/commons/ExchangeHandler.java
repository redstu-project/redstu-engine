package org.mock.redstu.server.commons;

import io.undertow.server.HttpHandler;
import org.mock.redstu.server.exchange.ServerContext;

import java.util.function.Consumer;


public abstract class ExchangeHandler implements HttpHandler {

    private HttpPath path;
    private Consumer<ServerContext> reactor;

    public ExchangeHandler(HttpPath path, Consumer<ServerContext> reactor){
        this.path = path;
        this.reactor = reactor;
    }

    public HttpPath getPath() {
        return path;
    }

    protected void getReaction(ServerContext input){
        this.reactor.accept(input);
    }

}
