package org.mock.redstu.server.commons;

import java.util.*;
import java.util.regex.Matcher;

public class PathParams {

    private List<String> paramNames;
    private Matcher matcher;

    PathParams(){
        this.paramNames = Collections.emptyList();
    }

    PathParams(List<String> paramNames, Matcher matcher){
        this.paramNames = paramNames;
        this.matcher = matcher;
    }

    public List<String> getNames(){
        return Collections.unmodifiableList(paramNames);
    }

    public String getValue(String name){
        return this.matcher.group(name);
    }

    public Map<String,String> toMap(){
        Map<String,String> map = new HashMap<>();
        for(String name: getNames()){
            map.put(name,getValue(name));
        }
        return map;
    }

}
