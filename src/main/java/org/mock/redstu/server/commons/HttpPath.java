package org.mock.redstu.server.commons;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class HttpPath {


    public static final String EVERYTHING = "/*";

    private String path;
    private String pattern;
    private Pattern compiledPattern;

    private List<String> paramNames = new ArrayList<>();

    private static final Pattern PARAM_NAME_PATTERN = Pattern.compile("\\{([A-Za-z]+)}");

    public HttpPath(String path) {
        this.path = path;
        this.pattern = path.replace("*", ".+");
        this.pattern = this.pattern.replaceAll("/", Matcher.quoteReplacement("\\/"));
        this.discoverParams();
    }

    public String getPath() {
        return this.path;
    }

    private void discoverParams() {
        Matcher matcher = PARAM_NAME_PATTERN.matcher(path);
        while (matcher.find()) {
            String paramName = matcher.group(1);
            paramNames.add(paramName);
            this.pattern = this.pattern.replaceAll(String.format("\\{%s}", paramName), String.format("\\(?<%s>[A-Za-z%%.0-9\\+_-]+)", paramName));
        }
        this.compiledPattern = Pattern.compile(pattern);
    }

    public List<String> getParamNames() {
        return Collections.unmodifiableList(paramNames);
    }

    public boolean matches(String url) {
        return compiledPattern.matcher(url)
                .matches();
    }

    public Optional<PathParams> getParams(String url) {
        Matcher matcher = compiledPattern.matcher(url);
        if (!matcher.matches())
            return Optional.empty();
        return Optional.of(new PathParams(paramNames,
                matcher));
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        HttpPath httpPath = (HttpPath) o;

        return path.equals(httpPath.path);
    }

    @Override
    public int hashCode() {
        return path.hashCode();
    }
}
