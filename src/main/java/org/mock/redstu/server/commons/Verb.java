package org.mock.redstu.server.commons;

public enum Verb {
    GET, POST, PUT, PATCH, DELETE, HEAD
}
