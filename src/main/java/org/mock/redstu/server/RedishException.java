package org.mock.redstu.server;

public class RedishException extends RuntimeException {

    public RedishException(String message){
        super(message);
    }

    public RedishException(String message, Throwable cause){
        super(message,cause);
    }

}
