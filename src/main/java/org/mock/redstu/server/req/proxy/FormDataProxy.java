package org.mock.redstu.server.req.proxy;

import org.graalvm.polyglot.Value;
import org.graalvm.polyglot.proxy.ProxyArray;
import org.graalvm.polyglot.proxy.ProxyObject;
import org.mock.redstu.api.req.FormEntry;

import java.util.HashMap;
import java.util.Map;

public class FormDataProxy implements ProxyObject {

    private Map<String,Object> proxiedFormData = new HashMap<>();

    public FormDataProxy(Map<String, FormEntry> formData){
        for(Map.Entry<String,FormEntry> entry: formData.entrySet()){
            proxiedFormData.put(entry.getKey(),new FormEntryProxy(entry.
                    getValue()));
        }
    }

    @Override
    public Object getMember(String key) {
        return proxiedFormData.get(key);
    }

    @Override
    public Object getMemberKeys() {
        return ProxyArray.fromArray(proxiedFormData.keySet().
                toArray());
    }

    @Override
    public boolean hasMember(String key) {
        return proxiedFormData.containsKey(key);
    }

    @Override
    public void putMember(String key, Value value) {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean removeMember(String key) {
        return false;
    }

}
