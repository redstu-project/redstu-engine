package org.mock.redstu.server.req;

import io.undertow.server.handlers.form.FormData;
import io.undertow.util.HeaderMap;
import io.undertow.util.HttpString;
import org.apache.commons.io.IOUtils;
import org.mock.redstu.api.ContentType;
import org.mock.redstu.api.common.BinaryContent;
import org.mock.redstu.api.common.Header;
import org.mock.redstu.api.req.FormValue;
import org.mock.redstu.server.Constants;
import org.mock.redstu.util.JSON;
import org.mock.redstu.util.XML;

import java.io.IOException;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.function.BiConsumer;

public class FormValueImpl implements FormValue {

    private FormData.FormValue formValue;

    private Map<String,Header> headers = new HashMap<>();

    public FormValueImpl(FormData.FormValue formValue){
        this.formValue = formValue;
        HeaderMap headerMap = formValue.getHeaders();
        if(headerMap!=null){
            Collection<HttpString> httpStrings = headerMap.getHeaderNames();
            for (HttpString headerName: httpStrings) {
                headers.put(headerName.toString(), new Header(headerMap.get(headerName)));
            }
        }
    }

    @Override
    public Map<String,Header> getHeaders() {
        return headers;
    }

    @Override
    public boolean isFileUpload() {
        return formValue.isFileItem();
    }

    @Override
    public String getContentType() {
        Header header = headers.get(Constants.HEADER_CONTENT_TYPE);
        if(header!=null)
            return header.value();
        return null;
    }

    @Override
    public String getCharset() {
        return formValue.getCharset();
    }

    @Override
    public String getFilename() {
        return formValue.getFileName();
    }

    @Override
    public BinaryContent getBinary() {
        if(!isFileUpload())
            throw new IllegalStateException("not a file upload");
        try {
            byte[] data = IOUtils.toByteArray(formValue.getFileItem().getInputStream());
            return new BinaryContentImpl(data);
        } catch (IOException ex) {
            throw new HttpRequestException("error reading uploaded file item",
                    ex);
        }
    }

    @Override
    public String getString() {
        if(isFileUpload())
            throw new IllegalStateException("value is a file upload");
        return formValue.getValue();
    }

    @Override
    public Boolean getBool() {
        return Boolean.parseBoolean(getString());
    }

    @Override
    public Double getDecimal() {
        try {
            return Double.parseDouble(getString());
        }catch (NumberFormatException ex){
            return 0.0;
        }
    }

    @Override
    public Integer getInteger() {
        try {
            return Integer.parseInt(getString());
        }catch (NumberFormatException ex){
            return 0;
        }
    }



    @Override
    public void parseObject(BiConsumer<Map<String,Object>, RuntimeException> callback) {
        String contentTypeStr = getContentType();
        ContentType contentType = ContentType.which(contentTypeStr);
        Map<String,Object> objectMap = null;
        if(contentType==ContentType.APPLICATION_XML)
            objectMap =XML.parseBytes(getBinary().toArray());
        else if(contentType==ContentType.APPLICATION_JSON)
            objectMap = JSON.parseBytes(getBinary().toArray());
        else callback.accept(null, new IllegalStateException("invalid content type found: "+
                    contentTypeStr));
        callback.accept(objectMap,null);
    }

    @Override
    public void parseArray(BiConsumer<Map<String,Object>[], RuntimeException> callback) {
        String contentTypeStr = getContentType();
        ContentType contentType = ContentType.which(contentTypeStr);
        Map<String,Object>[] maps = null;
        if(contentType!=ContentType.APPLICATION_JSON)
            callback.accept(null, new IllegalStateException("invalid content type found: "+
                    contentTypeStr));
        maps = JSON.parseBytesAs(getBinary().toArray(),Map[].class);
        callback.accept(maps,null);
    }

}
