package org.mock.redstu.server.req;

import org.mock.redstu.api.req.HttpParam;

public class HttpParamImpl implements HttpParam {

    private String value;

    public HttpParamImpl(String value){
        this.value = value;
    }

    @Override
    public String string() {
        return this.value;
    }

    @Override
    public Integer asInt() {
        try {
            return Integer.parseInt(value);
        }catch (NumberFormatException ex){
            return 0;
        }
    }

    @Override
    public Boolean asBool() {
        return Boolean.parseBoolean(value);
    }

    @Override
    public Double asDouble() {
        try {
            return Double.parseDouble(value);
        }catch (NumberFormatException ex){
            return 0.0;
        }
    }
}
