package org.mock.redstu.server.req;

import io.undertow.server.HttpHandler;
import io.undertow.server.HttpServerExchange;
import io.undertow.server.handlers.form.FormDataParser;
import io.undertow.server.handlers.form.FormParserFactory;
import io.undertow.util.HeaderMap;
import io.undertow.util.HeaderValues;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.mock.redstu.api.ContentType;
import org.mock.redstu.api.common.BinaryContent;
import org.mock.redstu.api.req.*;
import org.mock.redstu.server.RedishException;
import org.mock.redstu.util.JSON;
import org.mock.redstu.util.XML;

import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.BiConsumer;

public class HttpReqBodyImpl implements HttpReqBody {

    private HttpServerExchange exchange;
    private ContentType contentType = null;
    private String contentTypeHeader;

    private static final Logger LOGGER = LogManager.getLogger(HttpReqBodyImpl.class);

    public HttpReqBodyImpl(HttpServerExchange exchange){
        this.exchange = exchange;
        initialize();
    }

    private void initialize(){
        HeaderMap headerMap = exchange.getRequestHeaders();
        HeaderValues headerValues = headerMap.get("Content-Type");
        if(headerValues!=null){
            contentTypeHeader = exchange.getRequestHeaders().get("Content-Type").getFirst();
            LOGGER.info("Content type Header: "+ contentTypeHeader);
            contentType = ContentType.which(contentTypeHeader);
        }else LOGGER.warn("Content type header missing");
        LOGGER.info("Matched Content type: "+contentType);
    }


    @Override
    public String getContentType() {
        return contentTypeHeader;
    }

    @Override
    public void binary(BiConsumer<BinaryContent, RuntimeException> callback) {
        this.assertCallbackNotNull(callback);
        exchange.getRequestReceiver().receiveFullBytes((ex,bytes) -> {
            LOGGER.info("Successfully read Request body bytes");
            callback.accept(new BinaryContentImpl(bytes),null);
        },(ex,err) -> {
            String errMessage = "Error reading request body bytes";
            LOGGER.error(errMessage,err);
            callback.accept(null, new RedishException(errMessage,err));
        });
    }

    @Override
    public void text(BiConsumer<String, RuntimeException> callback) {
        this.assertCallbackNotNull(callback);
        if(contentType!=ContentType.TEXT)
            callback.accept(null,invalidContentTypeException());
        exchange.getRequestReceiver().receiveFullString((ex,str) -> {
            callback.accept(str,null);
        }, (ex,err) ->{
            String errMessage = "Error reading Request body full string";
            LOGGER.error(errMessage,err);
            callback.accept(null,new RedishException(errMessage,err));
        }, Charset.forName(exchange.getRequestCharset()));
    }

    @Override
    public void object(BiConsumer<Map<String,Object>, RuntimeException> callback) {
        try {
            ContentType contentType = ContentType.which(getContentType());
            if(contentType==ContentType.APPLICATION_JSON){
                Map<String,Object> parsed = JSON.parseString(rawText());
                callback.accept(parsed,null);
            }else if(contentType==ContentType.APPLICATION_XML){
                Map<String,Object> parsed = XML.parseString(rawText());
                callback.accept(parsed,null);
            }else callback.accept(null, new RuntimeException("invalid content type. Only JSON and XML are supported"));
        }catch (RuntimeException ex){
            callback.accept(null,ex);
        }
    }

    @Override
    public void array(BiConsumer<Map<String, Object>[], RuntimeException> callback) {
        try {
            ContentType contentType = ContentType.which(getContentType());
            if(contentType!=ContentType.APPLICATION_JSON)
                callback.accept(null, new RuntimeException("invalid content type. Only JSON is supported"));
            Map<String,Object>[] parsed = JSON.parseStringAs(rawText(),
                    Map[].class);
            callback.accept(parsed,null);
        }catch (RuntimeException ex){
            callback.accept(null,ex);
        }
    }

    @Override
    public void form(BiConsumer<Map<String, FormEntry>, RuntimeException> callback) {
        this.assertCallbackNotNull(callback);
        if(contentType!= ContentType.FORM_DATA_MULTIPART && contentType!=ContentType.FORM_DATA_URL_ENCODED)
            callback.accept(null,invalidContentTypeException());
        try {
            LOGGER.info("Parsing Form data payload");
            FormParserFactory.Builder builder = FormParserFactory.builder();
            final FormDataParser formDataParser = builder.build().createParser(exchange);
            if (formDataParser != null) {
                formDataParser.parse(new HttpHandler() {
                    @Override
                    public void handleRequest(HttpServerExchange httpServerExchange) {
                        io.undertow.server.handlers.form.FormData formData = exchange.getAttachment(FormDataParser.FORM_DATA);
                        Map<String,FormEntry> entries = new HashMap<>();
                        for (String name : formData) {
                            entries.put(name,new FormEntryImpl(name, formData.get(
                                    name)));
                        }
                        callback.accept(entries,null);
                    }
                });
            }
        }catch (RuntimeException ex){
            callback.accept(null,ex);
        }catch (Exception ex){
            callback.accept(null,new RedishException("error found parsing form data",
                    ex));
        }
    }


    private String rawText() {
        AtomicReference<String> textRef = new AtomicReference<>();
        exchange.getRequestReceiver().receiveFullString((ex,str) -> {
            textRef.set(str);
        }, (ex,err) ->{
            String errMessage = "Error reading Request body full string";
            LOGGER.error(errMessage,err);
            throw new RedishException(errMessage,err);
        }, Charset.forName(exchange.getRequestCharset()));
        return textRef.get();
    }

    /*
    @Override
    public void form(BiConsumer<HttpFormData, RuntimeException> callback) {
        this.assertCallbackNotNull(callback);
        if(contentType!= ContentType.FORM_DATA_MULTIPART && contentType!=ContentType.FORM_DATA_URL_ENCODED)
            callback.accept(null,invalidContentTypeException());
        try {
            LOGGER.info("Parsing Form data payload");
            FormParserFactory.Builder builder = FormParserFactory.builder();
            final FormDataParser formDataParser = builder.build().createParser(exchange);
            if (formDataParser != null) {
                formDataParser.parse(new HttpHandler() {
                    @Override
                    public void handleRequest(HttpServerExchange httpServerExchange) {
                        io.undertow.server.handlers.form.FormData formData = exchange.getAttachment(FormDataParser.FORM_DATA);
                        callback.accept(new FormDataImpl(formData),null);
                    }
                });
            }
        }catch (RuntimeException ex){
            callback.accept(null,ex);
        }catch (Exception ex){
            callback.accept(null,new StubServerException("error found parsing form data",
                    ex));
        }
    }*/

    private void assertCallbackNotNull(BiConsumer callback){
        if(callback==null)
            throw new IllegalArgumentException("callback must not be null");
    }

    private InvalidContentTypeException invalidContentTypeException(){
        return new InvalidContentTypeException(contentTypeHeader);
    }



}
