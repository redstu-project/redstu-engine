package org.mock.redstu.server.req.proxy;

import org.graalvm.polyglot.Value;
import org.graalvm.polyglot.proxy.ProxyArray;
import org.mock.redstu.api.common.Promise;
import org.mock.redstu.api.req.HttpReqBody;
import org.mock.redstu.js.AbstractProxyObject;
import org.mock.redstu.js.MapProxyHelper;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class HttpReqBodyProxy extends AbstractProxyObject {

    private HttpReqBody reqBody;

    public HttpReqBodyProxy(HttpReqBody reqBody){
        this.reqBody = reqBody;
        readable("contentType",() -> Value.asValue(reqBody.getContentType()));
        executable("text",this::text);
        executable("form",this::form);
        executable("object",this::object);
        executable("array",this::array);
        executable("binary",this::binary);
    }

    private Promise binary(Value... args){
        return (accept, reject) -> reqBody.binary((content, err) -> {
            if(err!=null)
                reject.executeVoid(err);
            else accept.executeVoid(new BinaryContentProxy(content));
        });
    }

    private Promise text(Value... args){
        return (accept, reject) -> reqBody.text((content, err) -> {
            if(err!=null)
                reject.executeVoid(err);
            else accept.executeVoid(content);
        });
    }

    private Promise form(Value... args){
        return (accept, reject) -> reqBody.form((content, err) -> {
            if(err!=null)
                reject.executeVoid(err);
            else accept.executeVoid(new FormDataProxy(content));
        });
    }

    private Promise object(Value... args){
        return (accept, reject) -> reqBody.object((content, err) -> {
            if(err!=null)
                reject.executeVoid(err);
            else accept.executeVoid(MapProxyHelper.proxyRecursive(content));
        });
    }

    private Promise array(Value... args){
        return (accept, reject) -> reqBody.array((array, err) -> {
            if(err!=null)
                reject.executeVoid(err);
            else {
                List<Object> proxiedList = new ArrayList<>();
                for(Map<String,Object> map: array){
                    proxiedList.add(MapProxyHelper.proxyRecursive(map));
                }
                accept.executeVoid(ProxyArray.fromList(proxiedList));
            }
        });
    }




}
