package org.mock.redstu.server.req.proxy;

import org.graalvm.polyglot.Value;
import org.graalvm.polyglot.proxy.ProxyObject;
import org.mock.redstu.api.common.Header;
import org.mock.redstu.api.common.HttpCookie;
import org.mock.redstu.api.req.HttpParam;
import org.mock.redstu.api.req.HttpReq;
import org.mock.redstu.js.AbstractProxyObject;

import java.util.HashMap;
import java.util.Map;

public class HttpRequestProxy extends AbstractProxyObject {

    private HttpReq request;

    private ProxyObject proxiedHeaders;
    private ProxyObject proxiedCookies;
    private ProxyObject proxiedBody;

    private ProxyObject queryParams;
    private ProxyObject pathParams;

    public HttpRequestProxy(HttpReq request){
        this.request = request;
        readable("method",() -> Value.asValue(request.getMethod()));
        readable("charset", () -> Value.asValue(request.getCharset()));
        readable("urlPattern", () -> Value.asValue(request.getUrlPattern()));
        readable("url", () -> Value.asValue(request.getUrl()));
        readable("path", () -> Value.asValue(request.getRequestPath()));
        readable("queryString",() -> Value.asValue(request.getQueryString()));
        readable("clientAddr", () -> Value.asValue(request.getClientAddr()));
        readable("headers", this::headers);
        readable("queryParams", this::queryParams );
        readable("pathParams", this::pathParams);
        readable("cookies",this::cookies);
        readable("body",this::body);
    }

    public HttpReq getObject() {
        return request;
    }

    private Object queryParams(){
        if(queryParams==null)
            queryParams = ProxyObject.fromMap(proxyParams(request.queryParams()));
        return queryParams;
    }

    private Object pathParams(){
        if(pathParams==null)
            pathParams = ProxyObject.fromMap(proxyParams(request.pathParams()));
        return pathParams;
    }

    private Map<String,Object> proxyParams(Map<String, HttpParam> paramMap){
        Map<String,Object> proxied = new HashMap<>();
        for(Map.Entry<String,HttpParam> entry: paramMap.entrySet()){
            proxied.put(entry.getKey(),new HttpParamProxy(entry.
                    getValue()));
        }
        return proxied;
    }

    private Object headers(){
        if(proxiedHeaders==null){
            Map<String,Object> map = new HashMap<>();
            for(Map.Entry<String, Header> entry: request.headers().entrySet()){
                map.put(entry.getKey(), new HeaderProxy(entry.
                        getValue()));
            }
            proxiedHeaders = ProxyObject.fromMap(map);
        }
        return proxiedHeaders;
    }

    private Object cookies(){
        if(proxiedCookies==null){
            Map<String,Object> map = new HashMap<>();
            for(Map.Entry<String, HttpCookie> entry: request.cookies().entrySet()){
                map.put(entry.getKey(),new HttpCookieProxy (entry.
                        getValue()));
            }
            proxiedCookies = ProxyObject.fromMap(map);
        }
        return proxiedCookies;
    }

    private Object body(){
        if(proxiedBody==null)
            proxiedBody = new HttpReqBodyProxy(request.body());
        return proxiedBody;
    }


}
