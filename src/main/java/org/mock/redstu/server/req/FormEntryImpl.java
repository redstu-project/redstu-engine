package org.mock.redstu.server.req;

import io.undertow.server.handlers.form.FormData;
import org.mock.redstu.api.req.FormEntry;
import org.mock.redstu.api.req.FormValue;

import java.util.ArrayList;
import java.util.Collection;

public class FormEntryImpl implements FormEntry {

    private String name;
    private Collection<FormValue> formValues = new ArrayList<>();

    public FormEntryImpl(String name, Collection<FormData.FormValue> values){
        this.name = name;
        for(FormData.FormValue value: values){
            formValues.add(new FormValueImpl(value));
        }
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public boolean isMultiValues() {
        return formValues.size()>1;
    }

    @Override
    public FormValue getValue() {
        return formValues.iterator().next();
    }

    @Override
    public FormValue[] getArray() {
        if(!isMultiValues())
            return new FormValue[]{};
        FormValue[] array = new FormValue[formValues.size()];
        formValues.toArray(array);
        return array;
    }
}
