package org.mock.redstu.server.req.proxy;

import org.graalvm.polyglot.Value;
import org.mock.redstu.api.req.HttpParam;
import org.mock.redstu.js.AbstractProxyObject;

public class HttpParamProxy extends AbstractProxyObject {

    private HttpParam param;

    public HttpParamProxy(HttpParam param){
        this.param = param;
        this.executable("string",(args) -> Value.asValue(param.string()));
        this.executable("boolean",(args) -> Value.asValue(param.asBool()));
        this.executable("number",this::number);
    }

    private Value number(Value... values){
        if(param.string().contains("."))
            return Value.asValue(param.asDouble());
        else return Value.asValue(param.asInt());
    }

}
