package org.mock.redstu.server.req;

import io.undertow.server.HttpServerExchange;
import io.undertow.server.handlers.Cookie;
import io.undertow.server.handlers.CookieImpl;
import io.undertow.util.HeaderMap;
import io.undertow.util.HeaderValues;
import io.undertow.util.HttpString;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jetbrains.annotations.Nullable;
import org.mock.redstu.api.common.Header;
import org.mock.redstu.api.common.HttpCookie;
import org.mock.redstu.api.req.HttpParam;
import org.mock.redstu.api.req.HttpReq;
import org.mock.redstu.api.req.HttpReqBody;
import org.mock.redstu.server.RedishException;
import org.mock.redstu.server.commons.HttpPath;
import org.mock.redstu.server.commons.PathParams;
import org.mock.redstu.api.common.HttpCookieImpl;

import java.util.*;

public class HttpRequestImpl implements HttpReq {

    private HttpPath httpPath;
    private PathParams pathParams;
    private HttpServerExchange exchange;

    private Map<String, Header> headers;
    private Map<String,HttpParam> queryParamsMap;
    private Map<String,HttpParam> pathParamsMap;
    private HttpReqBody reqBody;

    private static final Logger LOGGER = LogManager.getLogger(HttpRequestImpl.class);

    public HttpRequestImpl(HttpServerExchange exchange, @Nullable HttpPath path){
        this.exchange = exchange;
        if(path!=null)
            this.setPath(path);
        HeaderMap headerMap = exchange.getRequestHeaders();
        HeaderValues headerValues = headerMap.get("Content-Type");
        if(headerValues!=null)
            this.reqBody = new HttpReqBodyImpl(exchange);
    }

    @Override
    public String getMethod() {
        return exchange.getRequestMethod().toString();
    }

    @Override
    public String getCharset() {
        return exchange.getRequestCharset();
    }

    @Override
    public String getUrlPattern() {
        return httpPath.getPath();
    }

    @Override
    public String getUrl() {
        return exchange.getRequestURL();
    }

    @Override
    public String getRequestPath() {
        return exchange.getRequestPath();
    }

    @Override
    public String getQueryString() {
        return exchange.getQueryString();
    }

    @Override
    public String getClientAddr() {
        //TODO: Consider reverse proxies
        return exchange.getConnection().getPeerAddress().toString();
    }

    @Override
    public Map<String, Header> headers() {
        if(headers!=null)
            return Collections.unmodifiableMap(headers);
        headers = new HashMap<>();
        HeaderMap headerMap = exchange.getRequestHeaders();
        Collection<HttpString> headerNames = headerMap.getHeaderNames();
        for(HttpString name: headerNames){
            LOGGER.debug("Found Request header: {}",name);
            headers.put(name.toString(), new Header(headerMap.get(name)));
        }
        return Collections.unmodifiableMap(headers);
    }

    @Override
    public Map<String, HttpParam> queryParams() {
        if(queryParamsMap !=null)
            return Collections.unmodifiableMap(queryParamsMap);
        queryParamsMap = new HashMap<>();
        Map<String,Deque<String>> exchangeQueryParams = exchange.getQueryParameters();
        Set<String> keySet = exchangeQueryParams.keySet();
        for(String key: keySet) {
            queryParamsMap.put(key, new HttpParamImpl(exchangeQueryParams.get(key).
                    getLast()));
        }
        return Collections.unmodifiableMap(queryParamsMap);
    }

    @Override
    public Map<String,HttpParam> pathParams() {
        if(pathParamsMap==null){
            pathParamsMap = new HashMap<>();
            for(Map.Entry<String,String> entry: pathParams.toMap().entrySet()){
                pathParamsMap.put(entry.getKey(), new HttpParamImpl(entry.getValue()));
            }
        }
        return pathParamsMap;
    }

    @Override
    public Map<String, HttpCookie> cookies() {
        Map<String, HttpCookie> cookieMap= new HashMap<>();
        for(Cookie cookie : exchange.requestCookies()){
            cookieMap.put(cookie.getName(), new HttpCookieImpl(cookie));
        }
        return Collections.unmodifiableMap(cookieMap);
    }

    public void setPath(HttpPath path){
        if(path==null)
            throw new IllegalArgumentException("path must not be null");
        this.pathParamsMap = null;
        this.httpPath = path;
        this.pathParams = this.httpPath.getParams(exchange.getRequestPath()).orElseThrow(() -> new RedishException("failed to match Request Path"));
    }

    @Override
    public HttpReqBody body() {
        return this.reqBody;
    }

}
