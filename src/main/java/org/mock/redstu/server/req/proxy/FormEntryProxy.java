package org.mock.redstu.server.req.proxy;

import org.graalvm.polyglot.Value;
import org.graalvm.polyglot.proxy.ProxyArray;
import org.mock.redstu.api.req.FormEntry;
import org.mock.redstu.api.req.FormValue;
import org.mock.redstu.js.*;

import java.util.ArrayList;
import java.util.List;

public class FormEntryProxy extends AbstractProxyObject {

    private FormEntry formEntry;
    private Object proxiedArray = null;

    public FormEntryProxy(FormEntry formEntry){
        this.formEntry = formEntry;
        readable("name",() -> Value.asValue(formEntry.getName()));
        readable("multiValue",() -> Value.asValue(formEntry.isMultiValues()));
        readable("value",() -> Value.asValue(formEntry.getValue()));
        readable("array",this::array);
    }

    private Object array(){
        if(!formEntry.isMultiValues())
            return Value.asValue(null);
        if(proxiedArray==null){
            FormValue[] values = formEntry.getArray();
            List<Object> proxiedList = new ArrayList<>();
            for(FormValue formValue: values){
                proxiedList.add(new FormValueProxy(formValue));
            }
            proxiedArray = ProxyArray.fromList(proxiedList);
        }
        return proxiedArray;
    }

}
