package org.mock.redstu.server.req;

import org.mock.redstu.server.RedishException;

public class HttpRequestException extends RedishException {

    public HttpRequestException(String message) {
        super(message);
    }

    public HttpRequestException(String message, Throwable cause) {
        super(message, cause);
    }
}
