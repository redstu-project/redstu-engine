package org.mock.redstu.server.req.proxy;

import org.graalvm.polyglot.Value;
import org.mock.redstu.api.common.BinaryContent;
import org.mock.redstu.api.common.Promise;
import org.mock.redstu.js.AbstractProxyObject;

public class BinaryContentProxy extends AbstractProxyObject {

    private BinaryContent content;

    public BinaryContentProxy(BinaryContent content){
        super();
        this.content = content;
        readable("size",this::size);
        executable("saveAsTempFile",this::saveAsTempFile);
        executable("hexEncode",this::hexEncode);
        executable("base64Encode",this::base64Encode);
        executable("toArray",this::toArray);
    }

    private Value size(){
        return Value.asValue(content.size());
    }

    private Promise saveAsTempFile(Value...args){
        return (accept, reject) -> content.saveAsTempFile((file, err) -> {
            if(err!=null)
                reject.executeVoid(err);
            else accept.executeVoid(file);
        });
    }

    private Value hexEncode(Value...args){
        return Value.asValue(content.hexEncode());
    }

    private Value base64Encode(Value... args){
        return Value.asValue(content.base64Encode());
    }

    private Value toArray(Value... args){
        Object[] convertedByteArray = new Object[content.size()];
        byte[] byteArray = content.toArray();
        for(int i=0; i<byteArray.length;i++){
            convertedByteArray[i] = Value.asValue(
                    byteArray[i]);
        }
        return Value.asValue(convertedByteArray);
    }

}
