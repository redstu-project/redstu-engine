package org.mock.redstu.server.req;

import io.undertow.util.HexConverter;
import org.apache.commons.io.FileUtils;
import org.mock.redstu.api.common.BinaryContent;

import java.io.File;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.file.Files;
import java.util.Base64;
import java.util.function.BiConsumer;

public class BinaryContentImpl implements BinaryContent {

    private byte[] data;

    public BinaryContentImpl(ByteBuffer buffer){
        this.data = buffer.array();
    }

    public BinaryContentImpl(byte[] data){
        this.data = data;
    }

    @Override
    public String hexEncode() {
        return HexConverter.convertToHexString(data);
    }

    @Override
    public String base64Encode() {
        return Base64.getEncoder().withoutPadding().encodeToString(data);
    }

    @Override
    public void saveAsTempFile(BiConsumer<File,RuntimeException> callback) {
        try {
            File temp = Files.createTempFile("red", "stu").toFile();
            FileUtils.writeByteArrayToFile(temp, data);
            callback.accept(temp,null);
        }catch (IOException ex){
            callback.accept(null,new RuntimeException("error saving temporary file",
                    ex));
        }
    }

    @Override
    public int size() {
        return data.length;
    }

    @Override
    public byte[] toArray() {
        return data;
    }

}
