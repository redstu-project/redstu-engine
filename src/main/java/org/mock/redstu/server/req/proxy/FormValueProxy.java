package org.mock.redstu.server.req.proxy;


import org.graalvm.polyglot.Value;
import org.graalvm.polyglot.proxy.ProxyArray;
import org.graalvm.polyglot.proxy.ProxyObject;
import org.mock.redstu.api.common.Header;
import org.mock.redstu.api.common.Promise;
import org.mock.redstu.api.req.FormValue;
import org.mock.redstu.js.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class FormValueProxy extends AbstractProxyObject {

    private FormValue formValue;

    private ProxyObject proxiedHeaders;

    public FormValueProxy(FormValue formValue){
        super();
        this.formValue = formValue;
        readable("headers",this::headers);
        readable("fileUpload",() -> Value.asValue(formValue.isFileUpload()));
        readable("contentType",() -> Value.asValue(formValue.getContentType()));
        readable("charset",() -> Value.asValue(formValue.getCharset()));
        readable("filename",() -> Value.asValue(formValue.getFilename()));
        executable("number",this::getNumber);
        executable("boolean",args -> Value.asValue(formValue.getBool()));
        executable("string",args -> Value.asValue(formValue.getString()));
        executable("binary",args -> new BinaryContentProxy(formValue.getBinary()));
        executable("parseObject",this::parseObject);
        executable("parseArray",this::parseArray);
    }

    private Value getNumber(Value... values){
        if(formValue.getString().contains("."))
            return Value.asValue(formValue.getDecimal());
        else return Value.asValue(formValue.getInteger());
    }

    private ProxyObject headers(){
        if(proxiedHeaders==null){
            Map<String, Header> headerMap = formValue.getHeaders();
            Map<String,Object> proxyMap = new HashMap<>();
            for(Map.Entry<String,Header> entry: headerMap.entrySet()){
                proxyMap.put(entry.getKey(), new HeaderProxy(entry.getValue()));
            }
            proxiedHeaders = ProxyObject.fromMap(proxyMap);
        }
        return proxiedHeaders;
    }


    private Promise parseObject(Value... args){
        return (accept, reject) -> formValue.parseObject(((object, err) -> {
            if(err!=null)
                reject.executeVoid(err);
            else {
                accept.executeVoid(MapProxyHelper.proxyRecursive(object));
            }
        }));
    }

    private Promise parseArray(Value... args){
        return (accept, reject) -> formValue.parseArray(((maps, err) -> {
            if(err!=null)
                reject.executeVoid(err);
            else {
                List<Object> items = new ArrayList<>();
                for(Map<String,Object> map: maps){
                    items.add(MapProxyHelper.proxyRecursive(
                            map));
                }
                accept.executeVoid(ProxyArray.fromList(items));
            }
        }));
    }

}
