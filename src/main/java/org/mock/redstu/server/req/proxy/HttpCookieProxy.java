package org.mock.redstu.server.req.proxy;

import org.graalvm.polyglot.Value;
import org.graalvm.polyglot.proxy.ProxyTime;
import org.mock.redstu.api.common.HttpCookie;
import org.mock.redstu.js.AbstractProxyObject;

import java.time.LocalTime;

public class HttpCookieProxy extends AbstractProxyObject {

    public HttpCookieProxy(HttpCookie cookie){
        readable("name",() -> Value.asValue(cookie.getName()));
        readable("value", () -> Value.asValue(cookie.getValue()));
        readable("expires", () -> ProxyTime.from(LocalTime.from(cookie.getExpires())));
        readable("secured", () -> Value.asValue(cookie.isSecured()));
        readable("httpOnly", () -> Value.asValue(cookie.isHttpOnly()));
        readable("domain", () -> Value.asValue(cookie.getDomain()));
        readable("path", () -> Value.asValue(cookie.getPath()   ));
        readable("sameSite", () -> Value.asValue(cookie.getSameSiteMode()));
        readable("maxAge", () -> Value.asValue(cookie.getMaxAge()));
        readable("comment", () -> Value.asValue(cookie.getComment()));
    }

}
