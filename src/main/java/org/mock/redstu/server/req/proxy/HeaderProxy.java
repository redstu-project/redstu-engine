package org.mock.redstu.server.req.proxy;

import org.graalvm.polyglot.Value;
import org.mock.redstu.api.common.Header;
import org.mock.redstu.js.AbstractProxyObject;

public class HeaderProxy extends AbstractProxyObject {

    private Header header;

    public HeaderProxy(Header header){
        super();
        this.header = header;
        readable("value",() -> Value.asValue(header.value()));
        readable("array",() -> Value.asValue(header.toArray()));
        readable("multiValue",() -> Value.asValue(header.isMultiValue()));
        executable("contains",this::contains);
    }

    private Value contains(Value... args){
        if(args==null||args.length==0)
            throw new IllegalArgumentException("one argument is required");
        Value token = args[0];
        return Value.asValue(header.contains(token.asString()));
    }

}
