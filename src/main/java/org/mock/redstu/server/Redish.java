package org.mock.redstu.server;

import io.undertow.Undertow;
import io.undertow.server.HttpHandler;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jetbrains.annotations.Nullable;
import org.mock.redstu.api.filter.HttpFilter;
import org.mock.redstu.api.req.HttpReq;
import org.mock.redstu.api.resp.HttpResp;
import org.mock.redstu.server.config.ServerConfig;
import org.mock.redstu.server.commons.HttpPath;
import org.mock.redstu.server.commons.Verb;
import org.mock.redstu.server.exchange.ServerContext;
import org.mock.redstu.server.exchange.simple.SimpleExchangeContext;
import org.mock.redstu.server.exchange.simple.SimpleExchangeHandler;
import org.mock.redstu.server.exchange.sse.SseExchangeHandler;
import org.mock.redstu.server.filter.FilterRegistry;
import org.mock.redstu.server.undertow.CorsHandler;
import org.mock.redstu.server.undertow.EntryHandler;
import org.mock.redstu.server.undertow.FiltersHandler;
import org.mock.redstu.server.undertow.RespondersHandler;

import java.util.*;
import java.util.function.BiConsumer;
import java.util.function.Consumer;

/**
 * A deadly simple Web server, not meant for production usage.
 */
public class Redish {

    public static final String NAME_THREAD_CONTEXT_KEY = "serverName";

    private ServerConfig config;
    private Undertow undertow;
    private Map<String,Object> attachments = new HashMap<>();
    private RespondersHandler respondersHandler;
    private EntryHandler entryHandler;
    private boolean active = false;
    private String name;

    private FilterRegistry filterRegistry = new FilterRegistry();
    private static final Logger LOGGER = LogManager.getLogger(Redish.class);

    private static Redish CURRENT = null;

    public Redish(String name, ServerConfig config){
        if(name==null||name.isEmpty())
            throw new IllegalArgumentException("name must not be null nor empty");
        if(config==null)
            throw new IllegalArgumentException("config must not be null");
        this.name = name;
        this.config = config;
        this.respondersHandler = new RespondersHandler();
        HttpHandler filtersHandler = new FiltersHandler(filterRegistry,respondersHandler);
        HttpHandler corsHandler = new CorsHandler(config.getCorsConfig(),filtersHandler);
        this.entryHandler = new EntryHandler(name, corsHandler);
        undertow = Undertow.builder()
                .addHttpListener(config.getPort(), config.getHostname())
                .setHandler(entryHandler)
                .build();
    }

    public void addFilter(@Nullable List<Verb> verbs, @Nullable String path, HttpFilter filter){
        this.filterRegistry.addFilter(verbs,path,filter);
    }

    public void respondRequest(Verb verb, String path, Consumer<ServerContext> responder){
        if(path==null||path.isEmpty())
            throw new IllegalArgumentException("path must not be null nor empty");
        if(responder==null)
            throw new IllegalArgumentException("responder must not be null nor empty");
        HttpPath httpPath = new HttpPath(path);
        ResponderConfig responderConfig = new ResponderConfig(httpPath, ExchangeType.Simple,
                new SimpleExchangeHandler(httpPath,
                responder),verb);
        respondersHandler.addResponder(responderConfig);
    }

    public void respondRequest(Verb verb, String path, BiConsumer<HttpReq, HttpResp> callback){
        this.respondRequest(verb,path,context -> {
            SimpleExchangeContext exchangeContext = (SimpleExchangeContext) context;
            callback.accept(exchangeContext.getReq(), exchangeContext.
                    getResp());
        });
    }

    public void respondRequest(String path, Consumer<ServerContext> receiver){
        this.respondRequest(null,path,receiver);
    }

    public void respondEventSourceConnection(String path, Consumer<ServerContext> receiver){
        if(path==null||path.isEmpty())
            throw new IllegalArgumentException("path must not be null nor empty");
        HttpPath httpPath = new HttpPath(path);
        ResponderConfig responderConfig = new ResponderConfig(httpPath, ExchangeType.SSE, new SseExchangeHandler(
                httpPath, receiver));
        respondersHandler.addResponder(responderConfig);
    }

    public void start(){
        if(active)
            throw new IllegalStateException("already started");
        this.undertow.start();
        this.active = true;
    }

    public void stop(){
        if(!active)
            throw new IllegalStateException("server is not active");
        this.undertow.stop();
        this.active = false;
    }

    public String getHostname(){
        return config.getHostname();
    }

    public int getPort(){
        return config.getPort();
    }

    public String getUrl(){
        return String.format("http://%s:%d",getHostname(),getPort());
    }

    public boolean isActive(){
        return this.active;
    }

    public void putAttachment(String key, Object object){
        this.attachments.put(key,object);

    }

    public Object getAttachment(String key){
        return this.attachments.get(key);
    }

    public Map<String,Object> getAttachments(){
        return Collections.unmodifiableMap(attachments);
    }

    public Collection<ResponderInfo> getResponders(){

        return this.respondersHandler.getResponderInfos();

    }

    public String getName() {
        return name;
    }

    public  void setCurrent(){
        CURRENT = this;
    }

    public static Redish current(){
        if(CURRENT==null)
            throw new IllegalStateException("there is no current server");
        return CURRENT;
    }

}
