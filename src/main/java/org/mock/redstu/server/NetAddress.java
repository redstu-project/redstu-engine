package org.mock.redstu.server;

public class NetAddress {

    private String hostname;
    private int port;

    public NetAddress(String hostname, int port){
        if(hostname==null||hostname.isEmpty())
            throw new IllegalArgumentException("hostname must not be null nor empty");
        if(port<0)
            throw new IllegalArgumentException("port must be higher than zero");
        this.hostname = hostname;
        this.port = port;
    }

    public String getHostname() {
        return hostname;
    }

    public int getPort() {
        return port;
    }
}
