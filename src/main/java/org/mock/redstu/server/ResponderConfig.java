package org.mock.redstu.server;

import io.undertow.server.HttpServerExchange;
import org.mock.redstu.server.commons.HttpPath;
import org.mock.redstu.server.commons.ExchangeHandler;
import org.mock.redstu.server.commons.Verb;

public class ResponderConfig {

    private Verb verb;
    private ExchangeType type = ExchangeType.Simple;
    private HttpPath path;
    private ExchangeHandler handler;

    public ResponderConfig(HttpPath path, ExchangeType type, ExchangeHandler stubHandler){
        this(path,type,stubHandler,null);
    }

    public ResponderConfig(HttpPath path, ExchangeType type, ExchangeHandler stubHandler, Verb verb){
        this.path = path;
        this.type = type;
        this.handler = stubHandler;
        this.verb = verb;
    }

    public HttpPath getPath() {
        return path;
    }

    public Verb getVerb() {
        return verb;
    }

    public ExchangeType getType() {
        return type;
    }

    public boolean matches(HttpServerExchange exchange){
        boolean pathMatches = this.path.matches(exchange.getRequestPath());
        if(!pathMatches)
            return false;
        if(verb!=null){
            Verb parsedVerb = Verb.valueOf(exchange.getRequestMethod()
                    .toString()
                    .toUpperCase());
            return (parsedVerb==verb);
        }
        return true;
    }

    public void handleRequest(HttpServerExchange httpServerExchange) throws Exception {
        this.handler.handleRequest(httpServerExchange);
    }

    public ResponderInfo getInfo(){
        return new ResponderInfo(getPath().getPath(),
                getVerb());
    }

}
