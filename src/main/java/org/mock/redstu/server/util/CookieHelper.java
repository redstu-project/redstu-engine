package org.mock.redstu.server.util;

import io.undertow.server.handlers.Cookie;
import io.undertow.server.handlers.CookieImpl;
import org.mock.redstu.api.common.HttpCookie;

import java.util.Date;

public class CookieHelper {

    public static Cookie toUndertow(HttpCookie httpCookie){
        CookieImpl cookie = new CookieImpl(httpCookie.getName());
        cookie.setValue(httpCookie.getValue());
        if(httpCookie.getDomain()!=null)
            cookie.setDomain(httpCookie.getDomain());
        if(httpCookie.getPath()!=null)
            cookie.setPath(httpCookie.getPath());
        if(httpCookie.getExpires()!=null)
            cookie.setExpires(Date.from(httpCookie.getExpires()));
        if(httpCookie.getSameSiteMode()!=null)
            cookie.setSameSiteMode(httpCookie.getSameSiteMode());
        if(httpCookie.getComment()!=null)
            cookie.setComment(httpCookie.getComment());
        cookie.setHttpOnly(httpCookie.isHttpOnly());
        cookie.setSecure(httpCookie.isSecured());
        if(httpCookie.getMaxAge()<0)
            cookie.setMaxAge(httpCookie.getMaxAge());
        return cookie;
    }

}
