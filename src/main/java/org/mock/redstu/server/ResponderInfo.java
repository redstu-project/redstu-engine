package org.mock.redstu.server;

import org.mock.redstu.server.commons.Verb;

public class ResponderInfo
{
    private String path;
    private Verb verb;

    public ResponderInfo(String path, Verb verb){
        this.path = path;
        this.verb = verb;
    }

    public String getPath() {
        return path;
    }

    public Verb getVerb() {
        return verb;
    }
}
