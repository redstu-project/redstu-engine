package org.mock.redstu.server;

public class RedishJsonException extends RedishException {

    public RedishJsonException(String message) {
        super(message);
    }

    public RedishJsonException(String message, Throwable cause) {
        super(message, cause);
    }
}
