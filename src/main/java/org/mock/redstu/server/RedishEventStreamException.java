package org.mock.redstu.server;

public class RedishEventStreamException extends RedishException {

    public RedishEventStreamException(String message) {
        super(message);
    }

    public RedishEventStreamException(String message, Throwable cause) {
        super(message, cause);
    }

}
