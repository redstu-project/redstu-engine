package org.mock.redstu.server.undertow;

import io.undertow.server.HttpHandler;
import io.undertow.server.HttpServerExchange;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.mock.redstu.api.filter.HttpFilter;
import org.mock.redstu.server.RedishException;
import org.mock.redstu.server.commons.HttpPath;
import org.mock.redstu.server.filter.*;
import org.mock.redstu.server.req.HttpRequestImpl;
import org.mock.redstu.server.resp.HttpRespImpl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class FiltersHandler implements HttpHandler {

    private static final Logger LOGGER = LogManager.getLogger(FiltersHandler.class);
    private HttpHandler downstreamHandler;
    private FilterRegistry registry = null;
    private static final FilterKey GLOBAL_FILTER_KEY = new FilterKey();

    public FiltersHandler(FilterRegistry registry, HttpHandler downstreamHandler){
        if(registry==null)
            throw new IllegalArgumentException("registry must not be null");
        if(downstreamHandler ==null)
            throw new IllegalArgumentException("downstream handler must not be null");
        this.registry = registry;
        this.downstreamHandler = downstreamHandler;
    }


    @Override
    public void handleRequest(HttpServerExchange exchange) throws Exception {
        List<FilterDef> filterDefs = new ArrayList<>();

        registry.getEntries().forEach(entry -> {
            if(entry.getKey().matches(exchange)){
                HttpPath httpPath = entry.getKey().getPath();
                entry.getValue().forEach(item -> {
                    filterDefs.add(new FilterDef(item.getPosition(),httpPath,item.
                            getFilter()));
                });
            }
        });
        if(filterDefs.isEmpty()){
            LOGGER.debug("No filter definitions found");
            this.downstreamHandler.handleRequest(exchange);
        }else{
            LOGGER.debug("Found a total of {} filter definition(s)",filterDefs.size());
            Collections.sort(filterDefs);
            this.createChain(filterDefs,exchange).proceed();
        }
    }


    private FilterChainImpl createChain(List<FilterDef> filterDefs, HttpServerExchange exchange){
        LOGGER.debug("Creating filter chain");
        List<HttpFilter> filterList = filterDefs.stream().map(HttpFilterWrapper::new).collect(Collectors.toList());
        filterList.add((req, resp, chain) -> {
            try {
                LOGGER.debug("Passing control to downstream handler");
                downstreamHandler.handleRequest(exchange);
            } catch (Exception ex) {
               throw new RedishException("error found in Filtering downstream handler",
                       ex);
            }
        });
        return new FilterChainImpl(filterList,new HttpRequestImpl(exchange,null), new HttpRespImpl(
                exchange));
    }

}
