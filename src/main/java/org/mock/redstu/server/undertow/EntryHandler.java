package org.mock.redstu.server.undertow;

import io.undertow.server.HttpHandler;
import io.undertow.server.HttpServerExchange;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.ThreadContext;
import org.mock.redstu.server.Redish;

public class EntryHandler implements HttpHandler {

    private String name;
    private HttpHandler downstreamHandler;

    private static final Logger LOGGER = LogManager.getLogger(EntryHandler.class);

    public EntryHandler(String name, HttpHandler downstreamHandler){
        this.name = name;
        this.downstreamHandler = downstreamHandler;
    }

    @Override
    public void handleRequest(HttpServerExchange httpServerExchange) throws Exception {
        ThreadContext.put(Redish.NAME_THREAD_CONTEXT_KEY, name);
        try {
            this.downstreamHandler.handleRequest(httpServerExchange);
        }finally {
            ThreadContext.remove(Redish.NAME_THREAD_CONTEXT_KEY);
        }
    }
}
