package org.mock.redstu.server.undertow;

import io.undertow.server.HttpHandler;
import io.undertow.server.HttpServerExchange;
import io.undertow.util.HttpString;
import io.undertow.util.StatusCodes;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.mock.redstu.commons.CorsConfig;
import org.mock.redstu.server.Constants;

public class CorsHandler implements HttpHandler {

    private HttpHandler downstreamHandler;
    private static final Logger LOGGER = LogManager.getLogger(CorsHandler.class);

    private CorsConfig corsConfig;

    public CorsHandler(CorsConfig corsConfig, HttpHandler downstreamHandler){
        this.corsConfig = corsConfig;
        this.downstreamHandler = downstreamHandler;
    }

    @Override
    public void handleRequest(HttpServerExchange exchange) throws Exception {
        if (exchange.getRequestMethod().equalToString("OPTIONS")) {
            LOGGER.info("CORS Preflight Request");
            exchange.getResponseHeaders().add(HttpString.tryFromString(Constants.HEADER_ACCESS_CONTROL_ALLOW_ORIGIN), corsConfig.getAllowOrigin());
            exchange.getResponseHeaders().add(HttpString.tryFromString(Constants.HEADER_ACCESS_CONTROL_ALLOW_METHODS), corsConfig.getAllowMethods());
            exchange.getResponseHeaders().add(HttpString.tryFromString(Constants.HEADER_ACCESS_CONTROL_ALLOW_HEADERS), corsConfig.getAllowMethods());
            exchange.getResponseHeaders().add(HttpString.tryFromString(Constants.HEADER_ACCESS_CONTROL_MAX_AGE), corsConfig.getMaxAge());
            exchange.getResponseHeaders().add(HttpString.tryFromString(Constants.HEADER_ACCESS_CONTROL_EXPOSE_HEADERS), corsConfig.getExposeHeaders());
            exchange.setStatusCode(StatusCodes.ACCEPTED);
            exchange.getResponseSender().close();
        }else {
            exchange.getResponseHeaders().add(HttpString.tryFromString(Constants.HEADER_ACCESS_CONTROL_ALLOW_ORIGIN), corsConfig.getAllowOrigin());
            this.downstreamHandler.handleRequest(exchange);
        }
    }

}
