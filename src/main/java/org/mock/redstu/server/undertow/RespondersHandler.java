package org.mock.redstu.server.undertow;

import io.undertow.server.HttpHandler;
import io.undertow.server.HttpServerExchange;
import io.undertow.util.Headers;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.mock.redstu.server.ResponderConfig;
import org.mock.redstu.server.ResponderInfo;

import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

public class RespondersHandler implements HttpHandler {

    private List<ResponderConfig> responderConfigs = new ArrayList<>();
    private static final Logger LOGGER = LogManager.getLogger(RespondersHandler.class);

    public RespondersHandler(){
    }

    public void addResponder(ResponderConfig responderConfig){
        if(responderConfig==null)
            throw new IllegalArgumentException("responderConfig must not be null");
        this.responderConfigs.add(responderConfig);
    }

    public Collection<ResponderInfo> getResponderInfos(){

        return responderConfigs.stream().map(ResponderConfig::getInfo).collect(Collectors.
                toUnmodifiableList());

    }

    @Override
    public void handleRequest(HttpServerExchange exchange) throws Exception {
        LOGGER.info("{} {}",exchange.getRequestMethod().toString().toUpperCase(), exchange.getRequestPath());
        for (ResponderConfig stubConfig : responderConfigs) {
            if (stubConfig.matches(exchange)) {
                LOGGER.debug("Exchange type -> {}", stubConfig.getType());
                Instant start = Instant.now();
                stubConfig.handleRequest(exchange);
                Instant finish = Instant.now();
                long timeElapsed = Duration.between(start, finish).toMillis();
                LOGGER.info("Request handled. Took {} ms", timeElapsed);
                return;
            }
        }
        LOGGER.warn("No responder found. Responding with NOT FOUND");
        exchange.getResponseHeaders().put(Headers.STATUS, 404);
        exchange.setStatusCode(404);
        exchange.getResponseSender().close();
    }


}
