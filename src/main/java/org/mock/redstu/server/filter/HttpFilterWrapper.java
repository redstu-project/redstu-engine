package org.mock.redstu.server.filter;

import org.mock.redstu.api.filter.FilterChain;
import org.mock.redstu.api.filter.HttpFilter;
import org.mock.redstu.api.req.HttpReq;
import org.mock.redstu.api.resp.HttpResp;
import org.mock.redstu.server.req.HttpRequestImpl;

public class HttpFilterWrapper implements HttpFilter {

    private FilterDef def;

    public HttpFilterWrapper(FilterDef filterDef){
        this.def = filterDef;
    }

    @Override
    public void filterRequest(HttpReq req, HttpResp resp, FilterChain chain){
        HttpRequestImpl request = (HttpRequestImpl) req;
        request.setPath(this.def.getPath());
        this.def.getHandler().filterRequest(req,resp,chain);
    }

}
