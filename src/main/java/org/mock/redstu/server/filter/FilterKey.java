package org.mock.redstu.server.filter;


import io.undertow.server.HttpServerExchange;
import org.jetbrains.annotations.Nullable;
import org.mock.redstu.server.commons.HttpPath;
import org.mock.redstu.server.commons.Verb;


import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class FilterKey {

    private List<Verb> verbs = Arrays.asList(Verb.GET,Verb.POST,Verb.PUT,Verb.DELETE,Verb.HEAD,Verb.PATCH);
    private HttpPath path = new HttpPath(HttpPath.EVERYTHING);

    public FilterKey(){

    }

    public FilterKey(@Nullable List<Verb> verbs, @Nullable String path){
        if(verbs !=null)
            this.verbs = verbs;
        if(path!=null&&!path.isEmpty())
            this.path = new HttpPath(path);
    }

    public List<Verb> getVerbs() {
        return verbs;
    }

    public HttpPath getPath() {
        return path;
    }

    public boolean matches(HttpServerExchange exchange){
        if(exchange==null)
            throw new IllegalArgumentException("exchange must not be null");
        Verb verb = Verb.valueOf(exchange.getRequestMethod().toString().toUpperCase());
        if(!verbs.isEmpty()){
            if(!verbs.contains(verb)) {
                return false;
            }
        }
        return path.matches(exchange.getRequestPath());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        FilterKey filterKey = (FilterKey) o;

        if (!verbs.equals(filterKey.verbs)) return false;
        return path.equals(filterKey.path);
    }

    @Override
    public int hashCode() {
        int result = verbs.hashCode();
        result = 31 * result + path.hashCode();
        return result;
    }

    public String toString(){
        StringBuilder builder = new StringBuilder();
        if(verbs !=null) {
            builder.append(verbs.stream().map(Verb::name).collect(Collectors.joining("|")));
            builder.append(" ");
        }
        builder.append(path.getPath());
        return builder.toString();
    }


}
