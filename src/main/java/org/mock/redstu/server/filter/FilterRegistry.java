package org.mock.redstu.server.filter;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jetbrains.annotations.Nullable;
import org.mock.redstu.api.filter.HttpFilter;
import org.mock.redstu.server.Redish;
import org.mock.redstu.server.commons.Verb;

import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;

public class FilterRegistry {

    public static class FilterItem {

        private int position;
        private HttpFilter filter;

        FilterItem(int position, HttpFilter filter){
            this.position = position;
            this.filter = filter;
        }

        public int getPosition() {
            return position;
        }

        public void setPosition(int position) {
            this.position = position;
        }

        public HttpFilter getFilter() {
            return filter;
        }

        public void setFilter(HttpFilter filter) {
            this.filter = filter;
        }
    }

    private AtomicInteger counter = new AtomicInteger();
    private Map<FilterKey, Set<FilterItem>> httpFilterMap = new HashMap<>();
    private static final Logger LOGGER = LogManager.getLogger(Redish.class);

    public void addFilter(HttpFilter filter){
        if(filter==null)
            throw new IllegalArgumentException("filter must not be null");
        this.addFilter(new FilterKey(),filter);
    }

    public void addFilter(@Nullable List<Verb> verbs, @Nullable String path, HttpFilter filter){
        if(filter==null)
            throw new IllegalArgumentException("filter must not be null");
        this.addFilter(new FilterKey(verbs,path), filter);
    }

    private synchronized void addFilter(FilterKey filterKey, HttpFilter filter){
        LOGGER.info("Adding filter for {}",filterKey);
        Set<FilterItem> filterSet = httpFilterMap.get(filterKey);
        if(filterSet==null)
            filterSet = new HashSet<>();
        filterSet.add(new FilterItem(counter.incrementAndGet(), filter));
        httpFilterMap.put(filterKey,filterSet);
    }

    public Set<FilterKey> getFilterKeys(){
        return this.httpFilterMap.keySet();
    }

    public Set<Map.Entry<FilterKey,Set<FilterItem>>> getEntries(){
        return this.httpFilterMap.entrySet();
    }

    public Set<FilterItem> getFilters(FilterKey filterKey){
        if(filterKey==null)
            throw new IllegalArgumentException("filterKey must not be null");
        Set<FilterItem> set = httpFilterMap.get(filterKey);
        if(set==null)
            return Collections.emptySet();
        return set;
    }


}
