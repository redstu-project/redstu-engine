package org.mock.redstu.server.filter;

import org.jetbrains.annotations.NotNull;
import org.mock.redstu.api.filter.HttpFilter;
import org.mock.redstu.server.commons.HttpPath;

public class FilterDef implements Comparable<FilterDef> {

    private int chainPosition;
    private HttpPath path;
    private HttpFilter handler;

    public FilterDef(int chainPosition, HttpPath path, HttpFilter handler){
        this.chainPosition = chainPosition;
        this.path = path;
        this.handler = handler;
    }

    public HttpPath getPath() {
        return path;
    }

    public HttpFilter getHandler() {
        return handler;
    }

    @Override
    public int compareTo(@NotNull FilterDef o) {
        return Integer.compare(chainPosition,o.chainPosition);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        FilterDef def = (FilterDef) o;

        if (chainPosition != def.chainPosition) return false;
        if (!path.equals(def.path)) return false;
        return handler.equals(def.handler);
    }

    @Override
    public int hashCode() {
        int result = chainPosition;
        result = 31 * result + path.hashCode();
        result = 31 * result + handler.hashCode();
        return result;
    }
}
