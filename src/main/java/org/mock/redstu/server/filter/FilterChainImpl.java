package org.mock.redstu.server.filter;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.mock.redstu.api.filter.FilterChain;
import org.mock.redstu.api.filter.HttpFilter;
import org.mock.redstu.server.req.HttpRequestImpl;
import org.mock.redstu.server.resp.HttpRespImpl;

import java.util.Collection;
import java.util.Iterator;

public class FilterChainImpl implements FilterChain {

    private Iterator<HttpFilter> filterIterator = null;

    private HttpRequestImpl req;
    private HttpRespImpl resp;

    private static final Logger LOGGER = LogManager.getLogger(FilterChainImpl.class);

    public FilterChainImpl(Collection<HttpFilter> filterSet, HttpRequestImpl request, HttpRespImpl response){
        if(filterSet==null)
            throw new IllegalArgumentException("filter set must not be null");
        if(request==null)
            throw new IllegalArgumentException("request must not be null");
        if(response==null)
            throw new IllegalArgumentException("response must not be null");
        this.filterIterator = filterSet.iterator();
        this.req = request;
        this.resp = response;
    }

    @Override
    public void proceed() {
        if(!filterIterator.hasNext())
            throw new IllegalStateException("Already at the end of the chain");
        LOGGER.debug("Next filter");
        filterIterator.next().filterRequest(req,resp,this);
    }

}
