package org.mock.redstu.server.exchange.sse;

import io.undertow.server.HttpServerExchange;
import io.undertow.server.handlers.sse.ServerSentEventHandler;
import org.mock.redstu.server.commons.HttpPath;
import org.mock.redstu.server.commons.PathParams;
import org.mock.redstu.server.commons.ExchangeHandler;
import org.mock.redstu.server.RedishException;
import org.mock.redstu.server.exchange.ServerContext;
import org.mock.redstu.server.util.QueryParamsHelper;

import java.util.function.Consumer;

public class SseExchangeHandler extends ExchangeHandler {

    private ServerSentEventHandler serverSentEventHandler;

    public SseExchangeHandler(HttpPath path, Consumer<ServerContext> reactor) {
        super(path, reactor);
        this.serverSentEventHandler = new ServerSentEventHandler((connection,eventId) -> {
            HttpPath httpPath = getPath();
            PathParams pathParams = httpPath.getParams(connection.getRequestURI()).orElseThrow(() -> new RedishException("failed to match Request Path"));
            EventSessionImpl session = new EventSessionImpl(connection, QueryParamsHelper.toStringMap(connection.getQueryParameters()),pathParams.toMap());
            SseContext sseStubInput = new SseContext(session);
            SseExchangeHandler.this.getReaction(sseStubInput);
        });
    }

    @Override
    public void handleRequest(HttpServerExchange exchange) throws Exception {
        serverSentEventHandler.handleRequest(exchange);
    }
}
