package org.mock.redstu.server.exchange.sse;

import org.mock.redstu.api.sse.EventSession;
import org.mock.redstu.api.sse.EventContext;
import org.mock.redstu.api.sse.SseEvent;


public class EventContextImpl implements EventContext {

    private EventSession session;

    public EventContextImpl(EventSession eventSession){
        this.session = eventSession;
    }

    @Override
    public void broadcast(SseEvent event) {
        //TODO: Implement
        throw new UnsupportedOperationException();
    }

    @Override
    public EventSession[] getSessions() {
        //TODO: Implement
        throw new UnsupportedOperationException();
    }
}
