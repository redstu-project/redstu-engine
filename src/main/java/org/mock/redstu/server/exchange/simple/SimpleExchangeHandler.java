package org.mock.redstu.server.exchange.simple;

import io.undertow.server.HttpServerExchange;
import org.mock.redstu.server.commons.HttpPath;
import org.mock.redstu.server.commons.ExchangeHandler;
import org.mock.redstu.server.exchange.ServerContext;
import org.mock.redstu.server.req.HttpRequestImpl;
import org.mock.redstu.server.resp.HttpRespImpl;

import java.util.function.Consumer;

public class SimpleExchangeHandler extends ExchangeHandler {

    public SimpleExchangeHandler(HttpPath path, Consumer<ServerContext> reactor) {
        super(path, reactor);
    }

    @Override
    public void handleRequest(HttpServerExchange exchange) {
        HttpPath httpPath = getPath();
        getReaction(new SimpleExchangeContext(new HttpRequestImpl(exchange,httpPath),new HttpRespImpl(exchange)));
    }

}
