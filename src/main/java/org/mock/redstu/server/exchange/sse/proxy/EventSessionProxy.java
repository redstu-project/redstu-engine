package org.mock.redstu.server.exchange.sse.proxy;

import org.graalvm.polyglot.Value;
import org.graalvm.polyglot.proxy.ProxyObject;
import org.mock.redstu.api.req.HttpParam;
import org.mock.redstu.api.sse.EventBuilder;
import org.mock.redstu.api.sse.EventSession;
import org.mock.redstu.api.sse.SseEvent;
import org.mock.redstu.js.AbstractProxyObject;
import org.mock.redstu.server.req.proxy.HttpParamProxy;
import org.mock.redstu.util.JSON;

import java.util.HashMap;
import java.util.Map;

public class EventSessionProxy extends AbstractProxyObject {

    private EventSession session;

    private ProxyObject queryParams;
    private ProxyObject pathParams;

    public EventSessionProxy(EventSession session){
        this.session = session;
        readable("id",() -> Value.asValue(session.getId()));
        readable("pathParams",this::pathParams);
        readable("queryParams",this::queryParams);
        readable("closed",() -> Value.asValue(session.isClosed()));
        executable("close",this::close);
        executable("onClose",this::onClose);
        executable("emit",this::emit);
    }

    private Value emit(Value... args){
        if(args==null||args.length<1)
            throw new IllegalArgumentException("the event argument is required");
        EventBuilder eventBuilder = SseEvent.builder();
        Value event = args[0];
        if(!event.hasMember("data"))
            throw new IllegalArgumentException("data attribute is required");
        Value data = event.getMember("data");
        if(data.isString())
            eventBuilder.data(data.asString());
        else eventBuilder.data(JSON.string(data));
        if(event.hasMember("id"))
            eventBuilder.id(event.getMember("id").asString());
        if(event.hasMember("name"))
            eventBuilder.name(event.getMember("name").asString());
        session.emit(eventBuilder.build());
        return Value.asValue(null);
    }

    private Value close(Value... args){
        session.close();
        return null;
    }

    private Value onClose(Value... args){
        if(args==null||args.length<1)
            throw new IllegalArgumentException("one callback argument is required");
        Value callback = args[0];
        session.onClose((s -> {
            callback.executeVoid(this);
        }));
        return null;
    }

    private Object pathParams(){
        if(pathParams==null)
            pathParams = ProxyObject.fromMap(proxyParams(session.pathParams()));
        return pathParams;
    }

    private Object queryParams(){
        if(queryParams==null)
            queryParams = ProxyObject.fromMap(proxyParams(session.queryParams()));
        return queryParams;
    }

    private Map<String,Object> proxyParams(Map<String, HttpParam> paramMap){
        Map<String,Object> proxied = new HashMap<>();
        for(Map.Entry<String,HttpParam> entry: paramMap.entrySet()){
            proxied.put(entry.getKey(),new HttpParamProxy(entry.
                    getValue()));
        }
        return proxied;
    }

}
