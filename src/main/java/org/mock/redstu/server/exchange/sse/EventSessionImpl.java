package org.mock.redstu.server.exchange.sse;

import io.undertow.server.handlers.sse.ServerSentEventConnection;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.mock.redstu.api.req.HttpParam;
import org.mock.redstu.api.sse.SseEvent;
import org.mock.redstu.api.sse.EventSession;
import org.mock.redstu.server.RedishEventStreamException;
import org.mock.redstu.server.req.HttpParamImpl;
import org.mock.redstu.util.JSON;

import java.io.IOException;
import java.util.*;
import java.util.function.Consumer;

public class EventSessionImpl implements EventSession {

    private ServerSentEventConnection connection;
    private Map<String,HttpParam> queryParameters = null;
    private Map<String, HttpParam> pathParameters = null;
    private List<Consumer<EventSession>> closeCallbackList = new ArrayList<>();

    private boolean implicitlyClosed = false;
    private boolean closeEventFired = false;

    private static final Logger LOGGER = LogManager.getLogger(EventSessionImpl.class);

    public EventSessionImpl(ServerSentEventConnection connection, Map<String,String> queryParameters, Map<String,String> pathParameters){
        this.connection = connection;
        this.queryParameters = toHttpParams(queryParameters);
        this.pathParameters = toHttpParams(pathParameters);
        this.connection.addCloseTask((con -> {
            LOGGER.debug("Connection of session {} has been closed",getId());
            EventSessionImpl.this.fireCloseCallback();
        }));
    }

    private Map<String,HttpParam> toHttpParams(Map<String,String> map){
        Map<String,HttpParam> httpParams = new HashMap<>();
        for(Map.Entry<String,String> entry: map.entrySet()){
            httpParams.put(entry.getKey(),new HttpParamImpl(entry.
                    getValue()));
        }
        return httpParams;
    }

    @Override
    public String getId() {
        return UUID.randomUUID().toString();
    }

    @Override
    public void emit(SseEvent event) {
        assertNotClosed();
        String data = null;
        if(event.getData() instanceof String){
            data = (String) event.getData();
        }else JSON.string(event.getData());
        this.connection.send(data,event.getName(),event.getId(), new ServerSentEventConnection.EventCallback(){

            @Override
            public void done(ServerSentEventConnection serverSentEventConnection, String s, String s1, String s2) {
                LOGGER.debug("Event sent successfully");
            }

            @Override
            public void failed(ServerSentEventConnection serverSentEventConnection, String s, String s1, String s2, IOException e) {
                LOGGER.debug("Error sending event");
                EventSessionImpl.this.implicitlyClosed = true;
                EventSessionImpl.this.fireCloseCallback();
            }
        });
    }

    @Override
    public Map<String,HttpParam> pathParams() {
        return pathParameters;
    }

    @Override
    public Map<String,HttpParam> queryParams() {
        return queryParameters;
    }

    @Override
    public boolean isClosed() {
        return !connection.isOpen() || implicitlyClosed;
    }

    @Override
    public void onClose(Consumer<EventSession> callback) {
        assertNotClosed();
        if(callback==null)
            throw new IllegalArgumentException("callback must not be null");
        this.closeCallbackList.add(callback);
    }

    @Override
    public void close() {
        assertNotClosed();
        try {
            this.connection.close();
        }catch (IOException ex){
            throw new RedishEventStreamException("error closing session",ex);
        }
    }

    private void assertNotClosed(){
        if(!connection.isOpen())
            throw new IllegalArgumentException("session already closed");
    }

    private void fireCloseCallback(){
        if(closeEventFired)
            return;
        for(Consumer<EventSession> callback: closeCallbackList){
            callback.accept(this);
        }
        this.closeEventFired = true;
    }
}
