package org.mock.redstu.server.exchange.simple;

import org.mock.redstu.api.req.HttpReq;
import org.mock.redstu.api.resp.HttpResp;
import org.mock.redstu.server.exchange.ServerContext;

public class SimpleExchangeContext implements ServerContext {

    private HttpReq req;
    private HttpResp resp;

    public SimpleExchangeContext(HttpReq req, HttpResp resp){
        this.req = req;
        this.resp = resp;
    }

    public HttpReq getReq() {
        return req;
    }

    public HttpResp getResp() {
        return resp;
    }
}
