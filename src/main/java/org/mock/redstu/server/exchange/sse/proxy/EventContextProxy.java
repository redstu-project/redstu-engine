package org.mock.redstu.server.exchange.sse.proxy;

import org.graalvm.polyglot.Value;
import org.graalvm.polyglot.proxy.ProxyArray;
import org.mock.redstu.api.sse.EventBuilder;
import org.mock.redstu.api.sse.EventSession;
import org.mock.redstu.api.sse.EventContext;
import org.mock.redstu.api.sse.SseEvent;
import org.mock.redstu.js.AbstractProxyObject;
import org.mock.redstu.util.JSON;

import java.util.ArrayList;
import java.util.List;

public class EventContextProxy extends AbstractProxyObject {

    private EventContext context;

    public EventContextProxy(EventContext context){
        this.context = context;
        executable("broadcast",this::broadcast);
        readable("sessions",this::sessions);
    }

    private Object sessions(){
        List<Object> proxiedSessions = new ArrayList<>();
        for(EventSession session : context.getSessions())
            proxiedSessions.add(new EventSessionProxy(session));
        return ProxyArray.fromList(proxiedSessions);
    }

    private Value broadcast(Value... args){
        if(args==null||args.length<1)
            throw new IllegalArgumentException("the event data argument is required");
        EventBuilder eventBuilder = SseEvent.builder();
        Value dataArg = args[0];
        if(dataArg.isString()){
            eventBuilder.data(dataArg.asString());
        }else {
            eventBuilder.data(JSON.string(dataArg));
        }
        if(args.length>=2)
            eventBuilder.id(args[1].asString());
        if(args.length>=3)
            eventBuilder.name(args[2].asString());
        context.broadcast(eventBuilder.build());
        return Value.asValue(null);
    }


}
