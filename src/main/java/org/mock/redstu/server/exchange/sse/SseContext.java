package org.mock.redstu.server.exchange.sse;

import org.mock.redstu.api.sse.EventSession;
import org.mock.redstu.server.exchange.ServerContext;

public class SseContext implements ServerContext {

    private EventSession session;

    public SseContext(EventSession eventSession){
        this.session = eventSession;
    }

    public EventSession getSession() {
        return session;
    }

}
