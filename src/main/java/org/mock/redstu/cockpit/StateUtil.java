package org.mock.redstu.cockpit;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.graalvm.polyglot.Value;
import org.mock.redstu.js.JSObject;
import org.mock.redstu.proj.Deployment;

import java.util.HashMap;
import java.util.Map;

public class StateUtil {

    private Deployment deployment;

    StateUtil(Deployment deployment){
        this.deployment = deployment;
    }

    Map<String,Object> export(){
        Map<String,Object> state = deployment.getState();
        Map<String,Object> exported = new HashMap<>();
        for(Map.Entry<String,Object> entry: state.entrySet()){
            Value value = Value.asValue(entry.getValue());
            exported.put(entry.getKey(), JSObject.asHost(value));
        }
        return exported;
    }


}
