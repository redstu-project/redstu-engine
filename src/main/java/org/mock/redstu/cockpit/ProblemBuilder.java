package org.mock.redstu.cockpit;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class ProblemBuilder {

    private Map<String,Object> map = new HashMap<>();

    ProblemBuilder(){

    }

    public ProblemBuilder type(String type){
        if(type==null||type.isEmpty())
            throw new IllegalArgumentException("type must not be null nor empty");
        this.map.put("type",type);
        return this;
    }

    public ProblemBuilder status(int status){
        if(status<100||status>999)
            throw new IllegalArgumentException("status must not be less than 100 and higher than 999");
        this.map.put("status",status);
        return this;
    }

    public ProblemBuilder title(String title){
        if(title==null||title.isEmpty())
            throw new IllegalArgumentException("title must not be null nor empty");
        this.map.put("title",title);
        return this;
    }

    public ProblemBuilder detail(String detail){
        if(detail==null||detail.isEmpty())
            throw new IllegalArgumentException("detail must not be null nor empty");
        this.map.put("detail",detail);
        return this;
    }

    public Map<String,Object> build(){
        if(map.size()<4)
            throw new IllegalStateException("the following attributes must be set: type, status, title, detail");
        return Collections.unmodifiableMap(
                map);
    }

}
