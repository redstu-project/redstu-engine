package org.mock.redstu.cockpit.dto;

import org.mock.redstu.proj.ProjectManifest;
import org.mock.redstu.proj.manifest.Author;

public class ProjectInfo {

    private String name;
    private Author author;
    private String engine;

    public ProjectInfo(ProjectManifest manifest){
        this.name = manifest.getName();
        this.author = manifest.getAuthor();
        this.engine = manifest.getEngine();
    }

    public String getName() {
        return name;
    }

    public Author getAuthor() {
        return author;
    }

    public String getEngine() {
        return engine;
    }
}
