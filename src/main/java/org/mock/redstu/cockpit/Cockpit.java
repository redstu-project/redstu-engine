package org.mock.redstu.cockpit;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jetbrains.annotations.Nullable;
import org.mock.redstu.api.common.Header;
import org.mock.redstu.cockpit.dto.ProjectInfo;
import org.mock.redstu.proj.Deployment;
import org.mock.redstu.server.Redish;
import org.mock.redstu.server.commons.Verb;
import org.mock.redstu.server.config.ServerConfig;

public class Cockpit {

    private enum SecurityScheme {
        None,ApiKey
    }

    private Deployment deployment;
    private Redish server;
    private StateUtil stateUtil;
    private SecurityScheme securityScheme = SecurityScheme.None;

    private static final String API_KEY_HEADER = "key";
    private static final Logger LOGGER = LogManager.getLogger(Cockpit.class);
    private static final String SECURITY_SCHEME_PATH = "/servers/cockpit/security";

    public Cockpit(ServerConfig serverConfig, Deployment deployment, @Nullable String apiKey){
        if(serverConfig==null)
            throw new IllegalArgumentException("serverConfig must not be null nor empty");
        if(deployment==null)
            throw new IllegalArgumentException("deployment must not be null");
        this.server = new Redish("cockpit",serverConfig);
        if(apiKey!=null&&!apiKey.isEmpty()){
            securityScheme = SecurityScheme.ApiKey;
            this.server.addFilter(null,null, (req, resp, chain) -> {
                if(SECURITY_SCHEME_PATH.equals(req.getRequestPath())){
                    chain.proceed();
                }else {
                    Header header = req.headers().get(API_KEY_HEADER);
                    if (header != null) {
                        if (!apiKey.equals(header.value())) {
                            LOGGER.warn("Access Key doesn't match");
                            resp.setStatus(403);
                            resp.sendObject(Problem.builder().status(403)
                                    .title("Access forbidden")
                                    .type("/problem/forbidden")
                                    .detail("Operation is forbidden. Invalid access key")
                                    .build());
                        }else {
                            LOGGER.debug("Access key validation: OK");
                            chain.proceed();
                        }
                    } else {
                        LOGGER.warn("Access Key header missing");
                        resp.setStatus(401);
                        resp.sendObject(Problem.builder().status(403)
                                .title("Access Unauthorized")
                                .type("/problem/unauthorized")
                                .detail("You are required to provide the Access Key")
                                .build());
                    }
                }
            });
        }
        LOGGER.info("Cockpit security scheme: {}",securityScheme);
        this.deployment = deployment;
        this.stateUtil = new StateUtil(deployment);
        this.setup();
    }



    private void setup(){
        this.configureServerEndpoints();
        this.configureStateEndpoints();
        this.configureRespondersEndpoint();
        this.configureProjectEndpoints();
        this.configureSignalsEndpoints();
    }

    private void configureServerEndpoints(){
        this.server.respondRequest(Verb.GET, SECURITY_SCHEME_PATH,((req, resp) -> {
            resp.sendText(securityScheme.name());
        }));
    }

    private void configureSignalsEndpoints(){
        this.server.respondRequest(Verb.GET,"/signals",((req, resp) -> resp.sendObject(deployment.getSignals())));
        this.server.respondRequest(Verb.POST,"/signals/{name}",((req, resp) -> {
            String name = req.pathParams().get("name").string();
            req.body().object((object, err) -> {
                if(err!=null){
                    resp.setStatus(500);
                    resp.sendObject(Problem.serverError(err.getMessage()).
                            build());
                }else {
                    if(!deployment.emitSignal(name, object))
                        resp.setStatus(404);
                    else resp.setStatus(200);
                    resp.send();
                }
            });
        }));
    }

    private void configureProjectEndpoints(){
        this.server.respondRequest(Verb.GET,"/project",((req, resp) -> {
            resp.sendObject(new ProjectInfo(deployment.getProject().getManifest()));
        }));
    }

    private void configureStateEndpoints(){
        this.server.respondRequest(Verb.GET, "/state", (req, resp) -> {
            resp.sendObject(stateUtil.export());
        });
        this.server.respondRequest(Verb.PUT, "/state", (req, resp) -> {
            req.body().object((object, err) -> {
                if(err!=null){
                    LOGGER.error("Error reading object from request body",err);
                    resp.sendObject(Problem.serverError(err.getMessage()).
                            build());
                    return;
                }
                deployment.setState(object);
            });
        });
    }

    private void configureRespondersEndpoint(){
        this.server.respondRequest(Verb.GET,"/servers/mock/responders",((req, resp) -> {
            try {
                resp.sendObject(deployment.getMockServer().getResponders());
            }catch (RuntimeException ex){
                LOGGER.error("Error writing response",ex);
            }
        }));
    }

    public void activate(){
        if(isActive())
            throw new IllegalArgumentException("cockpit is already active");
        this.server.start();
    }

    public boolean isActive(){
        return server.isActive();
    }

    public void deactivate(){
        if(!isActive())
            throw new IllegalArgumentException("cockpit is not active");
        this.server.stop();
    }

}
