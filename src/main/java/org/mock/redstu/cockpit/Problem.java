package org.mock.redstu.cockpit;

public final class Problem {

    static ProblemBuilder builder(){
        return new ProblemBuilder();
    }

    static ProblemBuilder serverError(String detail){
        return builder()
                .status(500)
                .type("/problem/internal")
                .title("Internal server error")
                .detail(detail);
    }

}
