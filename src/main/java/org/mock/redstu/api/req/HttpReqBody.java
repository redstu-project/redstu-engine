package org.mock.redstu.api.req;

import org.mock.redstu.api.common.BinaryContent;

import java.util.Map;
import java.util.function.BiConsumer;

public interface HttpReqBody {

    String getContentType();
    void binary(BiConsumer<BinaryContent,RuntimeException> callback);
    void text(BiConsumer<String,RuntimeException> callback);
    void object(BiConsumer<Map<String,Object>,RuntimeException> callback);
    void array(BiConsumer<Map<String,Object>[],RuntimeException> callback);
    void form(BiConsumer<Map<String,FormEntry>,RuntimeException> callback);

}
