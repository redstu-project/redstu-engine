package org.mock.redstu.api.req;

public interface HttpParam {

    String string();
    Integer asInt();
    Boolean asBool();
    Double asDouble();

}
