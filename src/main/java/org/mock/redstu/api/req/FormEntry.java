package org.mock.redstu.api.req;

public interface FormEntry {

    String getName();
    boolean isMultiValues();
    FormValue getValue();
    FormValue[] getArray();

}
