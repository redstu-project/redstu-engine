package org.mock.redstu.api.req;

import org.mock.redstu.api.common.Header;
import org.mock.redstu.api.common.HttpCookie;

import java.util.Map;

public interface HttpReq {

    String getMethod();
    String getCharset();
    String getUrlPattern();
    String getUrl();
    String getRequestPath();
    String getQueryString();
    String getClientAddr();
    Map<String,Header> headers();
    Map<String, HttpParam> queryParams();
    Map<String, HttpParam> pathParams();
    Map<String,HttpCookie> cookies();
    HttpReqBody body();

}
