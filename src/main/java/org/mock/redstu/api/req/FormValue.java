package org.mock.redstu.api.req;


import org.mock.redstu.api.common.BinaryContent;
import org.mock.redstu.api.common.Header;

import java.util.Map;
import java.util.function.BiConsumer;

public interface FormValue {

    Map<String, Header> getHeaders();
    boolean isFileUpload();
    String getContentType();
    String getCharset();
    String getFilename();

    BinaryContent getBinary();
    String getString();
    Boolean getBool();
    Double getDecimal();
    Integer getInteger();
    void parseObject(BiConsumer<Map<String,Object>,RuntimeException> callback);
    void parseArray(BiConsumer<Map<String,Object>[],RuntimeException> callback);


}
