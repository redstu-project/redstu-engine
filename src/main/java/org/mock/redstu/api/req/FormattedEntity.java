package org.mock.redstu.api.req;

import java.util.Map;
import java.util.function.BiConsumer;

public interface FormattedEntity {

    String text();
    Map<String,Object> object(BiConsumer<Map<String, Object>, RuntimeException> callback);

}
