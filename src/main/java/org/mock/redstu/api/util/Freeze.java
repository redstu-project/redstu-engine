package org.mock.redstu.api.util;

import org.mock.redstu.server.RedishException;

import java.util.concurrent.TimeUnit;

public final class Freeze {

    public static void until(long time,TimeUnit unit){
        try {
            Thread.sleep(unit.toMillis(time));
        } catch (InterruptedException ex) {
            throw new RedishException("thread interrupted",ex);
        }
    }
}
