package org.mock.redstu.api.util;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;

public final class Repeat {

    private static final Timer TIMER = new Timer();

    private static Logger LOGGER = LogManager.getLogger(Repeat.class);

    public static void every(long time,TimeUnit unit, Consumer<Repetition> repetitionConsumer){
        TIMER.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                repetitionConsumer.accept(new Repetition() {
                    @Override
                    public void requestInterruption() {
                        LOGGER.debug("Repetition interruption requested");
                        cancel();
                    }
                });
            }
        },0,unit.toMillis(time));
    }

}
