package org.mock.redstu.api.util;

import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;

public class Postpone {

    private static final Timer TIMER = new Timer();

    public static void by(long time, TimeUnit unit, Runnable runnable){
        if(time<1)
            throw new IllegalArgumentException("time must not be less than 1");
        if(unit==null)
            throw new IllegalArgumentException("time unit must not be null");
        if(runnable==null)
            throw new IllegalArgumentException("runnable must not be null");
        TIMER.schedule(new TimerTask() {
            @Override
            public void run() {
                runnable.run();
            }
        },unit.toMillis(time));
    }

}
