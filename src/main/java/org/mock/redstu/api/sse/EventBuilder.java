package org.mock.redstu.api.sse;

public class EventBuilder {

    private EventImpl event;

    public EventBuilder(){
        this.event = new EventImpl();
    }

    public EventBuilder id(String id){
        this.event.setId(id);
        return this;
    }

    public EventBuilder data(Object data){
        this.event.setData(data);
        return this;
    }

    public EventBuilder name(String name){
        this.event.setName(name);
        return this;
    }

    public SseEvent build(){
        if(event.getData()==null)
            throw new IllegalStateException("data must be set");
        return this.event;
    }


}
