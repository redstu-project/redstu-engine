package org.mock.redstu.api.sse;

public interface SseEvent {

    String getName();
    String getId();
    Object getData();

    static SseEvent withData(Object data){
        return builder().data(data).build();
    }

    static EventBuilder builder(){
        return new EventBuilder();
    }

}
