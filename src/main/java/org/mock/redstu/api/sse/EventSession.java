package org.mock.redstu.api.sse;

import org.mock.redstu.api.req.HttpParam;

import java.util.Map;
import java.util.function.Consumer;

public interface EventSession {

    String getId();
    void emit(SseEvent event);
    Map<String, HttpParam> pathParams();
    Map<String,HttpParam> queryParams();
    boolean isClosed();
    void onClose(Consumer<EventSession> closeCallback);
    void close();

}
