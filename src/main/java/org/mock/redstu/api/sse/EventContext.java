package org.mock.redstu.api.sse;

public interface EventContext {

    void broadcast(SseEvent event);
    EventSession[] getSessions();

}
