package org.mock.redstu.api.sse;

public class EventImpl implements SseEvent {

    private String name;
    private String id;
    private Object data;

    public EventImpl(){
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public String getId() {
        return this.id;
    }

    @Override
    public Object getData() {
        return this.data;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setData(Object data) {
        this.data = data;
    }
}
