package org.mock.redstu.api.common;

import org.graalvm.polyglot.Value;

public interface Promise {

    void then(Value accept, Value reject);

}
