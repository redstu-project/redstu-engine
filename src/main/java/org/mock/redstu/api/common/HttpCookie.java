package org.mock.redstu.api.common;


import java.time.Instant;


public interface HttpCookie {

    String getName();
    String getValue();
    Instant getExpires();
    boolean isSecured();
    boolean isHttpOnly();
    String getDomain();
    String getPath();
    String getSameSiteMode();
    int getMaxAge();
    String getComment();

    static HttpCookie with(String name, String value){
        return new HttpCookieImpl(name,value);
    }

    static HttpCookieBuilder builder(String name, String value){
        return new HttpCookieBuilder(name,value);
    }

}
