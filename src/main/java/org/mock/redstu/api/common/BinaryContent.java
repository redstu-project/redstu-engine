package org.mock.redstu.api.common;


import java.io.File;
import java.util.function.BiConsumer;

public interface BinaryContent {

    String hexEncode();
    String base64Encode();
    void saveAsTempFile(BiConsumer<File,RuntimeException> callback);
    int size();
    byte[] toArray();

}
