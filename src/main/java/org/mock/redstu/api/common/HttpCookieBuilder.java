package org.mock.redstu.api.common;

import java.time.Instant;

public class HttpCookieBuilder {

    private HttpCookieImpl cookie;

    HttpCookieBuilder(String name, String value){
        this.cookie = new HttpCookieImpl(name,value);
    }

    public HttpCookieBuilder expires(Instant expires){
        this.cookie.setExpires(expires);
        return this;
    }

    public HttpCookieBuilder domain(String domain){
        this.cookie.setDomain(domain);
        return this;
    }

    public HttpCookieBuilder path(String path){
        this.cookie.setPath(path);
        return this;
    }

    public HttpCookieBuilder sameSiteMode(String mode){
        this.cookie.setSameSiteMode(mode);
        return this;
    }

    public HttpCookieBuilder httpOnly(boolean httpOnly){
        this.cookie.setHttpOnly(httpOnly);
        return this;
    }

    public HttpCookieBuilder secure(boolean secure){
        this.cookie.setSecured(secure);
        return this;
    }

    public HttpCookieBuilder maxAge(int maxAge){
        this.cookie.setMaxAge(maxAge);
        return this;
    }

    public HttpCookieBuilder comment(String comment){
        this.cookie.setComment(comment);
        return this;
    }

    public HttpCookie build(){
        return cookie;
    }

}
