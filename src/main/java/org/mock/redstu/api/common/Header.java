package org.mock.redstu.api.common;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class Header {

    private List<String> values = new ArrayList<>();

    public Header(Collection<String> collection){
        this.values.addAll(collection);
    }

    public String value(){
        return String.join(", ",values);
    }

    public boolean contains(String value){
       return values.stream().anyMatch(item -> item.equalsIgnoreCase(value));
    }


    public String[] toArray(){
        String[] array = new String[values.size()];
        values.toArray(array);
        return array;
    }

    public boolean isMultiValue(){
        return values.size() > 1;
    }

}
