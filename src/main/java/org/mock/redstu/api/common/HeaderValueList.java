package org.mock.redstu.api.common;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class HeaderValueList {

    private List<String> values = new ArrayList<>();

    public HeaderValueList(String value){
        if(value==null||value.isEmpty())
            throw new IllegalStateException("value must not be null nor empty");
        this.values.add(value);
    }

    public HeaderValueList(List<String> values){
        this.values.addAll(values);
    }

    public String getItem(int index){
        if ( index>=values.size() || index<0 )
            throw new IllegalArgumentException("index must be higher than -1 and less than the list size");
        return values.get(index);
    }

    public String[] array(){
        String[] items = new String[values.size()];
        values.toArray(items);
        return items;
    }

    public boolean isEmpty(){
        return this.values.isEmpty();
    }

    public boolean isSingle(){
        return this.values.size() == 1;
    }

    public String value(){
        if(isEmpty())
            return null;
        if(isSingle())
            return getItem(0);
        return values.get(values.size()-1);
    }

    public int getLength(){
        return this.values.size();
    }

    public List<String> getValues(){
        return Collections.unmodifiableList(this.values);
    }

}
