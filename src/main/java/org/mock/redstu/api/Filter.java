package org.mock.redstu.api;

import org.mock.redstu.api.filter.HttpFilter;
import org.mock.redstu.server.Redish;
import org.mock.redstu.server.commons.Verb;

import java.util.List;

public class Filter {

    public static void all(HttpFilter filter){
        if(filter==null)
            throw new IllegalArgumentException("filter must not be null");
        Redish.current().addFilter(null,null,filter);
    }

    public static void only(List<Verb> verbs, String path, HttpFilter filter){
        if(filter==null)
            throw new IllegalArgumentException("filter must not be null");
        Redish.current().addFilter(verbs,path,filter);
    }

}
