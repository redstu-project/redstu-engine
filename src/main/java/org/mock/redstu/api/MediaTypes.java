package org.mock.redstu.api;

public final class MediaTypes {
    public static final String APPLICATION_JSON = "application/json";
    public static final String APPLICATION_XML = "application/xml";
    public static final String APPLICATION_OCTET_STREAM = "application/octet-stream";
    public static final String TEXT_PLAIN = "text/plain";
    public static final String TEXT_HTML = "text/html";
    public static final String TEXT_EVENT_STREAM = "text/event-stream";
    public static final String FORM_URL_ENCODED = "application/x-www-form-urlencoded";
    public static final String FORM_MULTIPART_ENCODED = "multipart/form-data";
    public static final String APPLICATION_PROBLEM_XML = "application/problem+xml";
    public static final String APPLICATION_PROBLEM_JSON ="application/problem+json" ;

}
