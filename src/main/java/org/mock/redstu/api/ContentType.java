package org.mock.redstu.api;


public enum ContentType {

    FORM_DATA_MULTIPART("multipart/form-data"),
    FORM_DATA_URL_ENCODED("application/x-www-form-urlencoded"),
    OCTET_STREAM("application/octet-stream"),
    TEXT("text/"),
    APPLICATION_JSON("application/json"),
    APPLICATION_XML("application/xml"),
    OTHER("");

    private String value;

    ContentType(String value){
        this.value = value;
    }

    public boolean match(String value){
        return this.value.equalsIgnoreCase(value) || value.startsWith(this.value) || value.contains(this.value);
    }

    public static ContentType which(String value){
        for(ContentType type: ContentType.values()){
            if(type.match(value))
                return type;
        }
        return OTHER;
    }
}
