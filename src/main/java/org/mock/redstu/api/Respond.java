package org.mock.redstu.api;

import org.jetbrains.annotations.Nullable;
import org.mock.redstu.api.req.HttpReq;
import org.mock.redstu.api.resp.HttpResp;
import org.mock.redstu.api.sse.EventSession;
import org.mock.redstu.proj.Deployment;
import org.mock.redstu.proj.Signal;
import org.mock.redstu.server.commons.Verb;
import org.mock.redstu.server.Redish;
import org.mock.redstu.server.exchange.simple.SimpleExchangeContext;
import org.mock.redstu.server.exchange.sse.SseContext;

import java.util.Map;
import java.util.function.BiConsumer;
import java.util.function.Consumer;

public final class Respond {

    private static void request(@Nullable Verb verb, String path, BiConsumer<HttpReq, HttpResp> biConsumer){
        Redish.current().respondRequest(verb,path,(reactionInput -> {
            SimpleExchangeContext input = (SimpleExchangeContext) reactionInput;
            biConsumer.accept(input.getReq(),input.getResp());
        }));
    }

    public static void anyRequest(String path, BiConsumer<HttpReq, HttpResp> biConsumer){
        request(null,path,biConsumer);
    }

    public static void getRequest(String path, BiConsumer<HttpReq, HttpResp> biConsumer){
        request(Verb.GET,path,biConsumer);
    }

    public static void postRequest(String path, BiConsumer<HttpReq, HttpResp> biConsumer){
        request(Verb.POST,path,biConsumer);
    }

    public static void putRequest(String path, BiConsumer<HttpReq, HttpResp> biConsumer){
        request(Verb.PUT,path,biConsumer);
    }

    public static void patchRequest(String path, BiConsumer<HttpReq, HttpResp> biConsumer){
        request(Verb.PATCH,path,biConsumer);
    }

    public static void deleteRequest(String path, BiConsumer<HttpReq, HttpResp> biConsumer){
        request(Verb.DELETE,path,biConsumer);
    }

    public static void headRequest(String path, BiConsumer<HttpReq, HttpResp> biConsumer){
        request(Verb.HEAD,path,biConsumer);
    }

    public static void sseConnection(String path, Consumer<EventSession> receiver){
        Redish.current().respondEventSourceConnection(path, reactionInput -> {
            receiver.accept(((SseContext) reactionInput).getSession());
        });
    }

    public static void signal(Signal signal, Consumer<Map<String,Object>> handler){
        if(signal==null)
            throw new IllegalArgumentException("signal must not be null");
        if(handler==null)
            throw new IllegalArgumentException("handler must not be null");
        Deployment deployment = Deployment.current();
        deployment.addSignalListener(signal,handler);
    }

}
