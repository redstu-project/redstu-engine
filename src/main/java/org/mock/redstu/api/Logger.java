package org.mock.redstu.api;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

public class Logger {

    private List<Consumer<Event>> consumers = new ArrayList<>();

    private enum Level {
        Info, Error
    }

    public static class Event {
        private Level level = Level.Info;
        private String message;

        public Event(Level level, String message){
            this.level = level;
            this.message = message;
        }

        public Level getLevel() {
            return level;
        }

        public String getMessage() {
            return message;
        }
    }

    public void addConsumer(Consumer<Event> consumer){
        consumers.add(consumer);
    }

    public void removeConsumer(Consumer<Event> consumer){
        consumers.remove(consumer);
    }

    public void clearConsumers(){
        consumers.clear();
    }

    public void info(String message){
        consumers.forEach(it -> it.accept(new Event(Level.Info,message)));
    }

    public void error(String message){
        consumers.forEach(it -> it.accept(new Event(Level.Error,message)));
    }


}
