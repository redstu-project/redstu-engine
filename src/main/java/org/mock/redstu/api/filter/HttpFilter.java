package org.mock.redstu.api.filter;

import org.mock.redstu.api.req.HttpReq;
import org.mock.redstu.api.resp.HttpResp;

@FunctionalInterface
public interface HttpFilter {

    void filterRequest(HttpReq req, HttpResp resp, FilterChain chain);

}
