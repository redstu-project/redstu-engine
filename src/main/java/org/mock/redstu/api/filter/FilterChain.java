package org.mock.redstu.api.filter;

public interface FilterChain {

    void proceed();

}
