package org.mock.redstu.api.templating;

import java.util.Map;

public interface Template {

    String compile(Map<String,Object> data);

}
