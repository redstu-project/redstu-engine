package org.mock.redstu.api.resp;


import org.mock.redstu.api.common.HttpCookie;

import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.util.Collection;
import java.util.List;
import java.util.Map;


public interface HttpResp {

    void setHeader(String name, String... values);
    void setStatus(int status);
    void setContentType(String contentType);
    void setCookie(HttpCookie cookie);
    void sendBytes(byte[] bytes);
    void sendObject(Object map);
    void sendArray(Object[] array);
    void sendText(String text);
    void sendText(String text, Charset charset);
    void send();
}
