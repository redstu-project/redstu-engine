package org.mock.redstu.api.resp;

public final class SameSiteMode {

    public static final String LAX = "Lax";
    public static final String STRICT = "Strict";
    public static final String NONE = "NONE";

}
