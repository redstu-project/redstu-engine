package org.mock.redstu.api.oas3.req.schema;

import io.swagger.v3.oas.models.media.MapSchema;
import io.swagger.v3.oas.models.media.ObjectSchema;
import io.swagger.v3.oas.models.media.Schema;
import io.swagger.v3.oas.models.media.XML;
import org.apache.commons.collections.IteratorUtils;
import org.apache.commons.lang3.StringUtils;
import org.mock.redstu.api.MediaTypes;
import org.mock.redstu.api.oas3.OAS3Utils;
import org.mock.redstu.api.oas3.OAS3Violations;
import org.mock.redstu.api.oas3.SchemaValidationContext;
import org.mock.redstu.api.oas3.SchemaValidator;
import org.mock.redstu.api.oas3.req.OAS3Input;
import org.mock.redstu.api.oas3.req.XmlOAS3Input;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Optional;

public final class ObjectSchemaValidator extends AbstractSchemaValidator<OAS3Input, Schema<?>> implements SchemaValidator {

    @Override
    public boolean isSupported(Schema<?> schema) {
        return schema instanceof ObjectSchema || schema instanceof MapSchema;
    }

    @Override
    protected OAS3Input parse(Object value) {
        return (OAS3Input) value;
    }

    @Override
    protected boolean isParsable(Object value) {
        return value instanceof OAS3Input && ((OAS3Input) value).isContainerNode();
    }

    @Override
    @SuppressWarnings({"unchecked", "rawtypes"})
    protected void apply(SchemaValidationContext context, Schema<?> schema, OAS3Input node) {

        boolean isXMLRequest = StringUtils.equals(context.getMediaType(), MediaTypes.APPLICATION_XML);

        if (isXMLRequest) {

            if (!(node instanceof XmlOAS3Input)) {
                context.addError(OAS3Utils.newConstraintViolation(context, OAS3Violations.INVALID_INPUT_FOR_FIELD));
                return;
            }

            XmlOAS3Input xmlValue = (XmlOAS3Input) node;

            if (xmlValue.isRoot()) {

                String xmlName = Optional.ofNullable(schema.getXml()).map(XML::getName).or(() -> Optional.of(schema.getTitle()))
                        .or(() -> Optional.of(schema.getName())).orElse(context.getComponent());

                if (!StringUtils.equals(xmlValue.getName(), xmlName)) {
                    context.addError(OAS3Utils.newConstraintViolation(context, OAS3Violations.INVALID_INPUT_FOR_XML));
                    return;
                }

            }

        }

        if (schema.getMinProperties() != null && node.size() < schema.getMinProperties()) {
            context.addError(OAS3Utils.newConstraintViolation(context, OAS3Violations.MINIMUM_SIZE_VIOLATION_FOR_FIELD));
        }

        if (schema.getMaxProperties() != null && node.size() > schema.getMaxProperties()) {
            context.addError(OAS3Utils.newConstraintViolation(context, OAS3Violations.MAXIMUM_SIZE_VIOLATION_FOR_FIELD));
        }

        Map<String, Schema> schemas = Optional.ofNullable(schema.getProperties()).orElse(Collections.emptyMap());

        schemas.forEach((key, fieldSchema) -> {

            String k = key;

            if (StringUtils.equals(context.getMediaType(), MediaTypes.APPLICATION_XML) && fieldSchema.getXml() != null) {
                k = Optional.ofNullable(fieldSchema.getXml().getName()).orElse(key);
            }

            OAS3Input vNode = node.get(k);

            boolean isNull = (vNode == null || vNode.isNull());
            boolean isRequired = schema.getRequired() != null && schema.getRequired().contains(k);


            if (isRequired && isNull) {
                context.addError(OAS3Utils.newConstraintViolation(context, k, OAS3Violations.MISSING_INPUT_FOR_REQUIRED_FIELD));
            } else if (!isNull) {
                apply(context, node, k, fieldSchema, vNode, isXMLRequest);
            }

        });

        boolean supportsAdditionalProperties = schema.getAdditionalProperties() instanceof Boolean
                && schema.getAdditionalProperties().equals(Boolean.TRUE);

        if (!supportsAdditionalProperties) {
            List<String> keys = IteratorUtils.toList(node.fieldNames());
            keys.removeAll(schemas.keySet());

            keys.forEach(k -> context.addError(OAS3Utils.newConstraintViolation(context, k, OAS3Violations.DISALLOWED_PROPERTIES_FOR_FIELD)));

        }

    }

    private void apply(SchemaValidationContext context, OAS3Input node, String k, Schema<?> fieldSchema, OAS3Input input, boolean isXMLRequest) {

        if (isXMLRequest) {

            if (!(node instanceof XmlOAS3Input)) {
                context.addError(OAS3Utils.newConstraintViolation(context, k, OAS3Violations.INVALID_INPUT_FOR_FIELD));
                return;
            }

            XmlOAS3Input xmlValue = (XmlOAS3Input) input;
            boolean isProperty = !xmlValue.isAttribute();
            boolean isField = Optional.ofNullable(fieldSchema.getXml()).map(XML::getAttribute).map(each -> !each).orElse(true);

            if (fieldSchema.getXml() != null) {

                String prefix = fieldSchema.getXml().getPrefix();
                String namespace = fieldSchema.getXml().getNamespace();

                if (prefix != null && namespace == null) {
                    namespace = context.getXmlNamespaceOf(prefix);
                } else if (prefix == null && namespace != null) {
                    prefix = context.getXmlPrefixOf(namespace);
                }

                if (namespace == null && prefix != null) {
                    context.addError(OAS3Utils.newConstraintViolation(context, k, OAS3Violations.INVALID_PREFIX_FOR_FIELD));
                    return;
                }

                if (namespace != null && !StringUtils.equals(namespace, xmlValue.getNamespace())) {
                    context.addError(OAS3Utils.newConstraintViolation(context, k, OAS3Violations.INVALID_PREFIX_FOR_FIELD));
                    return;
                }

                if (prefix != null && StringUtils.equals(prefix, xmlValue.getPrefix())) {
                    context.addError(OAS3Utils.newConstraintViolation(context, k, OAS3Violations.INVALID_PREFIX_FOR_FIELD));
                    return;
                }

            }

            if ( isProperty != isField) {
                context.addError(OAS3Utils.newConstraintViolation(context, k, isField ? OAS3Violations.INVALID_INPUT_FOR_XML_FIELD : OAS3Violations.INVALID_INPUT_FOR_XML_ATTRIBUTE));
                return;
            }

        }

        context.filter(k, fieldSchema, input);
    }

}
