package org.mock.redstu.api.oas3;

public interface ValidationError {

    ReqPart getPart();
    String getKey();
    String getMessage();

}
