package org.mock.redstu.api.oas3;

import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.Operation;
import io.swagger.v3.oas.models.PathItem;
import org.mock.redstu.api.common.BinaryContent;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public interface ValidationContext {

    PathItem getPath();

    Operation getOperation();

    OpenAPI getSpecification();

    ValidationResult toResult();

    BinaryContent getBody();

    String getMediaType();

    void addViolations(List<? extends ValidationError> container);

    default void addViolations(ValidationError instance) {
        addViolations(Collections.singletonList(instance));
    }

    default void addViolations(ValidationError... instances) {
        addViolations(Arrays.asList(instances));
    }

    List<ValidationError> getViolations();

}
