package org.mock.redstu.api.oas3.resp;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import org.mock.redstu.api.oas3.ValidationError;

import java.io.IOException;

public class ValidationErrorJacksonSerializer extends JsonSerializer<ValidationError> {

    @Override
    public void serialize(ValidationError object, JsonGenerator gen, SerializerProvider provider) throws IOException {

        gen.writeStartObject();

        if (object.getKey() != null) {
            gen.writeStringField("key", object.getKey());
        }

        gen.writeObjectField("part", object.getPart());
        gen.writeStringField("message", object.getMessage());

        gen.writeEndObject();

    }

}
