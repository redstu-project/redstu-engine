package org.mock.redstu.api.oas3;

import io.swagger.v3.oas.models.media.Schema;

import java.util.Collection;

public interface SchemaValidationContext {

    String getKey();

    ReqPart getPart();

    SchemaValidationContext addError(ValidationError error);

    SchemaValidationContext addErrors(Collection<ValidationError> errors);

    SchemaValidationContext filter(int key, Schema<?> schema, Object value);

    SchemaValidationContext filter(String key, Schema<?> schema, Object value);

    String getMediaType();

    String getXmlPrefixOf(String value);

    String getXmlNamespaceOf(String value);

    String getComponent();


}
