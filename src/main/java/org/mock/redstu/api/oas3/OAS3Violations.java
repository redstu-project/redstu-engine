package org.mock.redstu.api.oas3;

public enum OAS3Violations {

    INVALID_INPUT_FOR_FIELD("bad-input", "Input not allowed for the field"),
    MISSING_INPUT_FOR_REQUIRED_FIELD("missing-input", "The key requires input"),
    EMPTY_INPUT_NOT_ALLOWED_FOR_FIELD("empty-input", newInputConstraintViolation("the key mustn't be empty")),

    DISALLOWED_PROPERTIES_FOR_FIELD("disallowed-properties", "The specified key isn't allowed"),

    INVALID_INPUT_FOR_XML("bad-input-for-xml", "The specified document isn't allowed"),
    INVALID_INPUT_FOR_XML_FIELD("bad-input-for-xml-field", "The specified key is an field not a attribute"),
    INVALID_INPUT_FOR_XML_ATTRIBUTE("bad-input-for-xml-attribute", "The specified key is an attribute not a field"),

    INVALID_INPUT_FOR_XML_WRAPPED_ARRAY_FIELD("bad-input-for-xml-array-wrapped", "The value specified for the key must be wrapped"),
    INVALID_INPUT_FOR_XML_UNWRAPPED_ARRAY_FIELD("bad-input-for-xml-array-unwrapped", "The value specified for the key mustn't be wrapped"),
    INVALID_INPUT_FOR_XML_WRAPPED_ARRAY_NAME_FIELD("bad-input-for-xml-array-wrapped-name", "The value specified for the key doesn't comply with xml.name"),

    INVALID_PREFIX_FOR_FIELD("bad-input-for-xml-namespace-prefix", "The specified prefix  doesn't match the specification namespace"),

    INVALID_INPUT_FOR_ARRAY_FIELD("bad-input-for-array", newTypeConstraintViolation("array")),
    UNIQUE_VALUE_VIOLATION_FOR_FIELD("duplication-in-array", newInputConstraintViolation("the key mustn't be duplicated")),

    INVALID_INPUT_FOR_NUMBER_FIELD("bad-input-for-number", newTypeConstraintViolation("number")),
    INVALID_INPUT_FOR_FLOAT_FIELD("bad-input-for-float", newTypeConstraintViolation("number", "float")),
    INVALID_INPUT_FOR_DOUBLE_FIELD("bad-input-for-double", newTypeConstraintViolation("number", "double")),

    INVALID_INPUT_FOR_INTEGER_FIELD("bad-input-for-integer", newTypeConstraintViolation("integer")),
    INVALID_INPUT_FOR_BOOLEAN_FIELD("bad-input-for-boolean", newTypeConstraintViolation("boolean")),

    INVALID_INPUT_FOR_TEXT_FIELD("bad-input-for-text", newTypeConstraintViolation("text")),
    INVALID_INPUT_FOR_BYTE_FIELD("bad-input-for-byte", newTypeConstraintViolation("string", "byte")),
    INVALID_INPUT_FOR_DATE_FIELD("bad-input-for-date", newTypeConstraintViolation("string", "date")),
    INVALID_INPUT_FOR_TIME_FIELD("bad-input-for-time", newTypeConstraintViolation("string", "time")),
    INVALID_INPUT_FOR_IPV4_FIELD("bad-input-for-ipv4", newTypeConstraintViolation("string", "ipv4")),
    INVALID_INPUT_FOR_IPV6_FIELD("bad-input-for-ipv6", newTypeConstraintViolation("string", "ipv6")),
    INVALID_INPUT_FOR_UUID_FIELD("bad-input-for-uuid", newTypeConstraintViolation("string", "uuid")),
    INVALID_INPUT_FOR_EMAIL_FIELD("bad-input-for-email", newTypeConstraintViolation("string", "email")),
    INVALID_INPUT_FOR_DATETIME_FIELD("bad-input-for-datetime", newTypeConstraintViolation("string", "datetime")),

    INVALID_NULL_INPUT_FOR_FIELD("null-input", newInputConstraintViolation("the key mustn't be null")),
    INVALID_INPUT_FOR_PATTERN_FIELD("bad-input-for-pattern", newInputConstraintViolation("the key must match the field pattern")),
    INVALID_INPUT_FOR_ENUM_FIELD("bad-input-for-enum", newInputConstraintViolation("the key doesn't match any known value for the field")),

    MINIMUM_SIZE_VIOLATION_FOR_FIELD("minimum-size", newInputConstraintViolation("the key size must be greater or equal to "
            + "the minimum size defined for the key")),
    MAXIMUM_SIZE_VIOLATION_FOR_FIELD("maximum-size", newInputConstraintViolation("the key size must be lesser or equal to"
            + " the maximum size defined for the key")),

    MULTIPLE_OF_VIOLATION_FOR_FIELD("invalid-multiple", newInputConstraintViolation("the input must be multiple of "
            + "definition for the key")),
    MINIMUM_LIMIT_VIOLATION_FOR_FIELD("minimum-limit", newInputConstraintViolation("the input must be greater or equal to "
            + "the minimum size defined for the key")),
    MAXIMUM_LIMIT_VIOLATION_FOR_FIELD("maximum-limit", newInputConstraintViolation("the input must be lesser or equal to"
            + " the maximum size defined for the key")),
    ;

    public final String message;
    public final String errorCode;

    OAS3Violations(String errorCode, String message) {
        this.message = message;
        this.errorCode = errorCode;
    }


    private static String newTypeConstraintViolation(String type) {
        return INVALID_INPUT_FOR_FIELD.message + ", the specified key expected an " + type;
    }

    private static String newTypeConstraintViolation(String type, String format) {
        return newTypeConstraintViolation(type) + " in " + format + " format";
    }

    private static String newInputConstraintViolation(String message) {
        return OAS3Violations.INVALID_INPUT_FOR_FIELD.message + ", " + message;
    }


}
