package org.mock.redstu.api.oas3.js;

import org.mock.redstu.api.oas3.APIDefinition;
import org.mock.redstu.js.AbstractProxyObject;
import org.mock.redstu.js.MapProxyHelper;

import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

public class APIDefinitionProxy extends AbstractProxyObject {

    private final APIDefinition definition;

    public APIDefinitionProxy(APIDefinition definition) {
        this.definition = definition;
        Object operations = MapProxyHelper.proxyRecursive(getOperations());
        readable("operations", () -> operations);
    }

    private Map<String,Object> getOperations() {
        return new HashMap<>(definition.getOperations().entrySet().stream()
                .collect(Collectors.toMap(Map.Entry::getKey, x -> new APIOperationProxy(x.getValue()))));
    }

}
