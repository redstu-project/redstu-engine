package org.mock.redstu.api.oas3;

import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.Operation;
import io.swagger.v3.oas.models.PathItem;
import org.mock.redstu.api.oas3.resp.ResponseEntityFactory;

import java.util.Collections;
import java.util.Map;
import java.util.stream.Collectors;

public final class OAS3APIDefinition implements APIDefinition {

    private final OpenAPI oas3;
    private final Map<String, APIOperation> operations;
    private final ResponseEntityFactory responseEntityFactory;

    public OAS3APIDefinition(OpenAPI doc, ResponseEntityFactory responseEntityFactory) {
        this.oas3 = doc;
        this.responseEntityFactory = responseEntityFactory;
        this.operations = Collections.unmodifiableMap(readOperations(doc));
    }

    private Map<String, APIOperation> readOperations(OpenAPI doc) {
        return doc.getPaths().values().stream().flatMap(pathItem -> pathItem.readOperations().stream()
                .map(operation -> Map.entry(operation.getOperationId(), newOperation(pathItem, operation))))
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
    }

    @Override
    public Map<String, APIOperation> getOperations() {
        return this.operations;
    }

    private APIOperation newOperation(PathItem pathItem, Operation operation) {

        OAS3APIOperation instance = new OAS3APIOperation();

        instance.setOas3(this.oas3);
        instance.setPathItem(pathItem);
        instance.setOperation(operation);
        instance.setResponseEntityFactory(responseEntityFactory);

        return instance;
    }

    @Override
    public String toString() {
        return oas3.toString();
    }

}
