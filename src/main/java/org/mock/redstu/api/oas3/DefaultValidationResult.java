package org.mock.redstu.api.oas3;

import org.apache.commons.lang3.StringUtils;
import org.mock.redstu.api.MediaTypes;
import org.mock.redstu.api.common.Header;
import org.mock.redstu.api.oas3.resp.ResponseEntityFactory;
import org.mock.redstu.api.req.HttpReq;
import org.mock.redstu.api.resp.HttpResp;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

public class DefaultValidationResult implements ValidationResult {

    private final List<ValidationError> errors;
    private final ResponseEntityFactory responseEntityFactory;

    public DefaultValidationResult(ResponseEntityFactory responseEntityFactory, List<ValidationError> errors) {
        this.errors = Collections.unmodifiableList(errors);
        this.responseEntityFactory = responseEntityFactory;
    }

    @Override
    public boolean anyErrors() {
        return !errors.isEmpty();
    }

    @Override
    public List<? extends ValidationError> getErrors() {
        return errors;
    }

    @Override
    public void sendErrors(HttpReq req, HttpResp resp) {
        try {

            String accept = Optional.ofNullable(req).map(each -> each.headers().get("Accept"))
                    .map(Header::value).filter(each -> !StringUtils.equals(each, "*/*"))
                    .map(each -> StringUtils.split(each, ";")[0]).orElse(MediaTypes.APPLICATION_JSON);

            String contentType = resolveProblemType(accept);

            resp.setStatus(400);
            resp.setContentType(contentType);
            resp.sendText(new String(responseEntityFactory.build(contentType, this), Charset.defaultCharset()));

        } catch (IOException ex) {
            throw new OAS3Exception("Cannot serialize due to failure", ex);
        }
    }

    @Override
    public boolean isValid(String key, ReqPart part) {
        return isValid(key, part);
    }

    @Override
    public boolean isValid(String key, ReqPart part, String errorCode) {
        return errors.stream().noneMatch(each -> {

            if (!StringUtils.equals(each.getKey(), key)) {
                return false;
            }

            if (!Objects.equals(each.getPart(), part)) {
                return false;
            }

            if (errorCode == null) {
                return true;
            }

            if (each instanceof ErrorCodeAwareValidationError) {
                return StringUtils.equals(((ErrorCodeAwareValidationError) each).getCode(), errorCode);
            }

            return false;
        });
    }

    private String resolveProblemType(String contentType) {
        switch (contentType) {
            case MediaTypes.APPLICATION_JSON:
                return MediaTypes.APPLICATION_PROBLEM_JSON;
            case MediaTypes.APPLICATION_XML:
                return MediaTypes.APPLICATION_PROBLEM_XML;
            default:
                throw new OAS3Exception("Cannot serialize due to unsupported Accept=" + contentType);
        }
    }

}
