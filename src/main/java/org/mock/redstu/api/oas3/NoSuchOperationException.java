package org.mock.redstu.api.oas3;

public class NoSuchOperationException extends OAS3Exception {

    public NoSuchOperationException(String s) {
        super(s);
    }

}
