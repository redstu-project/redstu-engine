package org.mock.redstu.api.oas3.req.schema;

import io.swagger.v3.oas.models.media.ArraySchema;
import io.swagger.v3.oas.models.media.Schema;
import io.swagger.v3.oas.models.media.XML;
import org.apache.commons.collections.IteratorUtils;
import org.apache.commons.lang3.StringUtils;
import org.mock.redstu.api.MediaTypes;
import org.mock.redstu.api.oas3.OAS3Utils;
import org.mock.redstu.api.oas3.OAS3Violations;
import org.mock.redstu.api.oas3.SchemaValidationContext;
import org.mock.redstu.api.oas3.SchemaValidator;
import org.mock.redstu.api.oas3.ValidationError;
import org.mock.redstu.api.oas3.req.OAS3Input;
import org.mock.redstu.api.oas3.req.XmlOAS3Input;

import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;

public final class ArraySchemaValidator extends AbstractSchemaValidator<OAS3Input, ArraySchema>
        implements SchemaValidator {

    @Override
    public boolean isSupported(Schema<?> schema) {
        return schema instanceof ArraySchema;
    }

    @Override
    protected OAS3Input parse(Object value) {
        return (OAS3Input) value;
    }

    @Override
    protected boolean isParsable(Object value) {
        return value instanceof Collection || (value != null && value.getClass().isArray())
                || (value instanceof OAS3Input && ((OAS3Input) value).isArray());
    }

    @Override
    @SuppressWarnings({"unchecked"})
    protected void apply(SchemaValidationContext context, ArraySchema schema, OAS3Input input) {

        if (!input.isArray()) {
            context.addError(newInputConstraintViolation(context));
            return;
        }

        List<OAS3Input> col = IteratorUtils.toList(input.elements());

        boolean isXMLRequest = StringUtils.equals(context.getMediaType(), MediaTypes.APPLICATION_XML);

        if (isXMLRequest && !isXMLCompliant(context, input, schema, col)) {
            return;
        }

        if (schema.getMinItems() != null && col.size() < schema.getMinItems()) {
            context.addError(OAS3Utils.newConstraintViolation(context, OAS3Violations.MINIMUM_SIZE_VIOLATION_FOR_FIELD));
        }

        if (schema.getMaxItems() != null && col.size() > schema.getMaxItems()) {
            context.addError(OAS3Utils.newConstraintViolation(context, OAS3Violations.MAXIMUM_SIZE_VIOLATION_FOR_FIELD));
        }

        if (schema.getUniqueItems() != null && schema.getUniqueItems() && !uniqueOnly(col)) {

            for (int index = 0; index < col.size(); index++) {

                if (Collections.frequency(col, col.get(index)) > 1) {
                    context.addError(OAS3Utils.newConstraintViolation(context, index, OAS3Violations.UNIQUE_VALUE_VIOLATION_FOR_FIELD));
                }

            }

        }

        Iterator<?> iterator = col.iterator();

        for (int index = 0; index < col.size(); index++) {
            context.filter(index, schema.getItems(), iterator.next());
        }

    }

    private boolean isXMLCompliant(SchemaValidationContext context, OAS3Input node, ArraySchema schema, List<OAS3Input> col) {


        if (!(node instanceof XmlOAS3Input)) {
            context.addError(OAS3Utils.newConstraintViolation(context, OAS3Violations.INVALID_INPUT_FOR_FIELD));
            return false;
        }

        XmlOAS3Input xmlValue = (XmlOAS3Input) node;
        boolean isProperty = !xmlValue.isAttribute();
        boolean isField = Optional.ofNullable(schema.getXml()).map(XML::getAttribute).map(each -> !each).orElse(true);

        if (schema.getXml() == null && !xmlValue.isVirtual()) {
            context.addError(OAS3Utils.newConstraintViolation(context, OAS3Violations.INVALID_INPUT_FOR_XML_UNWRAPPED_ARRAY_FIELD));
            return false;
        }

        if (isProperty != isField) {
            context.addError(OAS3Utils.newConstraintViolation(context, isField ? OAS3Violations.INVALID_INPUT_FOR_XML_FIELD : OAS3Violations.INVALID_INPUT_FOR_XML_ATTRIBUTE));
            return false;
        }

        if (schema.getXml() != null) {
            return isSchemaCompliant(context, (XmlOAS3Input) node, schema, col, xmlValue) && isNamespaceCompliant(context, schema, xmlValue);
        }

        return true;
    }

    private boolean isNamespaceCompliant(SchemaValidationContext context, ArraySchema schema, XmlOAS3Input xmlValue) {

        String prefix = schema.getXml().getPrefix();
        String namespace = schema.getXml().getNamespace();

        if (prefix == null && namespace != null) {
            prefix = context.getXmlPrefixOf(namespace);
        } else if (prefix != null && namespace == null) {
            namespace = context.getXmlNamespaceOf(prefix);
        }

        if (namespace == null && prefix != null) {
            context.addError(OAS3Utils.newConstraintViolation(context, OAS3Violations.INVALID_PREFIX_FOR_FIELD));
            return false;
        }

        if (namespace != null && !StringUtils.equals(namespace, xmlValue.getNamespace())) {
            context.addError(OAS3Utils.newConstraintViolation(context, OAS3Violations.INVALID_PREFIX_FOR_FIELD));
            return false;
        }

        if (prefix != null && StringUtils.equals(prefix, xmlValue.getPrefix())) {
            context.addError(OAS3Utils.newConstraintViolation(context, OAS3Violations.INVALID_PREFIX_FOR_FIELD));
            return false;
        }

        return true;
    }

    private boolean isSchemaCompliant(SchemaValidationContext context, XmlOAS3Input node, ArraySchema schema, List<OAS3Input> col, XmlOAS3Input xmlValue) {
        boolean isWrappedPresent = Optional.ofNullable(schema.getXml().getWrapped()).orElse(false);

        if (!isWrappedPresent && !xmlValue.isVirtual()) {
            context.addError(OAS3Utils.newConstraintViolation(context, OAS3Violations.INVALID_INPUT_FOR_XML_WRAPPED_ARRAY_FIELD));
            return false;
        }

        if (isWrappedPresent) {
            String name = Optional.of(schema).map(ArraySchema::getItems).map(Schema::getXml)
                    .map(XML::getName).orElse(node.getName());

            boolean isXMLName = col.stream().map(each -> (XmlOAS3Input) each).allMatch(each -> each.getName().equals(name));

            if (!isXMLName) {
                context.addError(OAS3Utils.newConstraintViolation(context, OAS3Violations.INVALID_INPUT_FOR_XML_WRAPPED_ARRAY_NAME_FIELD));
                return false;
            }

        }

        return true;
    }


    @Override
    protected ValidationError newInputConstraintViolation(SchemaValidationContext context) {
        return OAS3Utils.newConstraintViolation(context, OAS3Violations.INVALID_INPUT_FOR_ARRAY_FIELD);
    }

    private boolean uniqueOnly(Collection<?> col) {
        return col.stream().allMatch(each -> Collections.frequency(col, each) == 1);
    }

}
