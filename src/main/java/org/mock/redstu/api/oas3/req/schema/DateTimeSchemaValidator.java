package org.mock.redstu.api.oas3.req.schema;

import io.swagger.v3.oas.models.media.Schema;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.validator.GenericValidator;
import org.apache.commons.validator.routines.TimeValidator;
import org.mock.redstu.api.oas3.OAS3Utils;
import org.mock.redstu.api.oas3.OAS3Violations;
import org.mock.redstu.api.oas3.SchemaValidationContext;
import org.mock.redstu.api.oas3.ValidationError;

public class DateTimeSchemaValidator extends AbstractTextSchemaValidator {

    @Override
    public boolean isSupported(Schema<?> schema) {
        return super.isSupported(schema) && StringUtils.equals(schema.getFormat(), "date-time");
    }

    @Override
    protected void doValidate(SchemaValidationContext context, Schema<?> schema, String object) {
        if (!isDateTime(object)) {
            context.addError(newInputConstraintViolation(context));
        }
    }

    protected ValidationError newInputConstraintViolation(SchemaValidationContext context) {
        return OAS3Utils.newConstraintViolation(context, OAS3Violations.INVALID_INPUT_FOR_DATETIME_FIELD);
    }

    private boolean isDateTime(String object) {
        if (object == null) {
            return false;
        }

        String[] args = object.split("T");

        if (args.length != 2) {
            return false;
        }

        return GenericValidator.isDate(args[0], "yyyy-MM-dd", true)
                && TimeValidator.getInstance().isValid(args[1]);
    }

}
