package org.mock.redstu.api.oas3;

import java.util.Map;

public interface APIDefinition {

    /**
     * Getter method for operations map.
     * @return map of operations
     */
    Map<String,APIOperation> getOperations();

}
