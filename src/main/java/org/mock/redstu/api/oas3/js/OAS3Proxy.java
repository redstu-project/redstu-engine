package org.mock.redstu.api.oas3.js;

import org.graalvm.polyglot.Value;
import org.mock.redstu.api.OAS3;
import org.mock.redstu.api.oas3.APIDefinition;

public class OAS3Proxy extends AbstractExecutableProxy {

    public OAS3Proxy() {
        executable("spec", this::spec);
    }

    public Object spec(Value... values) {
        if (values.length != 1) {
            throw new IllegalArgumentException("Binding doesn't match required parameters");
        }

        APIDefinition definition = OAS3.spec(getString(values[0]));

        if (definition == null) {
            return null;
        }

        return new APIDefinitionProxy(definition);
    }

}
