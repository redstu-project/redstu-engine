package org.mock.redstu.api.oas3.js;

import org.graalvm.polyglot.Value;
import org.mock.redstu.api.oas3.ReqPart;
import org.mock.redstu.api.oas3.ValidationError;
import org.mock.redstu.api.oas3.ValidationResult;
import org.mock.redstu.server.req.proxy.HttpRequestProxy;
import org.mock.redstu.server.resp.proxy.HttpRespProxy;

import java.util.List;
import java.util.Optional;

public class ValidationResultProxy extends AbstractExecutableProxy {

    private final ValidationResult instance;

    public ValidationResultProxy(ValidationResult instance) {
        this.instance = instance;

        executable("isValid", this::isValid);
        executable("anyErrors", this::anyErrors);
        executable("sendErrors", this::sendErrors);

        ValidationErrorProxy[] args = toArray(instance.getErrors());
        readable("errors", () -> args);
    }

    private Value anyErrors(Value... args) {

        if (args.length != 0) {
            throw new IllegalArgumentException("Unsupported Parameters");
        }

        return Value.asValue(instance.anyErrors());
    }

    private Value sendErrors(Value... args) {

        if (args.length != 2) {
            throw new IllegalArgumentException("Unsupported Parameters");
        }

        if (!args[0].isProxyObject()) {
            throw new IllegalArgumentException("Parameters[0] must be proxyable");
        }

        HttpRequestProxy req = args[0].asProxyObject();

        if (!args[1].isProxyObject()) {
            throw new IllegalArgumentException("Parameters[1] must be proxyable");
        }

        HttpRespProxy resp = args[1].asProxyObject();

        instance.sendErrors(req.getObject(), resp.getObject());

        return null;

    }

    private Value isValid(Value... args) {
        if (!(args.length >= 2 && args.length <= 3)) {
            throw new IllegalArgumentException("Unsupported Parameters");
        }

        String key = getString(args[0]);
        String errorCode = args.length == 3 ? getString(args[2]) : null;
        ReqPart part = Optional.ofNullable(getString(args[1])).map(ReqPart::valueOf).orElse(null);

        if (key == null) {
            throw new IllegalArgumentException("key mustn't be null");
        }

        if (part == null) {
            throw new IllegalArgumentException("part mustn't be null");
        }

        return Value.asValue(instance.isValid(key, part, errorCode));
    }

    private ValidationErrorProxy[] toArray(List<? extends ValidationError> errors) {
        return errors.stream().map(ValidationErrorProxy::new)
                .toArray(ValidationErrorProxy[]::new);
    }

}
