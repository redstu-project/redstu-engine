package org.mock.redstu.api.oas3.req.validator;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.mock.redstu.api.MediaTypes;
import org.mock.redstu.api.oas3.OAS3Utils;

public class JsonJacksonMediaTypeValidator extends JacksonMediaTypeValidator {

    @Override
    protected String getMediaType() {
        return MediaTypes.APPLICATION_JSON;
    }

    @Override
    protected ObjectMapper getObjectMapper() {
        return OAS3Utils.JSON_OBJECT_MAPPER;
    }

}
