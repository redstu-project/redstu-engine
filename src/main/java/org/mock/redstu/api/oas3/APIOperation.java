package org.mock.redstu.api.oas3;

import org.mock.redstu.api.common.BinaryContent;
import org.mock.redstu.api.req.HttpReq;
import org.mock.redstu.api.resp.HttpResp;

public interface APIOperation {

    void sendExample(int statusCode, String name, HttpReq req, HttpResp resp);

    ValidationResult validate(HttpReq req, BinaryContent body) throws OAS3Exception;
}
