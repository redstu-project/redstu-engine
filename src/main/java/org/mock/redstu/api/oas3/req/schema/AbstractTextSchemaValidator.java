package org.mock.redstu.api.oas3.req.schema;

import io.swagger.v3.oas.models.media.Schema;
import org.mock.redstu.api.oas3.OAS3Utils;
import org.mock.redstu.api.oas3.OAS3Violations;
import org.mock.redstu.api.oas3.SchemaValidationContext;
import org.mock.redstu.api.oas3.SchemaValidator;
import org.mock.redstu.api.oas3.ValidationError;
import org.mock.redstu.api.oas3.req.OAS3Input;

import java.util.regex.Pattern;

public abstract class AbstractTextSchemaValidator extends AbstractSchemaValidator<String, Schema<?>> implements SchemaValidator {

    @Override
    public boolean isSupported(Schema<?> schema) {
        return schema != null && schema.getType().equals("string");
    }

    @Override
    protected String parse(Object value) {
        return value instanceof OAS3Input ? ((OAS3Input) value).asText() : value.toString();
    }

    @Override
    protected boolean isParsable(Object value) {
        return value instanceof CharSequence || (value instanceof OAS3Input && ((OAS3Input) value).isTextual());
    }

    @Override
    protected void apply(SchemaValidationContext context, Schema<?> schema, String object) {

        if (schema.getPattern() != null && !Pattern.compile(schema.getPattern()).matcher(object).matches()) {
            context.addError(OAS3Utils.newConstraintViolation(context,
                    OAS3Violations.INVALID_INPUT_FOR_PATTERN_FIELD));
        }

        if (schema.getMinLength() != null && object.length() < schema.getMinLength()) {
            context.addError(OAS3Utils.newConstraintViolation(context,
                    OAS3Violations.MINIMUM_SIZE_VIOLATION_FOR_FIELD));
        }

        if (schema.getMaxLength() != null && object.length() > schema.getMaxLength()) {
            context.addError(OAS3Utils.newConstraintViolation(context,
                    OAS3Violations.MAXIMUM_SIZE_VIOLATION_FOR_FIELD));
        }

        doValidate(context, schema, object);
    }


    protected ValidationError newInputConstraintViolation(SchemaValidationContext context) {
        return OAS3Utils.newInputConstraintViolation(context);
    }

    protected abstract void doValidate(SchemaValidationContext context, Schema<?> schema, String object);

    protected boolean contains(Schema<?> schema, String object) {
        return schema.getEnum().stream().anyMatch(each -> each.equals(object));
    }

}
