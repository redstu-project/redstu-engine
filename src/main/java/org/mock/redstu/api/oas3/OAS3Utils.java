package org.mock.redstu.api.oas3;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.examples.Example;
import io.swagger.v3.oas.models.media.MapSchema;
import io.swagger.v3.oas.models.media.ObjectSchema;
import io.swagger.v3.oas.models.media.Schema;
import io.swagger.v3.oas.models.parameters.Parameter;
import org.apache.commons.lang3.StringUtils;
import org.mock.redstu.api.common.Header;
import org.mock.redstu.api.oas3.req.schema.ArraySchemaValidator;
import org.mock.redstu.api.oas3.req.schema.BooleanSchemaValidator;
import org.mock.redstu.api.oas3.req.schema.ByteSchemaValidator;
import org.mock.redstu.api.oas3.req.schema.DateSchemaValidator;
import org.mock.redstu.api.oas3.req.schema.DateTimeSchemaValidator;
import org.mock.redstu.api.oas3.req.schema.DoubleSchemaValidator;
import org.mock.redstu.api.oas3.req.schema.EmailSchemaValidator;
import org.mock.redstu.api.oas3.req.schema.FloatSchemaValidator;
import org.mock.redstu.api.oas3.req.schema.IntegerSchemaValidator;
import org.mock.redstu.api.oas3.req.schema.Ipv4SchemaValidator;
import org.mock.redstu.api.oas3.req.schema.Ipv6SchemaValidator;
import org.mock.redstu.api.oas3.req.schema.NumberSchemaValidator;
import org.mock.redstu.api.oas3.req.schema.ObjectSchemaValidator;
import org.mock.redstu.api.oas3.req.schema.TextSchemaValidator;
import org.mock.redstu.api.oas3.req.schema.TimeSchemaValidator;
import org.mock.redstu.api.oas3.req.schema.UUIDSchemaValidator;
import org.mock.redstu.api.oas3.resp.Problem;
import org.mock.redstu.api.oas3.resp.ProblemJacksonSerializer;
import org.mock.redstu.api.oas3.resp.ValidationErrorJacksonSerializer;

import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

public final class OAS3Utils {

    public static final ObjectMapper JSON_OBJECT_MAPPER = new ObjectMapper()
            .setSerializationInclusion(JsonInclude.Include.NON_NULL)
            .registerModule(new SimpleModule()
                    .addSerializer(Problem.class, new ProblemJacksonSerializer())
                    .addSerializer(ValidationError.class, new ValidationErrorJacksonSerializer())
                    .addSerializer(ReqPart.class, new ReqPartJacksonSerializer()));

    public static final ObjectMapper XML_OBJECT_MAPPER = new XmlMapper()
            .setSerializationInclusion(JsonInclude.Include.NON_NULL)
            .registerModule(new SimpleModule()
                    .addSerializer(Problem.class, new ProblemJacksonSerializer())
                    .addSerializer(ValidationError.class, new ValidationErrorJacksonSerializer())
                    .addSerializer(ReqPart.class, new ReqPartJacksonSerializer()));


    public static List<SchemaValidator> SCHEMA_VALIDATORS = List.of(new ByteSchemaValidator(),
            new DateSchemaValidator(), new TimeSchemaValidator(), new DateTimeSchemaValidator(),
            new EmailSchemaValidator(), new Ipv4SchemaValidator(), new Ipv6SchemaValidator(),
            new UUIDSchemaValidator(), new TextSchemaValidator(), new BooleanSchemaValidator(),
            new IntegerSchemaValidator(), new DoubleSchemaValidator(), new FloatSchemaValidator(),
            new NumberSchemaValidator(), new ArraySchemaValidator(), new ObjectSchemaValidator());

    public static List<Map.Entry<String, String>> getXmlNamespaces(Schema<?> schema) {

        if (schema == null || schema.getXml() == null) {
            return Collections.emptyList();
        }

        if (schema.getXml().getPrefix() == null && schema.getXml().getNamespace() == null) {
            return Collections.emptyList();
        }

        if (schema.getXml().getPrefix() != null && schema.getXml().getNamespace() == null) {
            return Collections.emptyList();
        }

        List<Map.Entry<String, String>> col = new ArrayList<>();
        col.add(new AbstractMap.SimpleEntry<>(schema.getXml().getPrefix(), schema.getXml().getNamespace()));

        if (schema instanceof ObjectSchema || schema instanceof MapSchema) {
            Optional.ofNullable(schema.getProperties()).orElse(Collections.emptyMap()).forEach((k, v) -> col.addAll(getXmlNamespaces(v)));
        }

        return col;
    }

    public static boolean isRef(Parameter param) {

        if (param == null) {
            return false;
        }
        return param.getName() == null && param.getIn() == null && param.getDescription() == null
                && param.getRequired() == null && param.getDeprecated() == null && param.getAllowEmptyValue() == null
                && param.getStyle() == null && param.getExplode() == null && param.getAllowReserved() == null
                && param.getSchema() == null && param.getExamples() == null && param.getExample() == null
                && param.getContent() == null && param.get$ref() != null;
    }

    public static String extractMediaType(Header value) {
        return Optional.ofNullable(value).map(Header::value).filter(each -> !each.equals("*/*"))
                .filter(each -> StringUtils.countMatches(each, '/') == 1)
                .map(each -> StringUtils.split(each, ";")[0]).orElse(null);
    }

    public static Example toDefinitive(OpenAPI oas3, Example example) {

        if (example == null || example.get$ref() == null) {
            return example;
        }


        if (StringUtils.startsWith(example.get$ref(), "#/")) {
            return toDefinitive(oas3, oas3.getComponents().getExamples()
                    .get(StringUtils.substring(example.get$ref(), "#/components/examples/".length())));
        }

        throw new OAS3Exception("Unsupported reference for Example");
    }

    public static Schema<?> toDefinitive(OpenAPI oas3, Schema<?> schema) {

        if (schema == null) {
            throw new OAS3Exception("Schema mustn't be null");
        }

        if (schema.get$ref() != null && schema.getType() == null && schema.getFormat() == null) {

            if (StringUtils.startsWith(schema.get$ref(), "#/")) {
                return toDefinitive(oas3, oas3.getComponents().getSchemas()
                        .get(StringUtils.substring(schema.get$ref(), "#/components/schemas/".length())));
            }

            throw new OAS3Exception("Unsupported reference for Schema");
        }

        return schema;
    }

    public static Parameter toDefinitive(OpenAPI oas3, Parameter parameter) {

        if (parameter == null) {
            return null;
        }

        if (isRef(parameter)) {

            if (StringUtils.startsWith(parameter.get$ref(), "#/")) {
                return toDefinitive(oas3, oas3.getComponents().getParameters()
                        .get(StringUtils.substring(parameter.get$ref(), "#/components/parameters/".length())));
            }

            throw new OAS3Exception("Unsupported reference for Parameter");
        }

        return parameter;
    }

    public static ErrorCodeAwareValidationError newInputConstraintViolation(SchemaValidationContext ctx) {

        if (ctx == null) {
            return null;
        }

        return newConstraintViolation(ctx, OAS3Violations.INVALID_INPUT_FOR_FIELD);
    }

    public static ErrorCodeAwareValidationError newInvalidOptionConstraintViolation(SchemaValidationContext ctx) {

        if (ctx == null) {
            return null;
        }

        return newConstraintViolation(ctx, OAS3Violations.INVALID_INPUT_FOR_ENUM_FIELD);
    }

    public static ErrorCodeAwareValidationError newConstraintViolation(SchemaValidationContext ctx, OAS3Violations violation) {
        return newConstraintViolation(ctx.getPart(), ctx.getKey(), violation);
    }

    public static ErrorCodeAwareValidationError newConstraintViolation(SchemaValidationContext ctx, int index, OAS3Violations violation) {
        String key = Optional.ofNullable(ctx.getKey()).map(prefix -> prefix + "[" + index + "]")
                .orElseGet(() -> "[" + index + "]");
        return newConstraintViolation(ctx.getPart(), key, violation);
    }

    public static ErrorCodeAwareValidationError newConstraintViolation(SchemaValidationContext ctx, String field, OAS3Violations violation) {
        String key = Optional.ofNullable(ctx.getKey()).map(prefix -> prefix + "." + field).orElse(field);
        return newConstraintViolation(ctx.getPart(), key, violation);
    }

    public static ErrorCodeAwareValidationError newConstraintViolation(ReqPart part, String key, OAS3Violations violation) {
        return new DefaultValidationError(part, key, violation);
    }


}
