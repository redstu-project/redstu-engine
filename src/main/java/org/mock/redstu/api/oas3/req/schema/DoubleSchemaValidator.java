package org.mock.redstu.api.oas3.req.schema;

import org.mock.redstu.api.oas3.OAS3Exception;
import org.mock.redstu.api.oas3.OAS3Utils;
import org.mock.redstu.api.oas3.OAS3Violations;
import org.mock.redstu.api.oas3.SchemaValidationContext;
import org.mock.redstu.api.oas3.ValidationError;
import org.mock.redstu.api.oas3.req.OAS3Input;

import java.math.BigDecimal;
import java.math.BigInteger;

public class DoubleSchemaValidator extends AbstractNumberSchemaValidator {

    @Override
    protected BigDecimal parse(Object value) {

        if (value instanceof Double) {
            return BigDecimal.valueOf((double) value);
        }

        if (value instanceof String) {
            return new BigDecimal((String) value);
        }

        if (value instanceof BigDecimal) {
            return (BigDecimal) value;
        }

        if (value instanceof BigInteger) {
            return parse(value.toString());
        }

        if (value instanceof Integer) {
            return BigDecimal.valueOf((Integer) value);
        }

        if (value instanceof Float) {
            return BigDecimal.valueOf((Float) value);
        }

        if (value instanceof OAS3Input) {
            return parse(((OAS3Input) value).numberValue());
        }

        throw new OAS3Exception("Cannot convert " + value + " into Double");
    }

    @Override
    protected BigDecimal getMinimumValue() {
        return BigDecimal.valueOf(Double.MIN_VALUE);
    }

    @Override
    protected BigDecimal getMaximumValue() {
        return BigDecimal.valueOf(Double.MAX_VALUE);
    }

    @Override
    public String[] getFormat() {
        return new String[]{"double"};
    }

    @Override
    protected ValidationError newInputConstraintViolation(SchemaValidationContext context) {
        return OAS3Utils.newConstraintViolation(context, OAS3Violations.INVALID_INPUT_FOR_DOUBLE_FIELD);
    }

}

