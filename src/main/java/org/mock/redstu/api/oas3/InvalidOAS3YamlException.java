package org.mock.redstu.api.oas3;

public class InvalidOAS3YamlException extends OAS3Exception {

    public InvalidOAS3YamlException(String s) {
        super(s);
    }

    public InvalidOAS3YamlException(String message, Throwable cause) {
        super(message, cause);
    }

}
