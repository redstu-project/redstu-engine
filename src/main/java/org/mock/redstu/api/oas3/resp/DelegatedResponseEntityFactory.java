package org.mock.redstu.api.oas3.resp;

import org.mock.redstu.api.oas3.OAS3Exception;
import org.mock.redstu.api.oas3.ValidationError;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public final class DelegatedResponseEntityFactory implements ResponseEntityFactory {

    private final List<ResponseEntityFactory> sequence = new ArrayList<>();

    public void addFactory(ResponseEntityFactory factory) {

        if (factory == null) {
            return;
        }

        if (!sequence.contains(factory)) {
            sequence.add(factory);
        }
    }

    @Override
    public boolean supports(String contentType, Object value) {
        return sequence.stream().anyMatch(each -> each.supports(contentType, value));
    }

    @Override
    public byte[] build(String contentType, Object payload) throws IOException {

        if (payload == null) {
            return null;
        }

        for (ResponseEntityFactory factory : sequence) {
            if (factory.supports(contentType, payload)) {
                return factory.build(contentType, payload);
            }
        }

        throw new OAS3Exception("Cannot produce response entity for Content-Type=" + contentType);
    }

}
