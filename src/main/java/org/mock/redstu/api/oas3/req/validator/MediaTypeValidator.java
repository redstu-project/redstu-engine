package org.mock.redstu.api.oas3.req.validator;

import org.apache.commons.lang3.StringUtils;
import org.mock.redstu.api.common.Header;
import org.mock.redstu.api.oas3.OAS3Utils;
import org.mock.redstu.api.oas3.OAS3Violations;
import org.mock.redstu.api.oas3.ReqPart;
import org.mock.redstu.api.oas3.ValidationContext;
import org.mock.redstu.api.oas3.Validator;
import org.mock.redstu.api.oas3.ValidatorChain;
import org.mock.redstu.api.oas3.ValidatorChainImpl;
import org.mock.redstu.api.req.HttpReq;

import javax.xml.parsers.ParserConfigurationException;
import java.util.Optional;

public class MediaTypeValidator implements Validator {

    @Override
    public void execute(ValidationContext context, ValidatorChain chain, HttpReq req) {

        ValidatorChain innerChain;

        try {

            innerChain = ValidatorChainImpl.builder().withReq(req).withContext(context)
                    .withValidator(new JsonJacksonMediaTypeValidator())
                    .withValidator(new XmlMediaTypeValidator()).build();


            innerChain.proceed();

            if (!innerChain.isInterrupted()) {
                String contentType = Optional.ofNullable(req.headers().get("Content-Type"))
                        .map(Header::value).map(each -> StringUtils.split(each, ";")[0]).orElse(null);

                context.addViolations(OAS3Utils.newConstraintViolation(ReqPart.MediaType, contentType,
                        OAS3Violations.INVALID_INPUT_FOR_ENUM_FIELD));
            }
        } catch (ParserConfigurationException ex) {
            throw new IllegalStateException(ex);
        }
    }


}
