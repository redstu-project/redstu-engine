package org.mock.redstu.api.oas3;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import java.io.IOException;

public class ReqPartJacksonSerializer extends JsonSerializer<ReqPart> {

    @Override
    public void serialize(ReqPart reqPart, JsonGenerator gen, SerializerProvider sp) throws IOException {
        gen.writeObject(reqPart.name);
    }

}
