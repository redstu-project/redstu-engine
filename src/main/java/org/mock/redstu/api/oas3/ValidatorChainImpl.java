package org.mock.redstu.api.oas3;

import org.mock.redstu.api.req.HttpReq;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public class ValidatorChainImpl implements ValidatorChain {

    private boolean broken;
    private final HttpReq req;
    private final ValidationContext context;
    private final Iterator<Validator> sequence;

    private ValidatorChainImpl(ValidationContext context, Iterator<Validator> sequence, HttpReq req) {
        this.req = req;
        this.broken = false;
        this.context = context;
        this.sequence = sequence;
    }

    @Override
    public void proceed() {

        if (this.sequence.hasNext() && !broken) {
            this.sequence.next().execute(this.context, this, this.req);
            proceed();
        }
    }

    @Override
    public boolean isInterrupted() {
        return this.broken;
    }

    @Override
    public void interrupt() {
        this.broken = true;
    }


    public static Builder builder() {
        return new Builder();
    }

    public static class Builder {

        private HttpReq req;
        private List<Validator> sequence;
        private ValidationContext context;

        public Builder withReq(HttpReq req) {
            this.req = req;
            return this;
        }

        public Builder withContext(ValidationContext context) {
            this.context = context;
            return this;
        }

        public Builder withValidator(Validator instance) {

            if (instance == null) {
                return this;
            }

            if (this.sequence == null) {
                this.sequence = new ArrayList<>();
            }

            this.sequence.add(instance);

            return this;
        }

        public ValidatorChainImpl build() {

            if (this.context == null) {
                throw new OAS3Exception("Validation Context mustn't be null");
            }

            if (this.req == null) {
                throw new OAS3Exception("Request mustn't be null");
            }

            Iterator<Validator> iterator = sequence == null ? Collections.emptyIterator() : sequence.iterator();

            return new ValidatorChainImpl(context, iterator, req);
        }

    }


}
