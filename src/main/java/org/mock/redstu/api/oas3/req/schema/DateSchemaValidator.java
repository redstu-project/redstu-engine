package org.mock.redstu.api.oas3.req.schema;

import io.swagger.v3.oas.models.media.Schema;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.validator.GenericValidator;
import org.mock.redstu.api.oas3.OAS3Utils;
import org.mock.redstu.api.oas3.OAS3Violations;
import org.mock.redstu.api.oas3.SchemaValidationContext;
import org.mock.redstu.api.oas3.ValidationError;

public class DateSchemaValidator extends AbstractTextSchemaValidator {

    @Override
    public boolean isSupported(Schema<?> schema) {
        return super.isSupported(schema) && StringUtils.equals(schema.getFormat(), "date");
    }

    @Override
    protected void doValidate(SchemaValidationContext context, Schema<?> schema, String object) {
        if (!GenericValidator.isDate(object, "yyyy-MM-dd", true)) {
            context.addError(newInputConstraintViolation(context));
        }
    }

    protected ValidationError newInputConstraintViolation(SchemaValidationContext context) {
        return OAS3Utils.newConstraintViolation(context, OAS3Violations.INVALID_INPUT_FOR_DATE_FIELD);
    }

}
