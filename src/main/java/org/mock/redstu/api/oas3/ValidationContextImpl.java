package org.mock.redstu.api.oas3;


import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.Operation;
import io.swagger.v3.oas.models.PathItem;
import org.mock.redstu.api.common.BinaryContent;
import org.mock.redstu.api.oas3.resp.ResponseEntityFactory;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ValidationContextImpl implements ValidationContext {

    private PathItem path;
    private String mediaType;
    private BinaryContent body;
    private Operation operation;
    private OpenAPI specification;
    private ResponseEntityFactory responseEntityFactory;
    private final List<ValidationError> errors = new ArrayList<>();

    @Override
    public PathItem getPath() {
        return this.path;
    }

    @Override
    public Operation getOperation() {
        return this.operation;
    }

    @Override
    public OpenAPI getSpecification() {
        return this.specification;
    }

    @Override
    public ValidationResult toResult() {
        return new DefaultValidationResult(responseEntityFactory, this.errors);
    }

    @Override
    public BinaryContent getBody() {
        return this.body;
    }

    @Override
    public String getMediaType() {
        return mediaType;
    }

    @Override
    public void addViolations(List<? extends ValidationError> container) {

        if (container != null) {
            this.errors.addAll(container);
        }

    }

    @Override
    public List<ValidationError> getViolations() {
        return Collections.unmodifiableList(errors);
    }

    public void setPath(PathItem path) {
        this.path = path;
    }

    public void setOperation(Operation operation) {
        this.operation = operation;
    }

    public void setSpecification(OpenAPI specification) {
        this.specification = specification;
    }

    public void setResponseEntityFactory(ResponseEntityFactory responseEntityFactory) {
        this.responseEntityFactory = responseEntityFactory;
    }

    public void setBody(BinaryContent body) {
        this.body = body;
    }

    public void setMediaType(String mediaType) {
        this.mediaType = mediaType;
    }
}
