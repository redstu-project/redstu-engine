package org.mock.redstu.api.oas3.req.schema;

import io.swagger.v3.oas.models.media.Schema;
import org.mock.redstu.api.oas3.OAS3Utils;
import org.mock.redstu.api.oas3.OAS3Violations;
import org.mock.redstu.api.oas3.SchemaValidationContext;
import org.mock.redstu.api.oas3.ValidationError;

public class TextSchemaValidator extends AbstractTextSchemaValidator {

    @Override
    public boolean isSupported(Schema<?> schema) {
        return super.isSupported(schema) && schema.getFormat() == null;
    }

    @Override
    protected void doValidate(SchemaValidationContext context, Schema<?> schema, String object) {
        // TODO DO NOTHING
    }

    @Override
    protected ValidationError newInputConstraintViolation(SchemaValidationContext context) {
        return OAS3Utils.newConstraintViolation(context, OAS3Violations.INVALID_INPUT_FOR_TEXT_FIELD);
    }

}
