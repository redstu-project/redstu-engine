package org.mock.redstu.api.oas3.req.validator;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.mock.redstu.api.MediaTypes;
import org.mock.redstu.api.oas3.ValidationContext;
import org.mock.redstu.api.oas3.req.OAS3Input;
import org.mock.redstu.api.oas3.req.XmlOAS3Input;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Proxy;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.BiFunction;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class XmlMediaTypeValidator extends AbstractMediaTypeValidator {

    private final DocumentBuilder builder;

    public XmlMediaTypeValidator() throws ParserConfigurationException {
        this(factory -> factory.setNamespaceAware(true));
    }

    public XmlMediaTypeValidator(Consumer<DocumentBuilderFactory> configure) throws ParserConfigurationException {

        DocumentBuilderFactory instance = DocumentBuilderFactory.newInstance();

        if (configure != null) {
            configure.accept(instance);
        }

        this.builder = instance.newDocumentBuilder();
    }

    @Override
    protected Object getObject(ValidationContext context, InputStream io) throws IOException {
        try {

            if (io == null) {
                return null;
            }

            byte[] array = IOUtils.toByteArray(io);

            if (array.length == 0) {
                return null;
            }

            return newAdapter(this.builder.parse(new ByteArrayInputStream(array)));
        } catch (SAXException ex) {
            throw new IOException(ex);
        }
    }

    private Object newAdapter(Document document) {

        if (document == null) {
            return null;
        }
        document.normalize();

        return new XmlOAS3InputImpl(document);
    }

    @Override
    protected String getMediaType() {
        return MediaTypes.APPLICATION_XML;
    }


    private abstract static class AbstractXmlOAS3InputImpl implements XmlOAS3Input {

        private List<OAS3Input> elements;
        private Map<String, OAS3Input> fields;

        @Override
        public abstract boolean isRoot();

        @Override
        public abstract String getName();

        @Override
        public abstract String getPrefix();

        @Override
        public abstract String getNamespace();

        @Override
        public abstract Integer size();

        @Override
        public OAS3Input get(String fieldName) {
            return getFields().get(fieldName);
        }

        @Override
        public abstract boolean isNull();

        @Override
        public Iterator<String> fieldNames() {
            return getFields().keySet().iterator();
        }

        protected abstract <T> T toScalar(Class<T> returnType, Function<String, T> mapper);

        @Override
        public Number numberValue() {
            return toScalar(Number.class, text -> text == null ? null : new BigDecimal(text));
        }

        @Override
        public Boolean booleanValue() {
            return toScalar(Boolean.class, text -> text == null ? null : Boolean.parseBoolean(text));
        }

        @Override
        public String asText() {
            return toScalar(String.class, text -> text);
        }

        protected abstract Boolean isScalar(Predicate<String> predicate);

        @Override
        public boolean isBoolean() {
            return isScalar(text -> StringUtils.equalsAny(text, "true", "false", "TRUE", "FALSE"));
        }

        @Override
        public abstract boolean isArray();

        @Override
        public abstract boolean isContainerNode();

        @Override
        public final synchronized Iterator<OAS3Input> elements() {

            if (this.elements == null) {
                this.elements = discoverElements();
            }

            return this.elements.iterator();
        }

        protected abstract List<OAS3Input> discoverElements();

        private synchronized Map<String, OAS3Input> getFields() {

            if (this.fields == null) {
                Map<String, OAS3Input> container = new HashMap<>();
                container.putAll(discoverChildren());
                container.putAll(discoverAttributes());
                this.fields = container;
            }

            return this.fields;

        }

        protected abstract Map<String, ? extends OAS3Input> discoverChildren();

        protected abstract Map<String, ? extends OAS3Input> discoverAttributes();

        @Override
        public boolean isTextual() {
            return isScalar(value -> true);
        }

        @Override
        public boolean isNumber() {
            return isScalar(NumberUtils::isParsable);
        }

        @Override
        public abstract boolean isAttribute();

        protected final NodeList childrenOf(Node node) {

            NodeList nodeList = node.getChildNodes();

            if ((node.getNodeType() == Node.ELEMENT_NODE || node.getNodeType() == Node.DOCUMENT_NODE) && nodeList == null) {
                return map(newNodeList());
            }

            if (node.getNodeType() == Node.DOCUMENT_NODE && nodeList != null && nodeList.getLength() == 1) {
                return map(nodeList.item(0).getChildNodes());
            }

            return map(nodeList);
        }

        protected final NodeList map(NodeList nodeList) {

            if (nodeList == null) {
                return newNodeList();
            }

            List<Node> container = new ArrayList<>();

            for (int index = 0; index < nodeList.getLength(); index++) {

                Node each = nodeList.item(index);

                if (each.getNodeType() == Node.TEXT_NODE || each.getNodeType() == Node.CDATA_SECTION_NODE || each.getNodeType() == Node.COMMENT_NODE) {
                    continue;
                }

                container.add(each);
            }

            return newNodeList(container);
        }

        protected final NodeList newNodeList() {
            return newNodeList(Collections.emptyList());
        }

        protected final NodeList newNodeList(List<Node> container) {
            Object nativeObject = new Object();

            return (NodeList) Proxy.newProxyInstance(this.getClass().getClassLoader(), new Class[]{NodeList.class}, ((proxy, method, args) -> {

                if (method.getName().equals("item")) {
                    int index = (int) args[0];

                    if (container.isEmpty()) {
                        return null;
                    } else if (index < 0 || index >= container.size()) {
                        return null;
                    } else {
                        return container.get(index);
                    }

                } else if (method.getName().equals("getLength")) {
                    return container.size();
                } else {
                    return method.invoke(nativeObject, args);
                }

            }));
        }

        protected final int getChildrenLength(Node node) {
            return childrenOf(node).getLength();
        }

        protected final void forEachChild(Node node, Consumer<Node> exec) {
            NodeList nodeList = childrenOf(node);
            for (int index = 0; index < nodeList.getLength(); index++) {
                exec.accept(nodeList.item(index));
            }
        }

        protected final NamedNodeMap attributeOf(Node node) {

            if (node.getNodeType() == Node.DOCUMENT_NODE) {
                return node.getChildNodes().item(0).getAttributes();
            }

            return node.getAttributes();
        }

        protected final int sizeOfAttributes(Node node) {
            int size = 0;

            NamedNodeMap attributeList = attributeOf(node);

            if (attributeList != null) {
                for (int index = 0; index < attributeList.getLength(); index++) {
                    Node attribute = attributeList.item(index);

                    if (!attribute.getNodeName().startsWith("xmlns:")) {
                        size++;
                    }

                }
            }

            return size;
        }

        protected final void forEachAttribute(Node node, Consumer<Node> exec) {
            NamedNodeMap attributeList = attributeOf(node);

            if (attributeList != null) {
                for (int index = 0; index < attributeList.getLength(); index++) {
                    Node attribute = attributeList.item(index);

                    if (!attribute.getNodeName().startsWith("xmlns:")) {
                        exec.accept(attributeList.item(index));
                    }

                }
            }
        }

        protected final <Y> Y reduce(Node node, Y initialValue, BiFunction<Y, Node, Y> producer, Predicate<Y> breakIf) {

            Y currentValue = initialValue;

            NodeList nodeList = childrenOf(node);

            for (int index = 0; index < nodeList.getLength(); index++) {
                Node each = nodeList.item(index);

                if (each.getNodeType() == Node.TEXT_NODE && each.getLocalName() == null) {
                    continue;
                }

                currentValue = producer.apply(currentValue, node);

                if (breakIf != null && breakIf.test(currentValue)) {
                    return currentValue;
                }

            }

            return currentValue;
        }

    }

    private static class XmlOAS3InputImpl extends  AbstractXmlOAS3InputImpl {

        private final Node node;

        public XmlOAS3InputImpl(Node node) {
            this.node = node;
        }

        @Override
        public boolean isRoot() {
            return node.getNodeType() == Node.DOCUMENT_NODE;
        }

        @Override
        public String getName() {

            if (node.getNodeType() == Node.DOCUMENT_NODE) {
                return node.getChildNodes().item(0).getLocalName();
            }

            return node.getLocalName();
        }

        @Override
        public String getPrefix() {
            return this.node.getPrefix();
        }

        @Override
        public String getNamespace() {
            return this.node.getNamespaceURI();
        }

        @Override
        public Integer size() {

            if (isArray()) {
                return getChildrenLength(this.node);
            }

            return getChildrenLength(this.node) + sizeOfAttributes(this.node);
        }

        @Override
        public boolean isNull() {
            return this.node == null;
        }

        @Override
        protected <T> T toScalar(Class<T> returnType, Function<String, T> mapper) {
            if (node.getNodeType() == Node.DOCUMENT_NODE) {
                throw new UnsupportedOperationException("Cannot convert Document into " + returnType.getName());
            }

            if (node.getNodeType() == Node.ELEMENT_NODE) {

                if (getChildrenLength(node) != 0) {
                    throw new UnsupportedOperationException("Cannot convert Element into " + returnType.getName());
                }

                return mapper.apply(node.getTextContent());
            }

            if (node.getNodeType() == Node.ATTRIBUTE_NODE) {
                return mapper.apply(node.getTextContent());
            }

            throw new UnsupportedOperationException("Cannot convert Node{type:" + node.getNodeType() + "} into " + returnType.getName());
        }

        @Override
        protected Boolean isScalar(Predicate<String> predicate) {

            if (node.getNodeType() == Node.DOCUMENT_NODE) {
                return false;
            }

            if (node.getNodeType() == Node.ELEMENT_NODE) {

                if (getChildrenLength(node) != 0) {
                    return false;
                }

                return predicate == null || predicate.test(node.getTextContent());
            }

            if (node.getNodeType() == Node.ATTRIBUTE_NODE) {
                return predicate == null || predicate.test(node.getTextContent());
            }

            return false;
        }

        private boolean isObjectType() {

            if (node.getNodeType() != Node.DOCUMENT_NODE && node.getNodeType() != Node.ELEMENT_NODE) {
                return false;
            }

            int size = getChildrenLength(node) + sizeOfAttributes(node);

            if (size == 0 && node.getNodeValue() == null && StringUtils.length(node.getTextContent()) != 0) {
                return false;
            }

            if (size == 0 && node.getNodeValue() == null && StringUtils.length(node.getTextContent()) == 0) {
                return true;
            }
            return size > 0;
        }

        private boolean isNotObjectType() {
            return !isObjectType();
        }

        @Override
        public boolean isArray() {

            if (isNotObjectType()) {
                return false;
            }

            if (sizeOfAttributes(node) > 0) {
                return false;
            }

            if (getChildrenLength(node) == 0) {
                return true;
            }

            AtomicReference<String> returnValue = reduce(node, new AtomicReference<>(), (reference, each) -> {

                if (reference.get() == null) {
                    reference.set(each.getLocalName());
                    return reference;
                }

                if (StringUtils.equals(reference.get(), each.getLocalName())) {
                    return reference;
                }

                return null;
            }, Objects::isNull);

            return returnValue != null;
        }

        @Override
        public boolean isContainerNode() {
            return this.isObjectType();
        }

        @Override
        protected List<OAS3Input> discoverElements() {
            List<OAS3Input> container = new ArrayList<>();
            forEachChild(this.node, each -> container.add(new XmlOAS3InputImpl(each)));
            return container;
        }

        @Override
        protected Map<String, ? extends OAS3Input> discoverChildren() {

            final List<Node> collection = new ArrayList<>();
            forEachChild(this.node, collection::add);

            final Map<String, OAS3Input> container = new HashMap<>();

            collection.stream().map(Node::getLocalName).distinct().forEach(name -> {

                List<Node> target = collection.stream().filter(each -> each.getLocalName() != null)
                        .filter(each -> each.getLocalName().equals(name)).collect(Collectors.toList());

                if (target.size() == 1) {
                    container.put(target.get(0).getLocalName(), new XmlOAS3InputImpl(target.get(0)));
                } else if (target.size() > 1) {
                    XmlOAS3Input[] array= target.stream().map(XmlOAS3InputImpl::new).toArray(XmlOAS3Input[]::new);
                    container.put(target.get(0).getLocalName(), new UnwrappedArrayXmlOAS3InputImpl(name, array));
                }
            });

            return container;
        }

        @Override
        protected Map<String, ? extends OAS3Input> discoverAttributes() {
            final Map<String, OAS3Input> container = new HashMap<>();
            forEachAttribute(this.node, attribute -> container.put(attribute.getLocalName(), new XmlOAS3InputImpl(attribute)));
            return container;
        }

        @Override
        public boolean isAttribute() {
            return node.getNodeType() == Node.ATTRIBUTE_NODE;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            XmlOAS3InputImpl that = (XmlOAS3InputImpl) o;
            return node.isEqualNode(that.node);
        }

        @Override
        public int hashCode() {
            return Objects.hash(node);
        }

    }

    private static class UnwrappedArrayXmlOAS3InputImpl extends AbstractXmlOAS3InputImpl {

        private final String name;
        private final List<XmlOAS3Input> container;

        public UnwrappedArrayXmlOAS3InputImpl(String name, XmlOAS3Input[] seq) {
            this.name = name;
            this.container = seq == null ? Collections.emptyList() : Arrays.asList(seq);
        }

        @Override
        public boolean isRoot() {
            return false;
        }

        @Override
        public String getName() {

            if (this.name != null) {
                return this.name;
            }

            if (this.container.isEmpty()) {
                return null;
            }

            return this.container.get(0).getName();
        }

        @Override
        public String getPrefix() {

            if (this.container.isEmpty()) {
                return null;
            }

            return this.container.get(0).getPrefix();
        }

        @Override
        public String getNamespace() {
            if (this.container.isEmpty()) {
                return null;
            }

            return this.container.get(0).getName();
        }

        @Override
        public Integer size() {
            return this.container.size();
        }

        @Override
        public boolean isNull() {
            return this.container == null;
        }

        @Override
        protected <T> T toScalar(Class<T> returnType, Function<String, T> mapper) {
            throw new UnsupportedOperationException("Cannot convert Node[] into " + returnType.getName());
        }

        @Override
        protected Boolean isScalar(Predicate<String> predicate) {
            return false;
        }

        @Override
        public boolean isArray() {
            return !isNull();
        }

        @Override
        public boolean isContainerNode() {
            return true;
        }

        @Override
        protected List<OAS3Input> discoverElements() {
            return Collections.unmodifiableList(this.container);
        }

        @Override
        protected Map<String, ? extends OAS3Input> discoverChildren() {
            throw new UnsupportedOperationException("Cannot convert Node[] into " + OAS3Input.class.getName());
        }

        @Override
        protected Map<String, ? extends OAS3Input> discoverAttributes() {
            throw new UnsupportedOperationException("Cannot convert Node[] into " + OAS3Input.class.getName());
        }

        @Override
        public boolean isAttribute() {
            return false;
        }

        @Override
        public boolean isVirtual() {
            return true;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;

            if (o == null || getClass() != o.getClass()) return false;

            UnwrappedArrayXmlOAS3InputImpl that = (UnwrappedArrayXmlOAS3InputImpl) o;

            if (!name.equals(that.name)) {
                return false;
            }

            if (container.size() != that.container.size()) {
                return false;
            }

            for(int index =0;index<this.container.size();index++){
                if(!container.get(index).equals(that.container.get(index))){
                    return false;
                }
            }


            return true;
        }

        @Override
        public int hashCode() {
            return Objects.hash(name, container);
        }

    }

}
