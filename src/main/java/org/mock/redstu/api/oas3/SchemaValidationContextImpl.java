package org.mock.redstu.api.oas3;

import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.media.Schema;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Supplier;

public final class SchemaValidationContextImpl implements ReadableSchemaValidationContext {

    private String key;
    private OpenAPI api;
    private ReqPart part;
    private String mediaType;
    private String component;
    private List<ValidationError> errors;
    private List<SchemaValidator> validators;
    private List<Map.Entry<String, String>> xmlNamespaces;

    private SchemaValidationContextImpl() {
    }

    @Override
    public String getKey() {
        return key;
    }

    @Override
    public ReqPart getPart() {
        return part;
    }

    @Override
    public String getMediaType() {
        return this.mediaType;
    }

    @Override
    public String getComponent() {
        return this.component;
    }

    @Override
    public String getXmlPrefixOf(String namespace) {

        if (namespace == null) {
            return null;
        }

        return xmlNamespaces.stream().filter(entry -> StringUtils.equals(entry.getValue(), namespace))
                .map(Map.Entry::getKey).findFirst().orElse(null);
    }

    @Override
    public String getXmlNamespaceOf(String prefix) {

        if (prefix == null) {
            return null;
        }

        return xmlNamespaces.stream().filter(entry -> StringUtils.equals(entry.getKey(), prefix))
                .map(Map.Entry::getValue).findFirst().orElse(null);
    }

    @Override
    public ReadableSchemaValidationContext addError(ValidationError error) {
        if (error != null) {
            this.errors.add(error);
        }
        return this;
    }

    @Override
    public ReadableSchemaValidationContext addErrors(Collection<ValidationError> errors) {
        if (errors != null) {
            this.errors.addAll(errors);
        }
        return this;
    }

    @Override
    public ReadableSchemaValidationContext filter(int index, Schema<?> schema, Object value) {
        return filter(() -> (this.key == null ? "" : this.key) + "[" + index + "]", schema, value);
    }

    @Override
    public ReadableSchemaValidationContext filter(final String path, final Schema<?> schema, final Object value) {
        Supplier<String> keyFactory = () -> Arrays.stream(new String[]{this.key, path}).filter(Objects::nonNull)
                .reduce((acc, v) -> acc.isEmpty() ? v : acc + "." + v).orElse(null);

        return filter(keyFactory, schema, value);
    }

    @Override
    public List<ValidationError> toList() {
        return Collections.unmodifiableList(errors);
    }

    private ReadableSchemaValidationContext filter(final Supplier<String> keyFactory, final Schema<?> schema, final Object value) {

        boolean isValid = false;
        String key = keyFactory.get();
        final Schema<?> definitive = OAS3Utils.toDefinitive(api, schema);
        String component = Optional.ofNullable(schema.get$ref()).map(arg -> StringUtils.split(arg, "/"))
                .map(args -> args[args.length - 1]).orElse(null);

        SchemaValidationContext ctx = key == null ? this : newChildContext(key,component);

        for (SchemaValidator validator : this.validators) {
            boolean isSupported = validator.isSupported(definitive);

            if (isSupported) {
                validator.handle(ctx, definitive, value);
            }

            isValid = isValid || isSupported;
        }

        if (!isValid) {
            addError(OAS3Utils.newInputConstraintViolation(ctx));
        }

        return this;
    }

    private SchemaValidationContext newChildContext(String key, String component) {
        Builder builder = Builder.newInstance().setKey(key).setComponentName(component).setXmlNamespaces(this.xmlNamespaces).setPart(this.part).setMediaType(this.mediaType).setErrors(this.errors).setApi(this.api);
        builder.validators = this.validators;
        return builder.build();
    }

    public static class Builder {

        private String key;
        private OpenAPI api;
        private ReqPart part;
        private String mediaType;
        private String componentName;
        private List<ValidationError> errors;
        private List<SchemaValidator> validators;
        private List<Map.Entry<String, String>> xmlNamespaces;

        public static Builder newInstance() {
            return new Builder();
        }

        public Builder setApi(OpenAPI api) {
            this.api = api;
            return this;
        }

        public Builder setKey(String key) {
            this.key = key;
            return this;
        }

        public Builder setPart(ReqPart part) {
            this.part = part;
            return this;
        }

        public Builder setMediaType(String mediaType) {
            this.mediaType = mediaType;
            return this;
        }

        public Builder addValidator(SchemaValidator validator) {

            if (validator == null) {
                return this;
            }

            if (this.validators == null) {
                this.validators = new ArrayList<>();
            }

            if (!this.validators.contains(validator)) {
                this.validators.add(validator);
            }

            return this;
        }

        public Builder addValidators(List<SchemaValidator> validators) {

            if (validators != null && validators.isEmpty()) {
                this.validators.forEach(this::addValidator);
            }

            return this;
        }

        private Builder setErrors(List<ValidationError> errors) {
            this.errors = errors;
            return this;
        }

        public Builder setXmlNamespaces(List<Map.Entry<String, String>> xmlNamespaces) {
            this.xmlNamespaces = xmlNamespaces;
            return this;
        }

        public Builder setComponentName(String name) {
            this.componentName = name;
            return this;
        }

        public SchemaValidationContextImpl build() {

            SchemaValidationContextImpl instance = new SchemaValidationContextImpl();

            if (part == null) {
                throw new IllegalArgumentException("Request Part isn't optional");
            }

            if (mediaType == null) {
                throw new IllegalArgumentException("Media Type isn't optional");
            }

            if (api == null) {
                throw new IllegalArgumentException("Open API mustn't be null");
            }

            instance.api = this.api;
            instance.key = this.key;
            instance.part = this.part;
            instance.mediaType = this.mediaType;
            instance.component= this.componentName;
            instance.errors = this.errors == null ? new ArrayList<>() : this.errors;
            instance.validators = this.validators == null ? OAS3Utils.SCHEMA_VALIDATORS : this.validators;
            instance.xmlNamespaces = Optional.ofNullable(this.xmlNamespaces).map(Collections::unmodifiableList).orElse(Collections.emptyList());

            return instance;
        }


    }


}
