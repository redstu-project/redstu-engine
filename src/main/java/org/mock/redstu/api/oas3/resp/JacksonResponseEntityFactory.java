package org.mock.redstu.api.oas3.resp;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.eclipse.jgit.util.IO;
import org.mock.redstu.api.MediaTypes;
import org.mock.redstu.api.oas3.OAS3Utils;
import org.mock.redstu.api.oas3.ValidationResult;

import java.io.IOException;
import java.net.URI;
import java.nio.charset.Charset;

public class JacksonResponseEntityFactory implements ResponseEntityFactory {

    @Override
    public boolean supports(String contentType, Object object) {
        return contentType.equals(MediaTypes.APPLICATION_PROBLEM_JSON)
                || contentType.equals(MediaTypes.APPLICATION_PROBLEM_XML)
                || contentType.equals(MediaTypes.APPLICATION_XML)
                || contentType.equals(MediaTypes.APPLICATION_JSON);
    }

    @Override
    public byte[] build(String contentType, Object value) throws IOException {
        Object object = value;

        if (value instanceof ValidationResult) {
            Problem.Builder builder = Problem.builder().withStatusCode(400)
                    .withType(URI.create("/extensions/oas3/problems"))
                    .withTitle("OAS3 CONSTRAINT(S) VIOLATION(S) DISCOVERED")
                    .withDetail("HTTP Request doesn't comply with the OAS3 specification");

            builder.with("cause", ((ValidationResult) value).getErrors());
            object = builder.build();
        }

        if (contentType.equals(MediaTypes.APPLICATION_XML) && object instanceof String) {
            String response = object.toString();

            if (!response.startsWith("<?xml")) {
                response = "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>" + response;
            }

            return response.getBytes(Charset.defaultCharset());
        }

        if (contentType.equals(MediaTypes.APPLICATION_PROBLEM_XML) || contentType.equals(MediaTypes.APPLICATION_XML)) {
            return write(OAS3Utils.XML_OBJECT_MAPPER, object);
        } else {
            return write(OAS3Utils.JSON_OBJECT_MAPPER, object);
        }

    }

    private byte[] write(ObjectMapper objectMapper, Object object) throws IOException {
        return objectMapper.writeValueAsBytes(object);
    }

}
