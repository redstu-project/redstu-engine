package org.mock.redstu.api.oas3.req.schema;

import org.mock.redstu.api.oas3.OAS3Exception;
import org.mock.redstu.api.oas3.OAS3Utils;
import org.mock.redstu.api.oas3.OAS3Violations;
import org.mock.redstu.api.oas3.SchemaValidationContext;
import org.mock.redstu.api.oas3.ValidationError;
import org.mock.redstu.api.oas3.req.OAS3Input;

import java.math.BigDecimal;

public class FloatSchemaValidator extends AbstractNumberSchemaValidator {

    @Override
    protected BigDecimal parse(Object value) {

        if (value instanceof Float) {
            return BigDecimal.valueOf((float) value);
        }

        if (value instanceof String) {
            return new BigDecimal((String) value);
        }

        if (value instanceof BigDecimal) {
            return (BigDecimal) value;
        }

        if (value instanceof OAS3Input) {
            return parse(((OAS3Input) value).numberValue());
        }

        throw new OAS3Exception("Cannot convert " + value + " into Float");
    }

    @Override
    protected BigDecimal getMinimumValue() {
        return BigDecimal.valueOf(Float.MIN_VALUE);
    }

    @Override
    protected BigDecimal getMaximumValue() {
        return BigDecimal.valueOf(Float.MAX_VALUE);
    }

    @Override
    public String[] getFormat() {
        return new String[]{"float"};
    }

    @Override
    protected ValidationError newInputConstraintViolation(SchemaValidationContext context) {
        return OAS3Utils.newConstraintViolation(context, OAS3Violations.INVALID_INPUT_FOR_FLOAT_FIELD);
    }

}

