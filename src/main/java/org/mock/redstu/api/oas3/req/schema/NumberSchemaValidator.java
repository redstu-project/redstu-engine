package org.mock.redstu.api.oas3.req.schema;

import io.swagger.v3.oas.models.media.NumberSchema;
import io.swagger.v3.oas.models.media.Schema;
import org.mock.redstu.api.oas3.req.OAS3Input;

import java.math.BigDecimal;
import java.math.BigInteger;

public class NumberSchemaValidator extends AbstractNumberSchemaValidator {

    @Override
    public boolean isSupported(Schema<?> schema) {

        if (schema == null) {
            return false;
        }

        return schema instanceof NumberSchema && (schema.getFormat() == null || schema.getFormat().equals("none"));
    }

    @Override
    protected BigDecimal parse(Object value) {

        if (value instanceof Double) {
            return BigDecimal.valueOf((double) value);
        }

        if (value instanceof Float) {
            return BigDecimal.valueOf((float) value);
        }

        if (value instanceof Long) {
            return BigDecimal.valueOf((long) value);
        }

        if (value instanceof Integer) {
            return new BigDecimal((int) value);
        }

        if (value instanceof BigDecimal) {
            return (BigDecimal) value;
        }

        if (value instanceof BigInteger) {
            return parse(value.toString());
        }

        if (value instanceof OAS3Input) {
            return parse(((OAS3Input) value).numberValue());
        }

        return new BigDecimal(value.toString());
    }

    @Override
    protected BigDecimal getMinimumValue() {
        return BigDecimal.valueOf(Double.MIN_VALUE);
    }

    @Override
    protected BigDecimal getMaximumValue() {
        return BigDecimal.valueOf(Double.MAX_VALUE);
    }

    @Override
    public String[] getFormat() {
        return new String[]{};
    }



}

