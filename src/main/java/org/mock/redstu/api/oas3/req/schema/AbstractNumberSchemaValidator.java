package org.mock.redstu.api.oas3.req.schema;

import io.swagger.v3.oas.models.media.NumberSchema;
import io.swagger.v3.oas.models.media.Schema;
import org.apache.commons.lang3.math.NumberUtils;
import org.mock.redstu.api.oas3.OAS3Violations;
import org.mock.redstu.api.oas3.OAS3Utils;
import org.mock.redstu.api.oas3.SchemaValidator;
import org.mock.redstu.api.oas3.SchemaValidationContext;
import org.mock.redstu.api.oas3.ValidationError;
import org.mock.redstu.api.oas3.req.OAS3Input;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Optional;

public abstract class AbstractNumberSchemaValidator extends AbstractSchemaValidator<BigDecimal, Schema<?>> implements SchemaValidator {

    @Override
    public boolean isSupported(Schema<?> schema) {

        if (schema == null) {
            return false;
        }

        return schema instanceof NumberSchema && schema.getFormat() != null
                && Arrays.stream(getFormat()).anyMatch(format -> format.equals(schema.getFormat()));
    }

    @Override
    protected boolean isParsable(Object value) {

        if (value instanceof Number) {
            return true;
        }

        if (value instanceof OAS3Input && ((OAS3Input) value).isNumber()) {
            return true;
        }

        return value instanceof String && NumberUtils.isCreatable((String) value);
    }

    @Override
    protected ValidationError newInputConstraintViolation(SchemaValidationContext context) {
        return OAS3Utils.newConstraintViolation(context, OAS3Violations.INVALID_INPUT_FOR_NUMBER_FIELD);
    }

    @Override
    protected void apply(SchemaValidationContext context, Schema<?> schema, BigDecimal object) {
        BigDecimal maximum = getMaximumValue();
        BigDecimal minimum = getMaximumValue().negate();

        boolean withinMinimumBoundary = object.compareTo(minimum) >= 0;

        if (!withinMinimumBoundary) {
            context.addError(OAS3Utils.newConstraintViolation(context, OAS3Violations.MINIMUM_LIMIT_VIOLATION_FOR_FIELD));
            return;
        }

        boolean withinMaximumBoundary = object.compareTo(maximum) <= 0;

        if (!withinMaximumBoundary) {
            context.addError(OAS3Utils.newConstraintViolation(context, OAS3Violations.MAXIMUM_LIMIT_VIOLATION_FOR_FIELD));
            return;
        }

        if (schema.getEnum() != null && !schema.getEnum().isEmpty() && !contains(schema, object)) {
            context.addError(OAS3Utils.newInvalidOptionConstraintViolation(context));
        }

        if (!isGreaterOrEqualToMinimum(object, schema)) {
            context.addError(OAS3Utils.newConstraintViolation(context,
                    OAS3Violations.MINIMUM_LIMIT_VIOLATION_FOR_FIELD));
        }

        if (!isLesserOrEqualToMaximum(object, schema)) {
            context.addError(OAS3Utils.newConstraintViolation(context,
                    OAS3Violations.MAXIMUM_LIMIT_VIOLATION_FOR_FIELD));
        }

        if (schema.getMultipleOf() != null && !isMultiple(object, schema)) {
            context.addError(OAS3Utils.newConstraintViolation(context,
                    OAS3Violations.MULTIPLE_OF_VIOLATION_FOR_FIELD));
        }

    }

    protected abstract String[] getFormat();

    protected boolean contains(Schema<?> schema, BigDecimal object) {
        return schema.getEnum().stream().anyMatch(each -> each.equals(object));
    }

    protected boolean isMultiple(BigDecimal value, Schema<?> schema) {
        return Optional.ofNullable(schema.getMultipleOf())
                .map(multipleOf -> value.remainder(multipleOf).equals(BigDecimal.ZERO)).orElse(false);
    }

    protected boolean isGreaterOrEqualToMinimum(BigDecimal value, Schema<?> schema) {
        return Optional.ofNullable(schema.getMinimum()).map(minimum -> {
            if (schema.getExclusiveMinimum() != null && schema.getExclusiveMinimum()) {
                return minimum.add(getMinimumValue());
            }
            return minimum;
        }).map(minimum -> value.compareTo(minimum) >= 0).orElse(true);
    }

    protected boolean isLesserOrEqualToMaximum(BigDecimal value, Schema<?> schema) {
        return Optional.ofNullable(schema.getMaximum()).map(maximum -> {
            if (schema.getExclusiveMaximum() != null && schema.getExclusiveMaximum()) {
                return maximum.subtract(getMinimumValue());
            }
            return maximum;
        }).map(minimum -> value.compareTo(minimum) <= 0).orElse(true);
    }

    protected abstract BigDecimal getMinimumValue();

    protected abstract BigDecimal getMaximumValue();


}
