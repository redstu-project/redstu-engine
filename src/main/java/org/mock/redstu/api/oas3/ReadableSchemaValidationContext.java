package org.mock.redstu.api.oas3;

import io.swagger.v3.oas.models.media.Schema;

import java.util.Collection;
import java.util.List;

public interface ReadableSchemaValidationContext extends SchemaValidationContext {

    List<ValidationError> toList();

    ReadableSchemaValidationContext addError(ValidationError error);

    ReadableSchemaValidationContext addErrors(Collection<ValidationError> errors);

    ReadableSchemaValidationContext filter(int key, Schema<?> schema, Object value);

    ReadableSchemaValidationContext filter(String key, Schema<?> schema, Object value);

}
