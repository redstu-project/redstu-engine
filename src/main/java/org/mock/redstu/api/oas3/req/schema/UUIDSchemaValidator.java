package org.mock.redstu.api.oas3.req.schema;

import io.swagger.v3.oas.models.media.Schema;
import org.apache.commons.lang3.StringUtils;
import org.mock.redstu.api.oas3.OAS3Utils;
import org.mock.redstu.api.oas3.OAS3Violations;
import org.mock.redstu.api.oas3.SchemaValidationContext;
import org.mock.redstu.api.oas3.ValidationError;

import java.util.regex.Pattern;

public class UUIDSchemaValidator extends AbstractTextSchemaValidator {

    private static final Pattern PATTERN =
            Pattern.compile("^[0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}$");

    @Override
    public boolean isSupported(Schema<?> schema) {
        return super.isSupported(schema) && StringUtils.equals(schema.getFormat(), "uuid");
    }

    @Override
    protected void doValidate(SchemaValidationContext context, Schema<?> schema, String object) {
        if (!PATTERN.matcher(object).matches()) {
            context.addError(newInputConstraintViolation(context));
        }
    }

    protected ValidationError newInputConstraintViolation(SchemaValidationContext context) {
        return OAS3Utils.newConstraintViolation(context, OAS3Violations.INVALID_INPUT_FOR_UUID_FIELD);
    }

}
