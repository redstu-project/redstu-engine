package org.mock.redstu.api.oas3.resp;


import java.io.IOException;

/**
 * Factory to build the response body for Validation Errors
 */
public interface ResponseEntityFactory {

    boolean supports(String contentType, Object value);

    byte[] build(String contentType, Object payload) throws IOException;

}
