package org.mock.redstu.api.oas3;

import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.Operation;
import io.swagger.v3.oas.models.PathItem;
import io.swagger.v3.oas.models.examples.Example;
import io.swagger.v3.oas.models.media.Content;
import io.swagger.v3.oas.models.media.MediaType;
import io.swagger.v3.oas.models.responses.ApiResponse;
import io.swagger.v3.oas.models.responses.ApiResponses;
import org.mock.redstu.api.MediaTypes;
import org.mock.redstu.api.common.BinaryContent;
import org.mock.redstu.api.common.Header;
import org.mock.redstu.api.oas3.req.validator.ParametersValidator;
import org.mock.redstu.api.oas3.req.validator.MediaTypeValidator;
import org.mock.redstu.api.oas3.resp.ResponseEntityFactory;
import org.mock.redstu.api.req.HttpReq;
import org.mock.redstu.api.resp.HttpResp;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.Map;
import java.util.Optional;

public class OAS3APIOperation implements APIOperation {

    private OpenAPI oas3;
    private PathItem pathItem;
    private Operation operation;
    private ResponseEntityFactory responseEntityFactory;

    @Override
    public void sendExample(int statusCode, String name, HttpReq req, HttpResp resp) {
        ApiResponses responses = operation.getResponses();

        if (responses == null || responses.isEmpty()) {
            throw new OAS3Exception("Responses not found");
        }

        ApiResponse response = responses.get("" + statusCode);

        if (response == null) {
            throw new OAS3Exception("Response for code" + statusCode + " couldn't be found");
        }

        String contentType = Optional.ofNullable(OAS3Utils.extractMediaType(req.headers().get("Accept")))
                .orElseGet(() -> OAS3Utils.extractMediaType(req.headers().get("Content-Type")));

        Content content = response.getContent();

        if (content == null) {
            throw new OAS3Exception("Content not found");
        }

        MediaType mediaType = content.get(contentType);

        if (mediaType == null) {
            throw new OAS3Exception("Media Type " + contentType + " not found");
        }

        Map<String, Example> examples = mediaType.getExamples();

        if (examples == null || examples.isEmpty()) {
            throw new OAS3Exception("Cannot find example " + name);
        }

        Example example = OAS3Utils.toDefinitive(oas3, examples.get(name));

        if (example == null) {
            throw new OAS3Exception("Cannot find example " + name);
        }

        sendExample(statusCode, resp, contentType, example);
    }

    private void sendExample(int statusCode, HttpResp resp, String contentType, Example example) {
        try {
            resp.setStatus(statusCode);
            resp.setContentType(contentType);
            resp.sendText(new String(responseEntityFactory.build(contentType, example.getValue()), Charset.defaultCharset()));
        } catch (IOException ex) {
            throw new OAS3Exception("Cannot send error", ex);
        }
    }

    @Override
    public ValidationResult validate(HttpReq req, BinaryContent content) throws OAS3Exception {

        try {

            String mediaType = Optional.ofNullable(req.headers().get("Content-Type"))
                    .map(Header::value).orElse(MediaTypes.TEXT_HTML);

            ValidationContext context = newContext(pathItem, operation,mediaType, content);

            ValidatorChainImpl.builder().withReq(req).withValidator(new ParametersValidator())
                    .withValidator(new MediaTypeValidator()).withContext(context).build().proceed();

            return context.toResult();

        } catch (OAS3Exception ex) {
            throw ex;
        } catch (Exception ex) {
            throw new OAS3Exception(null, ex);
        }

    }

    public void setOas3(OpenAPI oas3) {
        this.oas3 = oas3;
    }

    public void setPathItem(PathItem pathItem) {
        this.pathItem = pathItem;
    }

    public void setOperation(Operation operation) {
        this.operation = operation;
    }

    public void setResponseEntityFactory(ResponseEntityFactory responseEntityFactory) {
        this.responseEntityFactory = responseEntityFactory;
    }

    private ValidationContext newContext(PathItem pathItem, Operation operation, String mediaType, BinaryContent content) {
        ValidationContextImpl context = new ValidationContextImpl();
        context.setBody(content);
        context.setPath(pathItem);
        context.setOperation(operation);
        context.setMediaType(mediaType);
        context.setSpecification(this.oas3);
        context.setResponseEntityFactory(this.responseEntityFactory);
        return context;
    }

}
