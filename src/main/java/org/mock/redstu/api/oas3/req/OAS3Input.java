package org.mock.redstu.api.oas3.req;

import java.util.Iterator;

/**
 * Represents an object from incoming request
 */
public interface OAS3Input {

    Integer size();

    boolean isContainerNode();

    OAS3Input get(String fieldName);

    boolean isNull();

    Iterator<String> fieldNames();

    Number numberValue();

    Boolean booleanValue();

    boolean isBoolean();

    boolean isArray();

    Iterator<OAS3Input> elements();

    boolean isTextual();

    String asText();

    boolean isNumber();

}
