package org.mock.redstu.api.oas3;

public class DefaultValidationError implements ErrorCodeAwareValidationError {

    private final String key;
    private final ReqPart part;
    private final OAS3Violations violation;

    public DefaultValidationError(ReqPart part, String key, OAS3Violations violation) {
        this.part = part;
        this.key = key;
        this.violation = violation;
    }

    @Override
    public String getKey() {
        return this.key;
    }

    @Override
    public ReqPart getPart() {
        return this.part;
    }

    @Override
    public String getMessage() {
        return this.violation.message;
    }

    @Override
    public String getCode() {
        return violation.errorCode;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();

        if (key != null) {
            sb.append("\"key\"").append(":").append("\"").append(getKey()).append("\"");
        } else {
            sb.append("\"key\"").append(":").append("null");
        }

        if (part != null) {
            sb.append(",").append("\"part\"").append(":").append("\"").append(getPart().name()).append("\"");
        } else {
            sb.append(",").append("\"part\"").append(":").append("null");
        }

        if (violation != null) {
            sb.append(",").append("\"cause\"").append(":").append("\"").append(getMessage()).append("\"");
        } else {
            sb.append(",").append("\"cause\"").append(":").append("null");
        }

        return sb.insert(0, "{").append("}").toString();
    }

}
