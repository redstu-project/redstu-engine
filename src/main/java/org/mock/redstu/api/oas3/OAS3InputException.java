package org.mock.redstu.api.oas3;

public class OAS3InputException extends OAS3Exception {

    public OAS3InputException(String message, Throwable cause) {
        super(message, cause);
    }

}
