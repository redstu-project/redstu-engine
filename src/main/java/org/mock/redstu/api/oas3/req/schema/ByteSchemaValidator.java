package org.mock.redstu.api.oas3.req.schema;

import io.swagger.v3.oas.models.media.Schema;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.validator.GenericValidator;
import org.mock.redstu.api.oas3.OAS3Utils;
import org.mock.redstu.api.oas3.OAS3Violations;
import org.mock.redstu.api.oas3.SchemaValidationContext;
import org.mock.redstu.api.oas3.ValidationError;

public class ByteSchemaValidator extends AbstractTextSchemaValidator {

    @Override
    public boolean isSupported(Schema<?> schema) {
        return super.isSupported(schema) && StringUtils.equals(schema.getFormat(), "byte");
    }

    @Override
    protected void doValidate(SchemaValidationContext context, Schema<?> schema, String object) {
        if (!GenericValidator.isByte(object)) {
            context.addError(newInputConstraintViolation(context));
        }
    }

    protected ValidationError newInputConstraintViolation(SchemaValidationContext context) {
        return OAS3Utils.newConstraintViolation(context, OAS3Violations.INVALID_INPUT_FOR_BYTE_FIELD);
    }

}
