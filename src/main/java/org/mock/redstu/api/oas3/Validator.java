package org.mock.redstu.api.oas3;

import org.mock.redstu.api.req.HttpReq;

public interface Validator {

    void execute(ValidationContext context, ValidatorChain chain, HttpReq req);

}
