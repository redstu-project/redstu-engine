package org.mock.redstu.api.oas3;

import org.mock.redstu.server.RedishException;

public class OAS3Exception extends RedishException {

    public OAS3Exception(String s) {
        super(s);
    }

    public OAS3Exception(String message, Throwable cause) {
        super(message, cause);
    }

}
