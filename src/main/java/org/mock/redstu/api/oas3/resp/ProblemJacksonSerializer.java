package org.mock.redstu.api.oas3.resp;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import java.io.IOException;
import java.util.Map;

public class ProblemJacksonSerializer extends JsonSerializer<Problem> {

    @Override
    public void serialize(Problem problem, JsonGenerator gen, SerializerProvider sp) throws IOException {
        gen.writeStartObject();
        gen.writeObjectField("type", problem.getType());
        gen.writeStringField("title", problem.getTitle());
        gen.writeStringField("detail", problem.getDetail());
        gen.writeNumberField("status", problem.getStatusCode());

        if (problem.getAdditionalProperties() != null && !problem.getAdditionalProperties().isEmpty()) {
            for (Map.Entry<String, Object> kv : problem.getAdditionalProperties().entrySet()) {
                gen.writeObjectField(kv.getKey(), kv.getValue());
            }
        }

        gen.writeEndObject();
    }

}
