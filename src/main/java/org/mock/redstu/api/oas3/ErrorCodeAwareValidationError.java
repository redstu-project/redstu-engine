package org.mock.redstu.api.oas3;

public interface ErrorCodeAwareValidationError extends ValidationError {
    String getCode();
}
