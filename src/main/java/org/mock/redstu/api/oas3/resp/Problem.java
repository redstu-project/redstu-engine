package org.mock.redstu.api.oas3.resp;

import io.undertow.util.StatusCodes;
import org.apache.commons.lang3.StringUtils;

import java.net.URI;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class Problem {

    private URI type;
    private String title;
    private String detail;
    private int statusCode;
    private Map<String, Object> additionalProperties;

    public URI getType() {
        return type;
    }

    public String getTitle() {
        return title;
    }

    public String getDetail() {
        return detail;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public Map<String, Object> getAdditionalProperties() {
        return additionalProperties;
    }

    public static Builder builder() {
        return new Builder();
    }

    public static class Builder {

        private URI type;
        private String title;
        private String detail;
        private Integer statusCode;
        private Map<String, Object> additionalProperties;

        public Builder withType(URI type) {
            this.type = type;
            return this;
        }

        public Builder withTitle(String title) {
            this.title = title;
            return this;
        }

        public Builder withStatusCode(int statusCode) {

            if (statusCode < 100 || statusCode > 511) {
                return this;
            }

            this.statusCode = statusCode;
            return this;
        }

        public Builder withDetail(String detail) {
            this.detail = detail;
            return this;
        }

        public Builder with(String key, Object object) {

            if (StringUtils.equalsAny(key, "type", "title", "detail", "status")) {
                return this;
            }

            if (this.additionalProperties == null) {
                this.additionalProperties = new HashMap<>();
            }

            if (object == null) {
                additionalProperties.remove(key);
            } else {
                additionalProperties.put(key, object);
            }

            return this;
        }

        public Problem build() {
            Problem instance = new Problem();

            instance.type = this.type == null ? URI.create("/problems") : type;
            instance.statusCode = this.statusCode == null ? 500 : this.statusCode;
            instance.title = title == null ? StatusCodes.getReason(instance.statusCode) : this.title;
            instance.detail = this.detail == null ? StatusCodes.getReason(instance.statusCode) : this.detail;
            instance.additionalProperties = this.additionalProperties == null ? null
                    : Collections.unmodifiableMap(this.additionalProperties);

            return instance;
        }

    }


}
