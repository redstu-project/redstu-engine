package org.mock.redstu.api.oas3.req.validator;

import io.swagger.v3.oas.models.media.Content;
import io.swagger.v3.oas.models.media.MediaType;
import io.swagger.v3.oas.models.media.Schema;
import io.swagger.v3.oas.models.parameters.RequestBody;
import org.apache.commons.lang3.StringUtils;
import org.mock.redstu.api.common.BinaryContent;
import org.mock.redstu.api.oas3.DefaultValidationError;
import org.mock.redstu.api.oas3.OAS3Exception;
import org.mock.redstu.api.oas3.OAS3Utils;
import org.mock.redstu.api.oas3.OAS3Violations;
import org.mock.redstu.api.oas3.ReqPart;
import org.mock.redstu.api.oas3.ValidationContext;
import org.mock.redstu.api.oas3.Validator;
import org.mock.redstu.api.oas3.ValidatorChain;
import org.mock.redstu.api.oas3.SchemaValidationContextImpl;
import org.mock.redstu.api.req.HttpReq;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Optional;

public abstract class AbstractMediaTypeValidator implements Validator {

    @Override
    public void execute(ValidationContext context, ValidatorChain chain, HttpReq req) {

        String contentType = req.headers().get("Content-Type").value();

        if (!isContentType(contentType)) {
            return;
        }

        RequestBody requestBodyConfig = context.getOperation().getRequestBody();

        if (requestBodyConfig == null) {
            return;
        }

        Content contentConfig = requestBodyConfig.getContent();

        if (contentConfig == null) {
            return;
        }

        MediaType mediaType = contentConfig.get(getMediaType());

        if (mediaType == null) {
            return;
        }

        execute(context, null, mediaType.getSchema(), extractBody(context.getBody()));

        chain.interrupt();
    }

    protected void execute(ValidationContext context, String ref, Schema<?> targetSchema, InputStream io) {
        try {

            if (targetSchema.getType() == null && targetSchema.get$ref() == null) {
                throw new OAS3Exception("Invalid OAS3 yaml");
            }

            if (targetSchema.getType() != null && targetSchema.get$ref() != null) {
                throw new OAS3Exception("Invalid OAS3 yaml");
            }

            Schema<?> schema = targetSchema;

            if (targetSchema.get$ref() != null) {
                schema = OAS3Utils.toDefinitive(context.getSpecification(), targetSchema);
                execute(context, targetSchema.get$ref(), schema, io);
                return;
            }

            Object object = getObject(context, io);

            if (object == null) {
                context.addViolations(new DefaultValidationError(ReqPart.Body, null, OAS3Violations.MISSING_INPUT_FOR_REQUIRED_FIELD));
                return;
            }

            String component = Optional.ofNullable(ref).map(arg -> StringUtils.split(arg, "/"))
                    .map(args -> args[args.length - 1]).orElse(null);

            context.addViolations(SchemaValidationContextImpl.Builder.newInstance().setPart(ReqPart.Body)
                    .setKey(null).setComponentName(component).setMediaType(getMediaType()).addValidators(OAS3Utils.SCHEMA_VALIDATORS)
                    .setApi(context.getSpecification()).setXmlNamespaces(OAS3Utils.getXmlNamespaces(schema)).build()
                    .filter(null, schema, object).toList());

        } catch (IOException ex) {
            ex.printStackTrace();
            context.addViolations(OAS3Utils.newConstraintViolation(ReqPart.Body, null, OAS3Violations.INVALID_INPUT_FOR_FIELD));
        }
    }

    protected abstract Object getObject(ValidationContext context, InputStream io) throws IOException;

    protected boolean isContentType(String contentType) {
        return StringUtils.equals(getMediaType(), StringUtils.split(contentType, ";")[0]);
    }

    protected InputStream extractBody(BinaryContent binary) {
        return new ByteArrayInputStream(binary.toArray());
    }

    protected abstract String getMediaType();

}
