package org.mock.redstu.api.oas3.js;

import org.mock.redstu.api.oas3.ValidationError;
import org.mock.redstu.js.AbstractProxyObject;

public class ValidationErrorProxy extends AbstractProxyObject {

    public ValidationErrorProxy(ValidationError instance) {
        readable("key", instance::getKey);
        readable("message", instance::getMessage);
        readable("part", () -> instance.getPart().name);
    }

}
