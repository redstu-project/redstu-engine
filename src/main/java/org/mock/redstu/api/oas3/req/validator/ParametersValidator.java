package org.mock.redstu.api.oas3.req.validator;

import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.parameters.CookieParameter;
import io.swagger.v3.oas.models.parameters.HeaderParameter;
import io.swagger.v3.oas.models.parameters.Parameter;
import io.swagger.v3.oas.models.parameters.PathParameter;
import io.swagger.v3.oas.models.parameters.QueryParameter;
import org.apache.commons.lang3.StringUtils;
import org.mock.redstu.api.common.Header;
import org.mock.redstu.api.common.HttpCookie;
import org.mock.redstu.api.oas3.DefaultValidationError;
import org.mock.redstu.api.oas3.InvalidOAS3YamlException;
import org.mock.redstu.api.oas3.OAS3Exception;
import org.mock.redstu.api.oas3.OAS3Utils;
import org.mock.redstu.api.oas3.OAS3Violations;
import org.mock.redstu.api.oas3.ReqPart;
import org.mock.redstu.api.oas3.ValidationContext;
import org.mock.redstu.api.oas3.ValidationError;
import org.mock.redstu.api.oas3.Validator;
import org.mock.redstu.api.oas3.ValidatorChain;
import org.mock.redstu.api.oas3.SchemaValidationContextImpl;
import org.mock.redstu.api.req.HttpParam;
import org.mock.redstu.api.req.HttpReq;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class ParametersValidator implements Validator {

    @Override
    public void execute(ValidationContext context, ValidatorChain chain, HttpReq req) {

        OpenAPI oas3 = context.getSpecification();

        List<Parameter> params = Stream.concat(context.getPath().getParameters().stream(), context.getOperation()
                .getParameters().stream()).map(param -> OAS3Utils.toDefinitive(oas3, param))
                .collect(Collectors.toList());

        params
                .forEach(param -> execute(oas3, param, extractValue(param, req), context));

    }

    private void execute(OpenAPI doc, Parameter param, String value, ValidationContext context) {

        if ((param.getRequired() == null || !param.getRequired()) && value == null) {
            return;
        }

        if (hasType(HeaderParameter.class, "header", param)) {
            context.addViolations(header(context, doc, param, value));
        } else if (hasType(QueryParameter.class, "query", param)) {
            context.addViolations(query(context, doc, param, value));
        } else if (hasType(PathParameter.class, "path", param)) {
            context.addViolations(path(context, doc, param, value));
        } else if (hasType(CookieParameter.class, "cookie", param)) {
            context.addViolations(cookie(context, doc, param, value));
        } else if (hasType(Parameter.class, null, param) && param.get$ref() != null) {
            String key = param.get$ref().substring(24);
            execute(doc, doc.getComponents().getParameters().get(key), value, context);
        } else {
            throw new OAS3Exception("Unsupported Parameter :" + param);
        }

    }

    private List<? extends ValidationError> path(ValidationContext context, OpenAPI doc, Parameter param, String value) {
        return in(context, doc, param, ReqPart.Path, value);
    }

    private List<? extends ValidationError> query(ValidationContext context, OpenAPI doc, Parameter param, String value) {
        return in(context, doc, param, ReqPart.Query, value);
    }

    private List<? extends ValidationError> header(ValidationContext context, OpenAPI doc, Parameter param, String value) {
        return in(context, doc, param, ReqPart.Header, value);
    }

    private List<? extends ValidationError> cookie(ValidationContext context, OpenAPI doc, Parameter param, String value) {
        return in(context, doc, param, ReqPart.Cookie, value);
    }

    private boolean hasType(Class<? extends Parameter> valueType, String name, Parameter param) {

        if (name == null) {
            return valueType.equals(param.getClass());
        }

        return valueType.equals(param.getClass()) || param.getIn() != null && param.getIn().equals(name);
    }

    private List<? extends ValidationError> in(ValidationContext context, OpenAPI doc, Parameter param, ReqPart part, String value) {
        boolean isRequired = Optional.ofNullable(param.getRequired()).orElse(false);

        if (isRequired && value == null) {
            return Collections.singletonList(new DefaultValidationError(part, param.getName(),
                    OAS3Violations.MISSING_INPUT_FOR_REQUIRED_FIELD));
        }

        boolean isAllowEmptyValue = Optional.ofNullable(param.getAllowEmptyValue()).orElse(true);

        if (value != null && !isAllowEmptyValue && StringUtils.isEmpty(value)) {
            return Collections.singletonList(new DefaultValidationError(part, param.getName(),
                    OAS3Violations.EMPTY_INPUT_NOT_ALLOWED_FOR_FIELD));
        }

        if (value == null && param.getSchema() != null && param.getSchema().getNullable() != null
                && !param.getSchema().getNullable()) {
            return Collections.singletonList(new DefaultValidationError(part, param.getName(),
                    OAS3Violations.INVALID_NULL_INPUT_FOR_FIELD));
        }

        if (param.getContent() != null && param.getSchema() != null) {
            throw new InvalidOAS3YamlException("Invalid OAS3 yaml");
        }

        if (param.getContent() != null && param.getContent().isEmpty()) {
            throw new InvalidOAS3YamlException("Invalid OAS3 yaml");
        }

        if (param.getContent() != null && !param.getContent().isEmpty() && param.getContent().size() != 1) {
            throw new InvalidOAS3YamlException("Invalid OAS3 yaml");
        }

        if (param.getSchema() != null) {
            return SchemaValidationContextImpl.Builder.newInstance().setPart(part).setKey(param.getName())
                    .setApi(doc).setMediaType(context.getMediaType()).setXmlNamespaces(OAS3Utils.getXmlNamespaces(param.getSchema()))
                    .build().filter(null, param.getSchema(), value).toList();
        }

        if (param.getContent() != null) {
            throw new UnsupportedOperationException();
        }

        return Collections.emptyList();
    }

    private String extractValue(final Parameter param, final HttpReq req) {
        String inParam = param.getIn();

        if (StringUtils.equals(inParam, "header") || param instanceof HeaderParameter) {
            return Optional.ofNullable(req.headers().get(param.getName())).map(Header::value)
                    .orElse(null);
        } else if (StringUtils.equals(inParam, "query") || param instanceof QueryParameter) {
            return Optional.ofNullable(req.queryParams().get(param.getName())).map(HttpParam::string)
                    .orElse(null);
        } else if (StringUtils.equals(inParam, "cookie") || param instanceof CookieParameter) {
            return Optional.ofNullable(req.cookies().get(param.getName())).map(HttpCookie::getValue)
                    .orElse(null);
        } else if (StringUtils.equals(inParam, "path") || param instanceof PathParameter) {
            return Optional.ofNullable(req.pathParams().get(param.getName())).map(HttpParam::string)
                    .orElse(null);
        } else {
            throw new OAS3Exception("Unsupported Parameter \n" + param);
        }

    }

}
