package org.mock.redstu.api.oas3;

public interface ValidatorChain {

    void proceed();

    void interrupt();

    boolean isInterrupted();

}
