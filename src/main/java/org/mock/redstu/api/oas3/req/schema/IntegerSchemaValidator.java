package org.mock.redstu.api.oas3.req.schema;

import io.swagger.v3.oas.models.media.IntegerSchema;
import io.swagger.v3.oas.models.media.Schema;
import org.apache.commons.lang3.StringUtils;
import org.mock.redstu.api.oas3.OAS3Exception;
import org.mock.redstu.api.oas3.OAS3Utils;
import org.mock.redstu.api.oas3.OAS3Violations;
import org.mock.redstu.api.oas3.SchemaValidationContext;
import org.mock.redstu.api.oas3.ValidationError;
import org.mock.redstu.api.oas3.req.OAS3Input;

import java.math.BigDecimal;
import java.util.Optional;

public class IntegerSchemaValidator extends AbstractNumberSchemaValidator {

    @Override
    public boolean isSupported(Schema<?> schema) {

        if (schema == null) {
            return false;
        }

        return schema instanceof IntegerSchema && Optional.ofNullable(schema.getFormat())
                .map(format -> format.equals("int32") || format.equals("int64")).orElse(true);
    }

    @Override
    protected boolean isParsable(Object value) {
        boolean isValueSupported = super.isParsable(value);

        if (!isValueSupported) {
            return false;
        }

        return StringUtils.isNumeric((String) value);
    }

    @Override
    protected BigDecimal parse(Object value) {

        if (value instanceof Integer) {
            return BigDecimal.valueOf((int) value);
        }

        if (value instanceof String) {
            return new BigDecimal((String) value);
        }

        if (value instanceof BigDecimal) {
            return (BigDecimal) value;
        }

        if (value instanceof OAS3Input) {
            return parse(((OAS3Input) value).numberValue());
        }

        throw new OAS3Exception("Cannot convert " + value + " into Integer");
    }

    @Override
    protected BigDecimal getMinimumValue() {
        return BigDecimal.ONE;
    }

    @Override
    protected BigDecimal getMaximumValue() {
        return BigDecimal.valueOf(Integer.MAX_VALUE);
    }

    @Override
    public String[] getFormat() {
        return new String[]{"int32", "int64"};
    }

    @Override
    protected ValidationError newInputConstraintViolation(SchemaValidationContext context) {
        return OAS3Utils.newConstraintViolation(context, OAS3Violations.INVALID_INPUT_FOR_INTEGER_FIELD);
    }

}

