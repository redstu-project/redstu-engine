package org.mock.redstu.api.oas3.req.schema;

import io.swagger.v3.oas.models.media.BooleanSchema;
import io.swagger.v3.oas.models.media.Schema;
import org.mock.redstu.api.oas3.OAS3Exception;
import org.mock.redstu.api.oas3.OAS3Violations;
import org.mock.redstu.api.oas3.SchemaValidator;
import org.mock.redstu.api.oas3.OAS3Utils;
import org.mock.redstu.api.oas3.SchemaValidationContext;
import org.mock.redstu.api.oas3.ValidationError;
import org.mock.redstu.api.oas3.req.OAS3Input;

public final class BooleanSchemaValidator extends AbstractSchemaValidator<Boolean, BooleanSchema> implements SchemaValidator {

    @Override
    public boolean isSupported(Schema<?> schema) {
        return schema instanceof BooleanSchema && schema.getFormat() == null;
    }

    @Override
    protected Boolean parse(Object value) {
        if (value instanceof Boolean) {
            return (Boolean) value;
        } else if (value instanceof String) {
            return Boolean.parseBoolean((String) value);
        } else if (value instanceof OAS3Input) {
            return ((OAS3Input) value).booleanValue();
        } else {
            throw new OAS3Exception("Cannot parse " + value + " into Boolean");
        }
    }

    @Override
    protected boolean isParsable(Object value) {
        return value instanceof Boolean || value instanceof String
                || (value instanceof OAS3Input && ((OAS3Input) value).isBoolean());
    }

    @Override
    protected void apply(SchemaValidationContext context, BooleanSchema schema, Boolean value) {

        if (schema.getEnum() != null && !schema.getEnum().isEmpty() && !contains(schema, value)) {
            context.addError(OAS3Utils.newConstraintViolation(context,
                    OAS3Violations.INVALID_INPUT_FOR_ENUM_FIELD));
        }

    }

    protected ValidationError newInputConstraintViolation(SchemaValidationContext context) {
        return OAS3Utils.newConstraintViolation(context, OAS3Violations.INVALID_INPUT_FOR_BOOLEAN_FIELD);
    }

}
