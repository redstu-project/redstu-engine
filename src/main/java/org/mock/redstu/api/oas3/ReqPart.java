package org.mock.redstu.api.oas3;

public enum ReqPart {

    Header("parameters.header"), Path("parameters.path"),
    Query("parameters.query"), Cookie("parameters.cookie"),
    MediaType("requestBody.content-type"), Body("requestBody.content-type.schema");

    public final String name;

    ReqPart(String name) {
        this.name = name;
    }
}
