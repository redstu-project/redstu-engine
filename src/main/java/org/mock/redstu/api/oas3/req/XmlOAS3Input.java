package org.mock.redstu.api.oas3.req;

public interface XmlOAS3Input extends OAS3Input {

    boolean isAttribute();

    String getPrefix();

    String getNamespace();

    boolean isRoot();

    String getName();

    default boolean isVirtual(){
        return false;
    }

}
