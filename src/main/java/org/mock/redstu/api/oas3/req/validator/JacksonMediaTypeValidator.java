package org.mock.redstu.api.oas3.req.validator;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.collections.IteratorUtils;
import org.mock.redstu.api.oas3.ValidationContext;
import org.mock.redstu.api.oas3.req.OAS3Input;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

public abstract class JacksonMediaTypeValidator extends AbstractMediaTypeValidator {

    @Override
    protected Object getObject(ValidationContext context, InputStream io) throws IOException {
        return io == null ? null : newAdapter(getObjectMapper().createParser(io).readValueAsTree());
    }

    protected Object newAdapter(JsonNode node) {
        return node == null ? null : new JacksonOAS3Input(node);
    }

    protected abstract ObjectMapper getObjectMapper();

    private static class JacksonOAS3Input implements OAS3Input {

        private final JsonNode node;
        private List<OAS3Input> elements;
        private Map<String, OAS3Input> fields;

        public JacksonOAS3Input(JsonNode node) {
            this.node = node;
        }

        @Override
        public Integer size() {
            return this.node.size();
        }

        @Override
        public boolean isContainerNode() {
            return this.node.isContainerNode();
        }

        @Override
        public OAS3Input get(String fieldName) {
            return getFields().get(fieldName);
        }

        @Override
        public boolean isNull() {
            return this.node.isNull();
        }

        @Override
        public Iterator<String> fieldNames() {
            return this.node.fieldNames();
        }

        @Override
        public Number numberValue() {
            return this.node.numberValue();
        }

        @Override
        public Boolean booleanValue() {
            return this.node.booleanValue();
        }

        @Override
        public boolean isBoolean() {
            return this.node.isBoolean();
        }

        @Override
        public boolean isArray() {
            return this.node.isArray();
        }

        @Override
        @SuppressWarnings({"unchecked"})
        public synchronized Iterator<OAS3Input> elements() {

            if (this.elements != null) {
                return this.elements.iterator();
            }

            this.elements = (List<OAS3Input>) IteratorUtils.toList(node.elements()).stream()
                    .map(each -> new JacksonOAS3Input((JsonNode) each)).collect(Collectors.toList());

            return elements.iterator();
        }

        private synchronized Map<String, OAS3Input> getFields() {

            if (this.fields != null) {
                return this.fields;
            }

            this.fields = new HashMap<>();

            Iterator<String> iterator = node.fieldNames();

            while (iterator.hasNext()) {
                String name = iterator.next();
                JsonNode innerNode = node.get(name);

                if (innerNode != null) {
                    this.fields.put(name, new JacksonOAS3Input(innerNode));
                }

            }

            return this.fields;
        }

        @Override
        public boolean isTextual() {
            return this.node.isTextual();
        }

        @Override
        public String asText() {
            return this.node.asText();
        }

        @Override
        public boolean isNumber() {
            return this.node.isNumber();
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            JacksonOAS3Input that = (JacksonOAS3Input) o;
            return Objects.equals(node, that.node);
        }

        @Override
        public int hashCode() {
            return Objects.hash(node);
        }

        @Override
        public String toString() {
            return node.toString();
        }
    }

}
