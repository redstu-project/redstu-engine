package org.mock.redstu.api.oas3;

import org.mock.redstu.api.req.HttpReq;
import org.mock.redstu.api.resp.HttpResp;

import java.util.List;

public interface ValidationResult {

    boolean anyErrors();

    List<? extends ValidationError> getErrors();

    void sendErrors(HttpReq req, HttpResp resp);

    boolean isValid(String key, ReqPart part);

    boolean isValid(String key, ReqPart part, String errorCode);

}
