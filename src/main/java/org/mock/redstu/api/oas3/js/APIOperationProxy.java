package org.mock.redstu.api.oas3.js;

import org.graalvm.polyglot.Value;
import org.mock.redstu.api.common.Promise;
import org.mock.redstu.api.oas3.APIOperation;
import org.mock.redstu.api.oas3.ValidationResult;
import org.mock.redstu.api.req.HttpReq;
import org.mock.redstu.api.resp.HttpResp;
import org.mock.redstu.server.req.proxy.HttpRequestProxy;
import org.mock.redstu.server.resp.proxy.HttpRespProxy;

import java.util.Optional;

public class APIOperationProxy extends AbstractExecutableProxy {

    private final APIOperation operation;

    public APIOperationProxy(APIOperation value) {
        this.operation = value;
        executable("validate", this::validate);
        executable("sendExample", this::sendExample);
    }

    private Promise validate(Value... args) {
        return (accept, reject) -> {
            try {

                if (args.length != 1) {
                    throw new IllegalArgumentException("Binding doesn't match required parameters");
                }

                if (!args[0].isProxyObject()) {
                    throw new IllegalArgumentException("Parameter doesn't match expected parameters");
                }


                HttpRequestProxy input = args[0].asProxyObject();

                if (input == null) {
                    throw new IllegalArgumentException("Parameter doesn't match expected parameters");
                }

                HttpReq req = input.getObject();

                req.body().binary((content, err) -> {
                    if (err != null) {
                        reject.executeVoid(err);
                    } else {
                        accept.executeVoid(proxyOf(operation.validate(req, content)));
                    }
                });

            } catch (Exception ex) {
                ex.printStackTrace();
                throw ex;
            }

        };
    }

    private Value sendExample(Value... args) {

        if (args.length != 4) {
            throw new IllegalArgumentException("Binding doesn't match required parameters");
        }

        String name = getString(args[1]);
        Integer statusCode = getInteger(args[0]);

        if (args[2] == null || !args[2].isProxyObject()) {
            throw new IllegalArgumentException("Parameter[2] doesn't match expected parameter ");
        }

        HttpReq req = ((HttpRequestProxy) args[2].asProxyObject()).getObject();

        if (args[3] == null || !args[3].isProxyObject()) {
            throw new IllegalArgumentException("Parameter[3] doesn't match expected parameter ");
        }

        HttpResp resp = ((HttpRespProxy)args[3].asProxyObject()).getObject();

        operation.sendExample(statusCode, name, req, resp);

        return Value.asValue(null);
    }

    private ValidationResultProxy proxyOf(ValidationResult instance) {
        return Optional.ofNullable(instance).map(ValidationResultProxy::new).orElse(null);
    }

}
