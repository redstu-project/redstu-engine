package org.mock.redstu.api.oas3;

import io.swagger.v3.oas.models.media.Schema;

public interface SchemaValidator {

    boolean isSupported(Schema<?> schema);

    void handle(SchemaValidationContext context, Schema<?> schema, Object value);

}
