package org.mock.redstu.api.oas3.req.schema;

import io.swagger.v3.oas.models.media.Schema;
import org.mock.redstu.api.oas3.OAS3Exception;
import org.mock.redstu.api.oas3.OAS3Utils;
import org.mock.redstu.api.oas3.OAS3Violations;
import org.mock.redstu.api.oas3.SchemaValidationContext;
import org.mock.redstu.api.oas3.SchemaValidator;
import org.mock.redstu.api.oas3.ValidationError;

public abstract class AbstractSchemaValidator<T, Y extends Schema<?>> implements SchemaValidator {

    @Override
    public abstract boolean isSupported(Schema<?> schema);

    protected abstract T parse(Object value);

    protected abstract boolean isParsable(Object value);

    protected abstract void apply(SchemaValidationContext context, Y schema, T value);

    protected boolean contains(Schema<?> schema, T object) {
        return schema.getEnum().stream().anyMatch(each -> each.equals(object));
    }

    @Override
    @SuppressWarnings({"unchecked"})
    public void handle(SchemaValidationContext context, Schema<?> rawSchema, Object value) {

        if (!isSupported(rawSchema)) {
            throw new OAS3Exception("Cannot validate schema " + rawSchema + " through type " + getClass());
        }

        if (!isParsable(value)) {
            context.addError(newInputConstraintViolation(context));
            return;
        }

        Y schema = (Y) rawSchema;
        T object = parse(value);

        if (schema.getEnum() != null && !schema.getEnum().isEmpty() && !contains(schema, object)) {
            context.addError(OAS3Utils.newInvalidOptionConstraintViolation(context));
        }

        apply(context, schema, object);
    }

    protected ValidationError newInputConstraintViolation(SchemaValidationContext context) {
        return OAS3Utils.newConstraintViolation(context, OAS3Violations.INVALID_INPUT_FOR_FIELD);
    }

}
