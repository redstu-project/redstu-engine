package org.mock.redstu.api.oas3.js;

import org.graalvm.polyglot.Value;
import org.mock.redstu.js.AbstractProxyObject;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.Optional;

public class AbstractExecutableProxy extends AbstractProxyObject {

    String getString(Object object) {

        if (object == null) {
            return null;
        }

        if (object instanceof String) {
            return (String) object;
        }

        if (object instanceof Value) {
            return ((Value) object).asString();
        }

        throw new IllegalArgumentException("Expected String or Value");
    }

    Integer getInteger(Object object) {

        if (object == null) {
            return null;
        }

        if (object instanceof Integer) {
            return (Integer) object;
        }

        if (object instanceof Value) {
            return ((Value) object).asInt();
        }

        throw new IllegalArgumentException("Expected String or Value");
    }

    Boolean getBoolean(Object object) {

        if (object == null) {
            return null;
        }

        if (object instanceof Boolean) {
            return (Boolean) object;
        }

        if (object instanceof Value) {
            return ((Value) object).asBoolean();
        }

        throw new IllegalArgumentException("Expected String or Value");
    }

    Instant getInstant(Object object) {
        return Optional.ofNullable(getString(object))
                .map(value -> LocalDateTime.parse(value, DateTimeFormatter.BASIC_ISO_DATE)
                        .atZone(ZoneOffset.systemDefault()).toInstant()).orElse(null);
    }

}
