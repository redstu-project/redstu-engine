package org.mock.redstu.api;

import io.swagger.parser.OpenAPIParser;
import io.swagger.v3.parser.core.models.SwaggerParseResult;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.mock.redstu.api.oas3.APIDefinition;
import org.mock.redstu.api.oas3.OAS3APIDefinition;

import org.mock.redstu.api.oas3.OAS3Exception;
import org.mock.redstu.api.oas3.resp.DelegatedResponseEntityFactory;
import org.mock.redstu.api.oas3.resp.JacksonResponseEntityFactory;
import org.mock.redstu.config.SystemConfig;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

import java.nio.file.Paths;
import java.util.Optional;

public final class OAS3 {

    private static OAS3 INSTANCE;

    private final OpenAPIParser parse;
    private final DelegatedResponseEntityFactory responseEntityFactory;
    private static final Logger LOGGER = LogManager.getLogger(OAS3.class);

    private static OAS3 getInstance() {

        if (INSTANCE == null) {
            INSTANCE = new OAS3();
        }

        return INSTANCE;
    }

    private OAS3() {
        this.parse = new OpenAPIParser();
        this.responseEntityFactory = new DelegatedResponseEntityFactory();
        this.responseEntityFactory.addFactory(new JacksonResponseEntityFactory());
    }

    public static APIDefinition spec(String path) {

        if (path == null) {
            return null;
        }

        InputStream io = getInputStream(path);

        if (io == null) {
            return null;
        }

        try (InputStream inputStream = io) {
            String yaml = IOUtils.toString(inputStream, Charset.defaultCharset());
            SwaggerParseResult contents = getInstance().parse.readContents(yaml, null, null);
            return new OAS3APIDefinition(contents.getOpenAPI(), getInstance().responseEntityFactory);
        } catch (IOException ex) {
            LOGGER.error("Unable to acquire OAS3 API definition", ex);
            return null;
        }

    }


    private static InputStream getInputStream(String value) {
        if (value == null) {
            return null;
        } else if (value.startsWith("http://") || value.startsWith("https://")) {
            return downloadFile(value);
        } else {
            return openFile(value);
        }
    }

    private static InputStream downloadFile(String url) {

        try {

            if (url == null) {
                return null;
            }

            HttpRequest request = HttpRequest.newBuilder().uri(new URI(url))
                    .version(HttpClient.Version.HTTP_2).GET().build();

            HttpResponse<byte[]> response = HttpClient.newBuilder().build()
                    .send(request, HttpResponse.BodyHandlers.ofByteArray());

            if (response.statusCode() != 200) {

                LOGGER.error("Unexpected response statusCode=" + response.statusCode());

                if (LOGGER.isDebugEnabled()) {
                    LOGGER.error(toResponseLine(response));
                }

                throw new OAS3Exception("Unexpected response statusCode=" + response.statusCode());
            }

            return new ByteArrayInputStream(response.body());
        } catch (URISyntaxException ex) {
            throw new OAS3Exception("Invalid URL:" + url, ex);
        } catch (IOException | InterruptedException ex) {
            throw new OAS3Exception("Cannot read yaml from URL:" + url, ex);
        }

    }

    private static String toResponseLine(HttpResponse<byte[]> response) {
        StringBuilder sb = new StringBuilder();
        sb.append(response.version().toString()).append(" ").append(response.statusCode()).append("\n");

        response.headers().map().forEach((k, container) -> sb.append(k).append(": ")
                .append(String.join("; ", container)));

        return sb.append("\n").append("\n").append(new String(response.body(), StandardCharsets.UTF_8)).toString();
    }

    private static InputStream openFile(String path) {
        try {

            String directory = SystemConfig.PROJECT_DIRECTORY.value();
            directory = Optional.ofNullable(directory).filter(StringUtils::isNotEmpty).orElse("/var/project");

            File file = Paths.get(directory, path).toFile();

            if (!file.exists() || file.isDirectory()) {
                LOGGER.error("Unable to acquire OAS3 API definition , file [" + path + "] cannot be opened");
                return null;
            }

            return new FileInputStream(file);
        } catch (FileNotFoundException ex) {
            LOGGER.error("Unable to acquire OAS3 API definition , file [" + path + "] cannot be opened");
            return null;
        }
    }

}
