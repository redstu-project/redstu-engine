package org.mock.redstu.git;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.eclipse.jgit.api.CloneCommand;
import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.errors.*;
import org.eclipse.jgit.transport.UsernamePasswordCredentialsProvider;

import java.io.File;

public class GitCloner {

    private String url;
    private GitCredentials credentials;

    private static final Logger LOGGER = LogManager.getLogger(GitCloner.class);

    public GitCloner(String url){
        this(url,null);
    }

    public GitCloner(String url, GitCredentials credentials){
        if(url==null||url.isEmpty())
            throw new IllegalArgumentException("url must not be null nor empty");
        this.url = url;
        this.credentials = credentials;
    }

    public void checkout(String branch, File target){
        if(branch==null||branch.isEmpty())
            throw new IllegalArgumentException("branch must not be null nor empty");
        if(target==null)
            throw new IllegalArgumentException("target must not be null");
        if(!target.exists()) {
            if(!target.mkdirs())
                throw new GitCloneException("failed to create target directory: "+target.getAbsolutePath());
        }
        try {
            LOGGER.info("Cloning Git repository {}",url);
            if(credentials==null)
                LOGGER.warn("No Git credentials provided");
            else LOGGER.info("Using Git basic credentials");
            CloneCommand cloneCommand = Git.cloneRepository()
                    .setURI(url)
                    .setDirectory(target);
            if(credentials!=null)
                cloneCommand.setCredentialsProvider(new UsernamePasswordCredentialsProvider(credentials.getUsername(), credentials.getPassword()));
            Git repository = cloneCommand.call();
            LOGGER.info("Clone completed successfully");
            LOGGER.info("Checking out branch {}",branch);
            repository.checkout().setName(String.format("origin/%s",branch))
                    //.setStartPoint(String.format("origin/%s",branch))
                    .setCreateBranch(false)
                    .call();
            LOGGER.info("Git checkout complete");
        }catch (InvalidRemoteException | TransportException  ex) {
            throw new GitCloneException(String.format("failed to clone remote repository [%s]",url),
                    ex);
        }catch (RefNotFoundException | InvalidRefNameException ex){
            throw new GitCloneException(String.format("Branch [%s] not found in remote [%s]", branch, url),
                    ex);
        }catch (GitAPIException  ex){
            throw new GitCloneException("Found an unexpected git error when performing git clone/checkout",
                    ex);
        }
    }


}
