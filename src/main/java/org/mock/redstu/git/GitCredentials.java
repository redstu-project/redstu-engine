package org.mock.redstu.git;

public class GitCredentials {

    private String username;
    private String password;

    public GitCredentials(String username, String password){
        this.username = username;
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }
}
