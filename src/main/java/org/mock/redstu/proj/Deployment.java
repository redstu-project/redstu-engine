package org.mock.redstu.proj;

import org.apache.logging.log4j.LogManager;
import org.jetbrains.annotations.Nullable;
import org.mock.redstu.api.Logger;
import org.mock.redstu.cockpit.Cockpit;
import org.mock.redstu.config.SystemConfig;
import org.mock.redstu.js.JsRuntime;
import org.mock.redstu.js.MapProxyHelper;
import org.mock.redstu.proj.manifest.CockpitServer;
import org.mock.redstu.server.NetAddress;
import org.mock.redstu.server.Redish;
import org.mock.redstu.server.config.ServerConfig;
import org.mock.redstu.util.JSON;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.*;
import java.util.function.Consumer;

/**
 * Represents the deployment of a {@link Project}
 */
public class Deployment {

    private static Deployment CURRENT;

    private Project project;
    private Redish mockServer;
    private Cockpit cockpit;
    private Map<String,Object> state = new HashMap<>();
    private Set<Signal> signals = new HashSet<>();
    private Map<String, Consumer<Map<String,Object>>> signalListeners = new HashMap<>();
    private Logger logger;
    private JsRuntime runtime;

    private boolean activated = false;
    private NetAddress mockAddr;
    private NetAddress cockpitAddr;

    private static final org.apache.logging.log4j.Logger LOGGER = LogManager.getLogger(Deployment.class);

    public Deployment(Project project, NetAddress mockAddr, @Nullable NetAddress cockpitAddr){
        if(project==null)
            throw new IllegalArgumentException("project must not be null");
        if(mockAddr==null)
            throw new IllegalArgumentException("mockAddr must not be null");
        this.project = project;
        this.mockAddr = mockAddr;
        this.cockpitAddr = cockpitAddr;
        this.logger = new Logger();
    }

    public Map<String,Object> getState(){
        return state;
    }

    public void setState(Map<String,Object> state){
        if(state==null||state.isEmpty())
            throw new IllegalArgumentException("state must not be null nor empty");
        LOGGER.info("Updating deployment state");
        this.state.clear();
        StringBuilder script = new StringBuilder();
        script.append(String.format("import {state as target} from './%s';",ProjectConstants.REDSTU_EXPORTS_FILENAME));
        script.append("\n");
        script.append(String.format("Object.assign(target,%s);", JSON.string(state)));
        runtime.eval(script.toString());
    }

    public Logger getLogger() {
        return logger;
    }

    public Redish getMockServer(){
        return mockServer;
    }

    private ServerConfig mockServerConfig(){
        ServerConfig config = new ServerConfig(mockAddr);
        config.setCorsConfig(project.getManifest().getServers().getMock().getCors());
        return config;
    }

    private ServerConfig cockpitServerConfig(){
        if(cockpitAddr==null)
            throw new IllegalStateException("cockpit address is null");
        ServerConfig config = new ServerConfig(cockpitAddr);
        config.setCorsConfig(project.getManifest().getServers().getCockpit().getCors());
        return config;
    }

    public void activate(){
        assertNotActivated();
        LOGGER.info("Deploying project...");
        mockServer = new Redish("mock", mockServerConfig());
        mockServer.setCurrent();
        LOGGER.info("Initializing JS Runtime");
        this.runtime = createRuntime();
        this.setInitialState();
        this.setCurrent();
        LOGGER.info("Executing entry file: {}",ProjectConstants.ENTRY_FILENAME);
        ProjectManifest manifest = project.getManifest();
        this.runtime.evalFile(manifest.getPaths().getEntryFile());
        this.activated = true;
        mockServer.start();
        CockpitServer cockpitServer = manifest.getServers().getCockpit();
        if(cockpitServer.isEnabled()){
            LOGGER.info("Setting up Cockpit");
            cockpit = new Cockpit(cockpitServerConfig(), this, SystemConfig.COCKPIT_API_KEY.value());
            LOGGER.info("Activating Cockpit");
            cockpit.activate();
        }else LOGGER.warn("Cockpit is disabled");
        LOGGER.info("Project Deployed successfully");
    }

    private void setInitialState(){
        String initialStateFile = project.getManifest().getPaths().getInitialStateFile();
        if(initialStateFile==null||initialStateFile.isEmpty())
            initialStateFile = SystemConfig.PROJECT_INITIAL_STATE_FILE.value();
        if(initialStateFile!=null&&!initialStateFile.isEmpty()){
            Map<String,Object> state = null;
            if(initialStateFile.toLowerCase().startsWith("http")){
                LOGGER.info("About to load initial state from URL: {}",initialStateFile);
                try {
                    state = StateFile.loadFromUrl(new URL(initialStateFile));
                } catch (MalformedURLException e) {
                    throw new BootstrapException("invalid state source URL: "+initialStateFile);
                }
            }else{
                LOGGER.info("About to load initial state from local file: {}",initialStateFile);
                File file = new File(project.getSrcDirectory().getAbsolutePath()+File.separator+initialStateFile);
                if(!file.exists()){
                    throw new BootstrapException("state file not found: "+initialStateFile);
                }
                state = StateFile.loadFromFile(file);
            }
            this.setState(state);
            LOGGER.info("Initial state Loaded successfully");
        }else LOGGER.info("No initial state file set");
    }

    private JsRuntime createRuntime(){
        JsRuntime runtime = new JsRuntime(state,logger, project.
                getSrcDirectory());
        runtime.start();
        return runtime;
    }
    private void assertNotActivated(){
        if(activated)
            throw new IllegalStateException("deployment already activated");
    }

    public JsRuntime getRuntime() {
        return runtime;
    }

    public Project getProject(){
        return this.project;
    }

    public void addSignalListener(Signal signal, Consumer<Map<String,Object>> listener){
        if(signal==null)
            throw new IllegalArgumentException("signal must not be null");
        if(listener==null)
            throw new IllegalArgumentException("listener must not be null");
        LOGGER.info("Adding Signal: {}",signal.getName());
        this.signals.add(signal);
        this.signalListeners.put(signal.getName(),listener);
    }

    public boolean emitSignal(String name, @Nullable Map<String,Object> parameter){
        if(name==null||name.isEmpty())
            throw new IllegalArgumentException("signal name must not be null nor empty");
        LOGGER.info("Raising signal: {}",name);
        Consumer<Map<String,Object>> listener = signalListeners.get(name);
        if(listener!=null) {
            listener.accept(parameter);
            return true;
        }else LOGGER.warn("No listener found for signal");
        return false;
    }

    public Collection<Signal> getSignals(){
        return Collections.unmodifiableCollection(signals);
    }

    public Optional<Cockpit> getCockpit(){
        return Optional.ofNullable(
                cockpit);
    }

    public void setCurrent(){
        CURRENT = this;
    }

    public static Deployment current(){
        return CURRENT;
    }

}
