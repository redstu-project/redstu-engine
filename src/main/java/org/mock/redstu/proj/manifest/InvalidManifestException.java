package org.mock.redstu.proj.manifest;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

public class InvalidManifestException extends ManifestLoadException {

    private Collection<String> problems = new ArrayList<>();

    public InvalidManifestException(Collection<String> problems){
        super(String.format("Project manifest is not valid. %d problems were found",problems.size()));
        this.problems.addAll(problems);
    }

    public Collection<String> getProblems() {
        return Collections.unmodifiableCollection(problems);
    }
}
