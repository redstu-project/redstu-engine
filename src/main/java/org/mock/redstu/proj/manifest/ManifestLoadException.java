package org.mock.redstu.proj.manifest;

import org.mock.redstu.proj.ProjectException;

public class ManifestLoadException extends ProjectException {

    public ManifestLoadException(String message){
        super(message);
    }

    public ManifestLoadException(String message, Throwable cause){
        super(message,cause);
    }

}
