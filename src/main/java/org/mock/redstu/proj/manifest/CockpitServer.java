package org.mock.redstu.proj.manifest;

public class CockpitServer extends ServerConfig {

    private boolean enabled = false;

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }
}
