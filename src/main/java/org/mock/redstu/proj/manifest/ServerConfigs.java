package org.mock.redstu.proj.manifest;

public class ServerConfigs {

    private CockpitServer cockpit = new CockpitServer();
    private MockServer mock = new MockServer();

    public CockpitServer getCockpit() {
        return cockpit;
    }

    public void setCockpit(CockpitServer cockpit) {
        this.cockpit = cockpit;
    }

    public MockServer getMock() {
        return mock;
    }

    public void setMock(MockServer mock) {
        this.mock = mock;
    }
}
