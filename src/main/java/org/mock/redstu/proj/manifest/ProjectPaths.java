package org.mock.redstu.proj.manifest;

import org.mock.redstu.proj.ProjectConstants;

public class ProjectPaths {

    private String entryFile = ProjectConstants.ENTRY_FILENAME;
    private String initialStateFile = null;

    public String getEntryFile() {
        return entryFile;
    }

    public void setEntryFile(String entryFile) {
        this.entryFile = entryFile;
    }

    public String getInitialStateFile() {
        return initialStateFile;
    }

    public void setInitialStateFile(String initialStateFile) {
        this.initialStateFile = initialStateFile;
    }

}
