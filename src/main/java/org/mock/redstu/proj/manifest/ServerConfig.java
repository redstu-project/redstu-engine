package org.mock.redstu.proj.manifest;

import org.mock.redstu.commons.CorsConfig;

public class ServerConfig {

    private CorsConfig cors;

    public CorsConfig getCors() {
        return cors;
    }

    public void setCors(CorsConfig cors) {
        this.cors = cors;
    }
}
