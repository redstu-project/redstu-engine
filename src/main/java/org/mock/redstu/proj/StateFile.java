package org.mock.redstu.proj;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.Map;

public final class StateFile {

    private StateFile(){

    }

    public static Map<String,Object> loadFromUrl(URL url){
        ObjectMapper mapper = new ObjectMapper();
        try {
            return mapper.readValue(url, Map.class);
        }catch (JsonParseException ex){
            throw new StateFileException("failed parsing downloaded state file. Make sure the content is valid JSON",
                    ex);
        }catch (IOException ex){
            throw new StateFileException("failed to download state content from URL source",
                    ex);
        }
    }

    public static Map<String,Object> loadFromFile(File file){
        ObjectMapper mapper = new ObjectMapper();
        try {
            return mapper.readValue(file, Map.class);
        }catch (JsonParseException ex){
            throw new StateFileException("failed parsing local state file. Make sure the content is valid JSON",
                    ex);
        }catch (IOException ex){
            throw new StateFileException("failed loading state from local file",
                    ex);
        }
    }

}
