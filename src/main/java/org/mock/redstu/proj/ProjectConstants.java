package org.mock.redstu.proj;

public final class ProjectConstants {

    public static final String ENTRY_FILENAME = "src/main.js";
    public static final String REDSTU_EXPORTS_FILENAME = "redstu.js";
    public static final String DEFAULT_MANIFEST_FILENAME = "redstu.yml";

}
