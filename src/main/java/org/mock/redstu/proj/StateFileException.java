package org.mock.redstu.proj;

public class StateFileException extends RuntimeException {

    public StateFileException(String message){
        super(message);
    }

    public StateFileException(String message, Throwable cause){
        super(message,cause);
    }

}
