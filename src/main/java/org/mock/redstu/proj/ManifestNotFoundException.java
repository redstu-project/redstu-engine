package org.mock.redstu.proj;

public class ManifestNotFoundException extends ProjectException {

    public ManifestNotFoundException(String path) {
        super(String.format("Project manifest file not found: %s",path));
    }
}
