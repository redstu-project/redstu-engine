package org.mock.redstu.proj;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jetbrains.annotations.Nullable;
import org.mock.redstu.server.NetAddress;

import java.io.File;

/**
 * Represents a dynamic mock project
 */
public class Project {

    private File srcDirectory;
    private ProjectManifest manifest;

    private static final Logger LOGGER = LogManager.getLogger(Project.class);

    public Project(File srcDirectory){
        this(srcDirectory,ProjectConstants.DEFAULT_MANIFEST_FILENAME);
    }

    private Project(File srcDirectory,  String manifestFilePath){
        if(srcDirectory==null)
            throw new IllegalArgumentException("srcDirectory must not be null");
        if(!srcDirectory.exists()||!srcDirectory.isDirectory())
            throw new IllegalArgumentException("project src directory not found: "+srcDirectory.getAbsolutePath());
        if(manifestFilePath==null||manifestFilePath.isEmpty())
            throw new IllegalArgumentException("manifestFilePath must not be null nor empty");
        this.srcDirectory = srcDirectory;
        File manifestFile = new File(srcDirectory.getAbsolutePath(),manifestFilePath);
        if(!manifestFile.exists()||manifestFile.isDirectory()){
            throw new ManifestNotFoundException(manifestFile.getAbsolutePath());
        }else {
            LOGGER.info("Loading manifest file");
            manifest = ProjectManifest.loadFrom(manifestFile);
            LOGGER.info("Manifest loaded successfully");
        }
        LOGGER.info("Entry file: "+manifest.getPaths().getEntryFile());
        File entryFile = new File(srcDirectory.getAbsolutePath(), manifest.getPaths().getEntryFile());
        if(!entryFile.exists()||entryFile.isDirectory())
            throw new ProjectException("entry file not found: "+entryFile.getAbsolutePath());
    }

    public File getSrcDirectory() {
        return srcDirectory;
    }

    public ProjectManifest getManifest() {
        return manifest;
    }


    public Deployment createDeployment(NetAddress mockAddr, @Nullable NetAddress cockpitAddr){
        return new Deployment(this, mockAddr,
                cockpitAddr);
    }

}
