package org.mock.redstu.proj;

public class Signal {

    private String name;
    private String description;
    private boolean parameterized;

    public Signal(String name, String description, boolean parameterized){
        this.name = name;
        this.description = description;
        this.parameterized = parameterized;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public boolean isParameterized() {
        return parameterized;
    }


}
