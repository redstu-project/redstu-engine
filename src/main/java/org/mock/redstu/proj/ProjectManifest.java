package org.mock.redstu.proj;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.exc.UnrecognizedPropertyException;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import jakarta.json.JsonReader;
import org.apache.commons.io.FileUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.leadpony.justify.api.JsonSchema;
import org.leadpony.justify.api.JsonValidationService;
import org.leadpony.justify.api.Problem;
import org.leadpony.justify.api.ProblemHandler;
import org.mock.redstu.proj.manifest.*;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class ProjectManifest {

    private String name;
    private String engine;
    private Author author = new Author();
    private ProjectPaths paths = new ProjectPaths();
    private ServerConfigs servers = new ServerConfigs();

    private static final Logger LOGGER = LogManager.getLogger(ProjectManifest.class);

    public String getName() {
        return name;
    }

    public String getEngine() {
        return engine;
    }

    public Author getAuthor() {
        return author;
    }

    public ServerConfigs getServers() {
        return servers;
    }

    public ProjectPaths getPaths() {
        return paths;
    }

    public static ProjectManifest loadFrom(File yamlFile){
        if(!yamlFile.exists()||yamlFile.isDirectory())
            throw new IllegalArgumentException("project manifest file not found: "+yamlFile.getAbsolutePath());
        byte[] yamlPayload = null;
        List<String> problemsList = new ArrayList<>();
        try (InputStream schemaStream = ProjectManifest.class.getClassLoader().getResourceAsStream("schemas/manifest.json")) {
            JsonValidationService service = JsonValidationService.newInstance();
            JsonSchema schema = service.readSchema(schemaStream);
            ProblemHandler problemHandler = problems ->{
                problems.stream().map(Problem::toString)
                        .forEach(problemsList::add);
            };
            yamlPayload = FileUtils.readFileToByteArray(yamlFile);
            try (JsonReader reader = service.createReader(new ByteArrayInputStream(yamlPayload), schema, problemHandler)) {
                reader.readValue();
            }
            if(!problemsList.isEmpty())
                throw new InvalidManifestException(problemsList);
        }catch (FileNotFoundException ex){
            throw new ManifestNotFoundException(yamlFile.getAbsolutePath());
        }catch (IOException ex){
            throw new ManifestLoadException("unexpected error loading manifest file",
                    ex);
        }
        ObjectMapper mapper = new ObjectMapper(new YAMLFactory());
        try {
            return mapper.readValue(yamlPayload, ProjectManifest.class);
        }catch (UnrecognizedPropertyException ex){
            throw new ManifestNotFoundException("Unknown field found in manifest file: "+ex.getPathReference());
        }catch (IOException ex){
            throw new ManifestLoadException("unexpected error found parsing the manifest file",
                    ex);
        }
    }

}
