package org.mock.redstu.proj;

public class BootstrapException extends ProjectException {

    public BootstrapException(String message){
        super(message);
    }

    public BootstrapException(String message, Throwable cause){
        super(message,cause);
    }

}
