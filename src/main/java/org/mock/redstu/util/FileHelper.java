package org.mock.redstu.util;

import java.io.File;
import java.util.function.Consumer;

public class FileHelper {

    public static void forEachFileIn(File file, Consumer<File> consumer){
        File[] files = file.listFiles();
        if(files!=null){
            for(File f: files){
                if(".".equalsIgnoreCase(f.getName())||"..".equalsIgnoreCase(f.getName()))
                    continue;
                if(f.isDirectory())
                    forEachFileIn(f,consumer);
                else consumer.accept(f);
            }
        }
    }


    public static String relativePath(File subject, File relative){
        String[] subjectParts = subject.getAbsolutePath().split("/");
        String[] relativeParts = relative.getAbsolutePath().split("/");

        int subjectRelativeDiff = relativeParts.length - subjectParts.length ;
        StringBuilder builder = new StringBuilder();

        if(subjectRelativeDiff==0){
            return relative.getName();
        }else if(subjectRelativeDiff<0){
            for(;subjectRelativeDiff<0;subjectRelativeDiff++)
                builder.append("../");
        }else{
            File parent = relative.getParentFile();
            for(;subjectRelativeDiff>0;subjectRelativeDiff--){
                builder.append(parent.getName());
                builder.append(File.separator);
                parent = parent.getParentFile();
            }
        }

        builder.append(relative.getName());
        return builder.toString();
    }

}
