package org.mock.redstu.js.proxy;


import org.graalvm.polyglot.Value;
import org.mock.redstu.api.Respond;
import org.mock.redstu.js.*;
import org.mock.redstu.proj.Signal;
import org.mock.redstu.server.exchange.sse.proxy.EventSessionProxy;
import org.mock.redstu.server.req.proxy.HttpRequestProxy;
import org.mock.redstu.server.resp.proxy.HttpRespProxy;

public class RespondProxy extends AbstractProxyObject {

    public RespondProxy(){
        super();
        executable("GET",this::getRequest);
        executable("POST",this::postRequest);
        executable("PUT",this::putRequest);
        executable("PATCH",this::patchRequest);
        executable("DELETE",this::deleteRequest);
        executable("HEAD",this::headRequest);
        executable("SSE",this::sseConnection);
        executable("signal",this::signal);
    }

    private void validateArgs(Value... values){
        if(values==null||values.length<2)
            throw new IllegalArgumentException("two arguments are required");
        Value path = values[0];
        if(!path.isString())
            throw new IllegalArgumentException("path must be a string");
        Value callback = values[1];
        if(!callback.canExecute())
            throw new IllegalArgumentException("callback must be a function");
    }

    private Value postRequest(Value... values){
        validateArgs(values);
        Value path = values[0];
        Value callback = values[1];
        Respond.postRequest(path.asString(), ((req, resp) -> callback.executeVoid(new HttpRequestProxy(req),
                new HttpRespProxy(resp))));
        return Value.asValue(null);
    }

    private Value getRequest(Value... values){
        validateArgs(values);
        Value path = values[0];
        Value callback = values[1];
        Respond.getRequest(path.asString(),((req, resp) -> callback.executeVoid(new HttpRequestProxy(req),
                new HttpRespProxy(resp))));
        return Value.asValue(null);
    }

    private Value deleteRequest(Value... values){
        validateArgs(values);
        Value path = values[0];
        Value callback = values[1];
        Respond.deleteRequest(path.asString(),((req, resp) -> callback.executeVoid(new HttpRequestProxy(req),
                new HttpRespProxy(resp))));
        return Value.asValue(null);
    }

    private Value patchRequest(Value... values){
        validateArgs(values);
        Value path = values[0];
        Value callback = values[1];
        Respond.patchRequest(path.asString(),((req, resp) -> callback.executeVoid(new HttpRequestProxy(req),
                new HttpRespProxy(resp))));
        return Value.asValue(null);
    }

    private Value putRequest(Value... values){
        validateArgs(values);
        Value path = values[0];
        Value callback = values[1];
        Respond.putRequest(path.asString(),((req, resp) -> callback.executeVoid(new HttpRequestProxy(req),
                new HttpRespProxy(resp))));
        return Value.asValue(null);
    }

    private Value headRequest(Value... values){
        validateArgs(values);
        Value path = values[0];
        Value callback = values[1];
        Respond.headRequest(path.asString(),((req, resp) -> callback.executeVoid(new HttpRequestProxy(req),
                new HttpRespProxy(resp))));
        return Value.asValue(null);
    }

    private Value sseConnection(Value... values){
        validateArgs(values);
        Value path = values[0];
        Value callback = values[1];
        Respond.sseConnection(path.asString(),session -> callback.executeVoid(new EventSessionProxy(session)));
        return Value.asValue(null);
    }

    private Value signal(Value... values){
        if(values.length<2)
            throw new IllegalArgumentException("two arguments must be provided");
        Value signalVal = values[0];
        if(!signalVal.hasMembers()||!signalVal.hasMember(SignalAttributes.NAME)||!signalVal.hasMember(SignalAttributes.DESCRIPTION)||!signalVal.hasMember(SignalAttributes.PARAMETERIZED))
            throw new IllegalArgumentException("invalid signal object");
        Signal signal = SignalConverter.fromValue(signalVal);
        Value callback = values[1];
        if(!callback.canExecute())
            throw new IllegalArgumentException("callback must be a function");
        Respond.signal(signal,map -> callback.executeVoid(
                MapProxyHelper.proxyRecursive(
                        map)));
        return Value.asValue(null);
    }

}
