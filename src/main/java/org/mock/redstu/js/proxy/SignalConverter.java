package org.mock.redstu.js.proxy;

import org.graalvm.polyglot.Value;
import org.mock.redstu.proj.Signal;

public final class SignalConverter {

    static Signal fromValue(Value value){
        Value nameVal = value.getMember(SignalAttributes.NAME);
        if(!nameVal.isString())
            throw new IllegalArgumentException("signal name must be a string");
        Value descriptionVal = value.getMember(SignalAttributes.DESCRIPTION);
        if(!descriptionVal.isString())
            throw new IllegalArgumentException("signal description must be a string");
        Value parameterizedVal = value.getMember(SignalAttributes.PARAMETERIZED);
        if(!parameterizedVal.isBoolean())
            throw new IllegalArgumentException("signal parameterized attribute must be boolean");
        return new Signal(nameVal.asString(),descriptionVal.asString(),parameterizedVal.
                asBoolean());
    }

}
