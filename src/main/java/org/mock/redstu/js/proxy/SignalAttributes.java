package org.mock.redstu.js.proxy;

public final class SignalAttributes {

    public static final String NAME = "name";
    public static final String DESCRIPTION = "description";
    public static final String PARAMETERIZED = "parameterized";

}
