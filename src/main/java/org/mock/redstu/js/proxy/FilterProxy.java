package org.mock.redstu.js.proxy;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.graalvm.polyglot.Value;
import org.mock.redstu.api.Filter;
import org.mock.redstu.js.AbstractProxyObject;
import org.mock.redstu.server.commons.Verb;
import org.mock.redstu.server.req.proxy.HttpRequestProxy;
import org.mock.redstu.server.resp.proxy.HttpRespProxy;

import java.util.ArrayList;
import java.util.List;

public class FilterProxy extends AbstractProxyObject {

    private static final Logger LOGGER = LogManager.getLogger(FilterProxy.class);

    public FilterProxy(){
        super();
        executable("all",this::all);
        executable("only",this::only);
    }

    private Value all(Value... values){
        if(values.length<1)
            throw new IllegalArgumentException("filter function must be provided");
        Value value = values[0];
        if(!value.canExecute())
            throw new IllegalArgumentException("a filter function is expected. Argument provided is not a function");
        Filter.all((req, resp, chain) -> value.executeVoid(
                new HttpRequestProxy(req),
                new HttpRespProxy(resp),
                chain));
        return Value.asValue(null);
    }

    private Value only(Value... values){
        if(values.length<3)
            throw new IllegalArgumentException("3 arguments are expected");
        Value arg1 = values[0];
        if(!arg1.hasArrayElements()&&!arg1.isNull())
            throw new IllegalArgumentException("1st argument must be a string array or undefined");
        Value arg2 = values[1];
        if(!arg2.isString()&&!arg2.isNull())
            throw new IllegalArgumentException("2nd argument must be a string or undefined");
        Value arg3 = values[2];
        if(!arg3.canExecute())
            throw new IllegalArgumentException("3rd argument must be a function");
        List<Verb> verbList = new ArrayList<>();
        if(!arg1.isNull()) {
            for (int i = 0; i < arg1.getArraySize(); i++) {
                String verbStr = arg1.getArrayElement(i).asString().toUpperCase();
                try {
                    Verb verb = Verb.valueOf(verbStr);
                    verbList.add(verb);
                } catch (IllegalArgumentException ex) {
                    LOGGER.debug("Invalid Verb passed: {}",verbStr);
                }
            }
        }
        Filter.only(verbList.isEmpty() ? null : verbList, arg2.isNull() ? null : arg2.asString(), (req, resp, chain) -> arg3.executeVoid(
                new HttpRequestProxy(req),
                new HttpRespProxy(resp),chain));
        return Value.asValue(null);
    }

}
