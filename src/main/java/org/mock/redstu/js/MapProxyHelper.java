package org.mock.redstu.js;

import org.graalvm.polyglot.proxy.ProxyArray;
import org.graalvm.polyglot.proxy.ProxyObject;

import java.util.Map;

public class MapProxyHelper {

    private MapProxyHelper(){

    }

    public static void proxyValuesRecursive(Map<String,Object> map){
        for(Map.Entry<String,Object> entry: map.entrySet()){
            if(entry.getValue() instanceof Map){
                entry.setValue(proxyRecursive((Map<String,Object>) entry.
                        getValue()));
            }
        }
    }

    public static ProxyObject proxyRecursive(Map<String,Object> map){
        for(Map.Entry<String,Object> entry: map.entrySet()){
            if(entry.getValue() instanceof Map){
                entry.setValue(proxyRecursive((Map<String,Object>) entry.
                        getValue()));
            }else if(entry.getValue().getClass().isArray()){
                entry.setValue(ProxyArray.fromArray((Object[]) entry.getValue()));
            }
        }
        return ProxyObject.fromMap(map);
    }

}
