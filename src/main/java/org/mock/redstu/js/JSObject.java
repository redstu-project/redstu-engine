package org.mock.redstu.js;

import org.graalvm.polyglot.Value;
import org.graalvm.polyglot.proxy.Proxy;
import org.graalvm.polyglot.proxy.ProxyArray;
import org.graalvm.polyglot.proxy.ProxyObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class JSObject {

    public static Object asHost(Object object){
        if(object instanceof Value){
            return asHost((Value) object);
        }else if(object instanceof Proxy){
            Proxy proxy = (Proxy) object;
            if(proxy instanceof ProxyArray){
                return toList((ProxyArray) proxy);
            }
            return toMap((ProxyObject) proxy);
        }else return object;
    }


    //The order of the If states matters. Don't change it unless you know what you doing
    public static Object asHost(Value value){
        if(value.isHostObject()){
            return value.asHostObject();
        }else if(value.hasArrayElements()) {
            return toList(value);
        }else if(value.hasMembers()) {
            return toMap(value);
        }else if(value.isProxyObject()) {
            return asHost((Proxy) value.asProxyObject());
        }else if(value.isNumber()){
            if(value.fitsInByte())
                return value.asByte();
            else if(value.fitsInShort())
                return value.asShort();
            else if(value.fitsInInt())
                return value.asInt();
            else if(value.fitsInLong())
                return value.asLong();
            else if(value.fitsInFloat())
                return value.asFloat();
            else if(value.fitsInDouble())
                return value.asDouble();
            else throw new IllegalStateException("unsupported number value found in state object");
        }else if(value.isString()){
            return value.asString();
        }else if(value.isBoolean()){
            return value.isBoolean();
        }else if(value.isDate()||value.isTime()) {
            return value.asDate();
        }else if(value.isInstant()) {
            return value.asInstant();
        }else {
            throw new IllegalStateException("unsupported value found in state object");
        }
    }


    public static Map<String,Object> toMap(Value mapValue){
        Map<String,Object> map = new HashMap<>();
        for(String key : mapValue.getMemberKeys()){
            Value value = mapValue.getMember(key);
            map.put(key,asHost(value));
        }
        return map;
    }

    public static Map<String,Object> toMap(ProxyObject proxyObject){
        ProxyArray keysArray = (ProxyArray) proxyObject.getMemberKeys();
        Map<String,Object> map = new HashMap<>();
        for(Object key : toList(keysArray)){
            String keyStr = key.toString();
            Object value = proxyObject.getMember(keyStr);
            map.put(keyStr,asHost(value));
        }
        return map;
    }

    public static List<Object> toList(ProxyArray proxyArray){
        List<Object> list = new ArrayList<>();
        for(int i=0;i<proxyArray.getSize();i++){
            Object object = proxyArray.get(i);
            list.add(asHost(object));
        }
        return list;
    }


    public static List<Object> toList(Value arrayValue){
        List<Object> objectList = new ArrayList<>();
        for(int i=0; i<arrayValue.getArraySize(); i ++){
            Value value = arrayValue.getArrayElement(i);
            objectList.add(asHost(value));
        }
        return objectList;
    }

}
