package org.mock.redstu.js.spi;


public interface JsFacadeObject {

    void setBackingObject(Object object);

}
