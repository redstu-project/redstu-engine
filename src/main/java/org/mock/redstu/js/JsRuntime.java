package org.mock.redstu.js;

import org.apache.commons.io.FileUtils;
import org.apache.logging.log4j.LogManager;
import org.graalvm.polyglot.Context;
import org.graalvm.polyglot.Source;
import org.graalvm.polyglot.Value;
import org.graalvm.polyglot.proxy.ProxyObject;
import org.mock.redstu.api.Logger;
import org.mock.redstu.api.MediaTypes;
import org.mock.redstu.api.oas3.js.OAS3Proxy;
import org.mock.redstu.api.sse.EventBuilder;
import org.mock.redstu.api.sse.EventStream;
import org.mock.redstu.api.templating.Templates;
import org.mock.redstu.api.util.Freeze;
import org.mock.redstu.api.util.Postpone;
import org.mock.redstu.api.util.Repeat;
import org.mock.redstu.js.proxy.FilterProxy;
import org.mock.redstu.js.proxy.RespondProxy;
import org.mock.redstu.proj.BootstrapException;
import org.mock.redstu.proj.ProjectConstants;
import org.mock.redstu.util.FileHelper;

import java.io.File;
import java.io.IOException;
import java.nio.ByteOrder;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

public class JsRuntime {

    private Context context;
    private File exportsFile;
    private final Logger logger;
    private final File currentDirectory;
    private final Map<String, Object> state;


    private static final org.apache.logging.log4j.Logger LOGGER = LogManager.getLogger(JsRuntime.class);

    public JsRuntime(Map<String, Object> state, Logger logger, File currentDirectory) {
        this.state = state;
        this.logger = logger;
        this.currentDirectory = currentDirectory;
    }

    public static JsRuntime fromTempDirectory() {
        return fromTempDirectory(Collections.emptyMap(), new Logger());
    }

    public static JsRuntime fromTempDirectory(Map<String, Object> state, Logger logger) {
        try {
            return new JsRuntime(state, logger, Files.createTempDirectory("red").toFile());
        } catch (IOException ex) {
            throw new JsRuntimeException("error creating temporary directory", ex);
        }
    }

    public void start() {
        requireNotStarted();
        exportsFile = new File(currentDirectory.getAbsolutePath() + File.separator + ProjectConstants.REDSTU_EXPORTS_FILENAME);
        this.writeExportsFile();
        this.refactorImports();
        this.createJSContext();
    }

    static String generateExports() {
        List<Class<?>> exportsList = Arrays.asList(String.class, TimeUnit.class, ByteOrder.class, MediaTypes.class,
                EventStream.class,
                EventBuilder.class, Templates.class, Freeze.class, Postpone.class, Repeat.class);
        String[] exportNames = new String[exportsList.size() + 5];
        exportNames[0] = "state";
        exportNames[1] = "log";
        exportNames[2] = "Respond";
        exportNames[3] = "Filter";
        exportNames[4] = "OAS3";

        StringBuilder builder = new StringBuilder();
        builder.append(String.format("\nlet state=%s;", RuntimeConstants.STATE_MEMBER_ID));
        builder.append(String.format("\nlet log=%s;", RuntimeConstants.LOGGER_MEMBER_ID));
        builder.append(String.format("\nlet Respond=%s;", RuntimeConstants.RESPOND_GLOBAL_ID));
        builder.append(String.format("\nlet Filter=%s;", RuntimeConstants.FILTER_GLOBAL_ID));
        builder.append(String.format("\nlet OAS3=%s;", RuntimeConstants.OAS3_GLOBAL_ID));

        int i = 5;

        for (Class<?> clazz : exportsList) {
            exportNames[i] = clazz.getSimpleName();
            builder.append(String.format("\nlet %s = Java.type('%s');", clazz.getSimpleName(), clazz.
                    getCanonicalName()));
            i++;
        }

        builder.append(String.format("\nexport {%s};", String.join(", ", exportNames)));
        return builder.toString();
    }

    private void refactorImports() {
        LOGGER.info("Refactoring redstu-js imports");
        FileHelper.forEachFileIn(currentDirectory, (file -> {
            if (file.getName().endsWith(".js")) {
                try {
                    String relativePath = FileHelper.relativePath(file, exportsFile);
                    String fileContents = FileUtils.readFileToString(file, StandardCharsets.UTF_8);
                    String replacedContent = fileContents.replaceAll("@redstu\\/redstu-js", relativePath);
                    FileUtils.writeStringToFile(file, replacedContent, StandardCharsets.UTF_8);
                } catch (IOException ex) {
                    throw new BootstrapException("error refactoring redstu-js imports on file: " + file.getAbsolutePath(),
                            ex);
                }
            }
        }));
    }

    private void writeExportsFile() {
        try {
            FileUtils.writeStringToFile(exportsFile, generateExports(), StandardCharsets.UTF_8);
        } catch (IOException ex) {
            throw new JsRuntimeException("error writing to exports file: " + exportsFile.getAbsolutePath(),
                    ex);
        }
    }

    private void createJSContext() {
        Context.Builder contextBuilder = Context.newBuilder(RuntimeConstants.JS_LANGUAGE_ID).allowAllAccess(true)
                .allowExperimentalOptions(true)
                .option("js.foreign-object-prototype", "true")
                .currentWorkingDirectory(Paths.get(currentDirectory
                        .getAbsolutePath()));
        context = contextBuilder.build();
        Value value = context.getBindings(RuntimeConstants.JS_LANGUAGE_ID);
        value.putMember(RuntimeConstants.STATE_MEMBER_ID, ProxyObject.fromMap(state));
        value.putMember(RuntimeConstants.LOGGER_MEMBER_ID, logger);
        value.putMember(RuntimeConstants.RESPOND_GLOBAL_ID, new RespondProxy());
        value.putMember(RuntimeConstants.FILTER_GLOBAL_ID, new FilterProxy());
        value.putMember(RuntimeConstants.OAS3_GLOBAL_ID, new OAS3Proxy());

    }

    public boolean isStarted() {
        return context != null;
    }

    public void shutdown() {
        requireStarted();
        this.context.close(true);
        this.context = null;
    }

    public Map<String, Object> getState() {
        return state;
    }

    public Logger getLogger() {
        return logger;
    }

    public Value evalFile(String relativePath) {
        requireStarted();
        File sourceFile = new File(currentDirectory.getAbsolutePath() + File.separator + relativePath);
        Source source = null;
        try {
            source = Source.newBuilder(RuntimeConstants.JS_LANGUAGE_ID, sourceFile)
                    .mimeType("application/javascript+module")
                    .build();
        } catch (IOException ex) {
            throw new JsRuntimeException("error loading JS file: " + sourceFile.getAbsolutePath(),
                    ex);
        }
        try {
            return context.eval(source);
        } catch (RuntimeException ex) {
            throw new JsEvaluationException("error evaluating source file: " + relativePath,
                    ex);
        }
    }

    public void mountFile(File file) {
        try {
            FileUtils.copyFileToDirectory(file, currentDirectory);
        } catch (IOException ex) {
            throw new JsRuntimeException("error copying file into working directory: " + file.getAbsolutePath(),
                    ex);
        }
    }

    public void mountFile(File file, String mountPath) {
        File target = new File(currentDirectory.getAbsolutePath() + File.separator + mountPath);
        File targetDirectory = target.getParentFile();
        try {
            if (!targetDirectory.exists()) {
                targetDirectory.mkdirs();
            }
            FileUtils.copyFile(file, target);
        } catch (IOException ex) {
            throw new JsRuntimeException("error copying file to: " + targetDirectory.getAbsolutePath(),
                    ex);
        }
    }

    public void mountFile(String content, String mountPath) {
        File target = new File(currentDirectory.getAbsolutePath() + File.separator + mountPath);
        File targetDirectory = target.getParentFile();
        try {
            if (!targetDirectory.exists()) {
                targetDirectory.mkdirs();
            }
            FileUtils.writeStringToFile(target, content, StandardCharsets.UTF_8);
        } catch (IOException ex) {
            throw new JsRuntimeException("error mounting file to: " + targetDirectory.getAbsolutePath(),
                    ex);
        }
    }

    public void removeFile(String relativePath) {
        File target = new File(currentDirectory.getAbsolutePath() + File.separator + relativePath);
        if (target.exists()) {
            if (!target.delete())
                target.deleteOnExit();
        }
    }

    public Value eval(String script) {
        requireStarted();
        String randomName = UUID.randomUUID().toString() + ".js";
        mountFile(script, randomName);
        Value value = evalFile(randomName);
        removeFile(randomName);
        return value;
    }

    private void requireStarted() {
        if (!isStarted())
            throw new IllegalStateException("runtime not started");
    }

    private void requireNotStarted() {
        if (isStarted())
            throw new IllegalStateException("runtime has already been started");
    }

}
