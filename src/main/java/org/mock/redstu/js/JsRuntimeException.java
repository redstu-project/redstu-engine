package org.mock.redstu.js;

public class JsRuntimeException extends RuntimeException {

    public JsRuntimeException(String message, Throwable cause){
        super(message,cause);
    }

}
