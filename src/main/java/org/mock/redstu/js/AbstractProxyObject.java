package org.mock.redstu.js;

import org.graalvm.polyglot.Value;
import org.graalvm.polyglot.proxy.ProxyArray;
import org.graalvm.polyglot.proxy.ProxyExecutable;
import org.graalvm.polyglot.proxy.ProxyObject;

import java.util.*;
import java.util.function.Supplier;

public abstract class AbstractProxyObject implements ProxyObject {

    private Map<String, Supplier<Object>> attributes = new HashMap<>();
    private Map<String, ProxyExecutable> methods = new HashMap<>();


    protected void readable(String name, Supplier<Object> supplier){
        this.attributes.put(name,supplier);
    }

    protected void executable(String name, ProxyExecutable executable){
        this.methods.put(name,executable);
    }


    @Override
    public Object getMember(String key) {
        Supplier<Object> valueSupplier = attributes.get(key);
        if(valueSupplier!=null)
            return valueSupplier.get();
        else return methods.get(key);
    }

    @Override
    public Object getMemberKeys() {
        List<Object> members = new ArrayList<>(attributes.keySet());
        members.addAll(attributes.keySet());
        return ProxyArray.fromList(members);
    }

    @Override
    public boolean hasMember(String key) {
        return attributes.containsKey(key) || methods.containsKey(key);
    }

    @Override
    public void putMember(String key, Value value) {
        throw new UnsupportedOperationException();
    }
}
