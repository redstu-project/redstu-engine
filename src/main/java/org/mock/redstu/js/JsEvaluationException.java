package org.mock.redstu.js;

public class JsEvaluationException extends JsRuntimeException {

    public JsEvaluationException(String message, Throwable cause) {
        super(message, cause);
    }

}
