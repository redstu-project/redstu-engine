package org.mock.redstu.js;

public final class RuntimeConstants {

    public static final String JS_LANGUAGE_ID = "js";
    public static final String OAS3_GLOBAL_ID = "$$oas3";
    public static final String STATE_MEMBER_ID = "$$state";
    public static final String LOGGER_MEMBER_ID = "$$logger";
    public static final String RESPOND_GLOBAL_ID = "$$respond";
    public static final String FILTER_GLOBAL_ID = "$$filter";

}
