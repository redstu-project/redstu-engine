package org.mock.redstu.signal;

public interface SignalConfig {

    String getName();
    boolean isParameterized();

}
