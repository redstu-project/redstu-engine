package org.mock.redstu.commons;

public class CorsConfig {

    private String allowOrigin = "*";
    private String allowMethods = "*";
    private String allowHeaders = "*";
    private String exposeHeaders = "*";
    private int maxAge = 86400;

    public String getAllowOrigin() {
        return allowOrigin;
    }

    public String getAllowMethods() {
        return allowMethods;
    }

    public String getAllowHeaders() {
        return allowHeaders;
    }

    public String getExposeHeaders() {
        return exposeHeaders;
    }

    public int getMaxAge() {
        return maxAge;
    }

    public void setAllowOrigin(String allowOrigin) {
        this.allowOrigin = allowOrigin;
    }

    public void setAllowMethods(String allowMethods) {
        this.allowMethods = allowMethods;
    }

    public void setAllowHeaders(String allowHeaders) {
        this.allowHeaders = allowHeaders;
    }

    public void setExposeHeaders(String exposeHeaders) {
        this.exposeHeaders = exposeHeaders;
    }

    public void setMaxAge(int maxAge) {
        this.maxAge = maxAge;
    }

}
