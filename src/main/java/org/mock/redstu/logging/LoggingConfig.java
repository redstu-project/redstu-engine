package org.mock.redstu.logging;

import org.apache.logging.log4j.Level;
import org.mock.redstu.config.SystemConfig;

public class LoggingConfig {

    public enum Mode {
        DEVELOPMENT("%highlight{%d{HH:mm:ss.SSS} %-5level %logger{36} (%X{serverName}) - %msg%n %throwable}"), LIVE("%highlight{%d{HH:mm:ss.SSS} %-5level (%X{serverName}) - %msg%n %throwable{none}}");
        private String pattern;

        Mode(String pattern){
            this.pattern = pattern;
        }

        public String getPattern() {
            return pattern;
        }
    }

    private Mode mode;
    private Level level = Level.INFO;

    public LoggingConfig(Mode mode, Level level){
        this.mode = mode;
        this.level = level;
    }

    public Mode getMode() {
        return mode;
    }

    public Level getLevel() {
        return level;
    }

    public static LoggingConfig fromSystemConfigs(){
        return new LoggingConfig(Mode.valueOf(SystemConfig.LOG_MODE.value()),
                Level.valueOf(SystemConfig.LOG_LEVEL.
                        value()));
    }

}
