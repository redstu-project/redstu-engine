package org.mock.redstu.logging;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.core.appender.ConsoleAppender;
import org.apache.logging.log4j.core.config.Configurator;
import org.apache.logging.log4j.core.config.builder.api.*;
import org.apache.logging.log4j.core.config.builder.impl.BuiltConfiguration;

public class Logging {

    public static void initialize(LoggingConfig config){
        ConfigurationBuilder<BuiltConfiguration> builder = ConfigurationBuilderFactory.newConfigurationBuilder();
        builder.setStatusLevel(config.getLevel());
        builder.setConfigurationName("DefaultLogger");

        AppenderComponentBuilder appenderBuilder = builder
                .newAppender("Console", "CONSOLE")
                .addAttribute("target", ConsoleAppender.Target.SYSTEM_OUT);

        appenderBuilder.add(builder.newLayout("PatternLayout")
                .addAttribute("pattern",config.getMode().getPattern()));
        RootLoggerComponentBuilder rootLogger = builder.newRootLogger(config.getLevel());
        rootLogger.add(builder.newAppenderRef("Console"));

        builder.add(appenderBuilder);
        builder.add(rootLogger);
        builder.add(silentConsoleLogger(builder,"org.xnio"));
        builder.add(silentConsoleLogger(builder,"io.undertow"));
        builder.add(silentConsoleLogger(builder,"org.jboss.threads"));
        Configurator.reconfigure(builder.build());
    }

    private static LoggerComponentBuilder silentConsoleLogger(ConfigurationBuilder<BuiltConfiguration> builder, String name){
        LoggerComponentBuilder logger = builder.newLogger(name, Level.ERROR);
        logger.add(builder.newAppenderRef("Console"));
        return logger;
    }

}
