package org.mock.redstu;

import org.apache.commons.io.FileUtils;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.mock.redstu.config.SystemConfig;
import org.mock.redstu.git.GitCloner;
import org.mock.redstu.git.GitCredentials;
import org.mock.redstu.logging.Logging;
import org.mock.redstu.logging.LoggingConfig;
import org.mock.redstu.proj.Deployment;
import org.mock.redstu.proj.Project;
import org.mock.redstu.proj.manifest.InvalidManifestException;
import org.mock.redstu.server.NetAddress;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;

public class Starter {

    private static final Logger LOGGER = LogManager.getLogger(Starter.class);

    private static final String LOGO = "\n" +
            "   ▄████████    ▄████████ ████████▄     ▄████████     ███     ███    █▄  \n" +
            "  ███    ███   ███    ███ ███   ▀███   ███    ███ ▀█████████▄ ███    ███ \n" +
            "  ███    ███   ███    █▀  ███    ███   ███    █▀     ▀███▀▀██ ███    ███ \n" +
            " ▄███▄▄▄▄██▀  ▄███▄▄▄     ███    ███   ███            ███   ▀ ███    ███ \n" +
            "▀▀███▀▀▀▀▀   ▀▀███▀▀▀     ███    ███ ▀███████████     ███     ███    ███ \n" +
            "▀███████████   ███    █▄  ███    ███          ███     ███     ███    ███ \n" +
            "  ███    ███   ███    ███ ███   ▄███    ▄█    ███     ███     ███    ███ \n" +
            "  ███    ███   ██████████ ████████▀   ▄████████▀     ▄████▀   ████████▀  \n" +
            "  ███    ███                                                             \n";

    public static void main(String[] args) {
        System.out.println(LOGO);
        System.setProperty("org.apache.logging.log4j.simplelog.StatusLogger.level","OFF");
        Logging.initialize(LoggingConfig.fromSystemConfigs());
        Starter starter = new Starter();
        if(starter.up())
            LOGGER.info("Redstu is up and Running :)");
    }

    private boolean up(){
        try {
            Deployment deployment = deployProject();
            deployment.setCurrent();
            return true;
        }catch (InvalidManifestException ex){
            LOGGER.error("Invalid project manifest: {}",String.join("\n",ex.
                    getProblems()));
            return false;
        }catch (RuntimeException ex){
            LOGGER.error(ex.getMessage(),
                    ex);
            return false;
        }
    }

    private NetAddress getMockAddr(){
        return new NetAddress(SystemConfig.MOCK_SERVER_HOSTNAME.value(),
                SystemConfig.MOCK_SERVER_PORT.asInt());
    }

    private NetAddress getCockpitAddr(){
        return new NetAddress(SystemConfig.COCKPIT_SERVER_HOSTNAME.value(),
                SystemConfig.COCKPIT_SERVER_PORT.asInt());
    }

    private Deployment deployProject(){
        Project project;
        if(SystemConfig.GIT_REPOSITORY_URL.isPresent()) {
            LOGGER.info("Git project");
            project = getGitProject();
        }else{
            LOGGER.info("Local Directory project");
            project = getLocalDirectoryProject();
        }
        Deployment deployment = project.createDeployment(getMockAddr(),getCockpitAddr());
        deployment.activate();
        return deployment;
    }

    private Project getGitProject(){
        String repositoryUrl = SystemConfig.GIT_REPOSITORY_URL.value();
        String branch = SystemConfig.GIT_BRANCH.value();
        GitCredentials credentials = null;
        LOGGER.info("Project Git repository: {}",repositoryUrl);
        LOGGER.info("Git branch: {}",branch);
        if(SystemConfig.GIT_PROJECT_PATH.isPresent())
            LOGGER.info("Git project path: {}",SystemConfig.GIT_PROJECT_PATH.value());
        if(SystemConfig.GIT_USERNAME.isPresent()&&SystemConfig.GIT_PASSWORD.isPresent()){
            credentials = new GitCredentials(SystemConfig.GIT_USERNAME.value(),SystemConfig.GIT_PASSWORD.value());
        }
        try {
            File cloneTarget = Files.createTempDirectory("redstu-git").toFile();
            cloneTarget.deleteOnExit();
            new GitCloner(repositoryUrl, credentials).checkout(branch,
                    cloneTarget);
            if (SystemConfig.GIT_PROJECT_PATH.isPresent())
                return new Project(new File(cloneTarget.getAbsolutePath() + File.separator + SystemConfig.GIT_PROJECT_PATH.
                        value()));
            else return new Project(cloneTarget);
        }catch (IOException ex){
            throw new RuntimeException("error creating temporary directory to clone repository",
                    ex);
        }
    }

    private Project getLocalDirectoryProject(){
        File projectDirectory = new File("".equals(SystemConfig.PROJECT_DIRECTORY.value()) ? new File("").getAbsolutePath() :
                SystemConfig.PROJECT_DIRECTORY.value());
        LOGGER.info(projectDirectory.getAbsolutePath());
        if(!projectDirectory.isDirectory()||!projectDirectory.exists()){
            throw new RuntimeException("Project directory does not exists: "+projectDirectory.getAbsolutePath());
        }
        LOGGER.debug("Copying project to temp directory");
        Path tempDirectoryPath = null;
        try {
            tempDirectoryPath = Files.createTempDirectory("redstu_");
        } catch (IOException e) {
            throw new RuntimeException("Error creating temporary directory for project");
        }
        File tempDirectory = tempDirectoryPath.toFile();
        try {
            FileUtils.copyDirectory(projectDirectory,tempDirectory);
        } catch (IOException ex) {
            throw new RuntimeException("Error copying project source to temp directory: "+tempDirectory.getAbsolutePath(),ex);
        }
        LOGGER.debug("Project copied successfully");
        LOGGER.debug("New project directory: "+tempDirectory.getAbsolutePath());
        projectDirectory = tempDirectory;
        return new Project(projectDirectory);
    }

}
