FROM ubuntu:20.10 as GraalDownloader
RUN apt-get update && apt-get --assume-yes install wget
#COPY graalvm-ce-java11-linux-amd64-21.0.0.2.tar.gz /etc/download/graalvm-ce-java11-linux-amd64-21.0.0.2.tar.gz
RUN wget https://github.com/graalvm/graalvm-ce-builds/releases/download/vm-21.0.0.2/graalvm-ce-java11-linux-amd64-21.0.0.2.tar.gz -P /etc/download
RUN mkdir /usr/graalvm && tar -xzf /etc/download/graalvm-ce-java11-linux-amd64-21.0.0.2.tar.gz -C /usr/graalvm

FROM ubuntu:20.10
COPY --from=GraalDownloader  /usr/graalvm /usr/graalvm
ENV PATH=/usr/graalvm/graalvm-ce-java11-21.0.0.2/bin/:/usr/graalvm/graalvm-ce-java11-21.0.0.2/languages/js/bin/:$PATH
ENV JAVA_HOME=/usr/graalvm/graalvm-ce-java11-21.0.0.2
ENV REDSTU_HOME=/etc/redstu
ENV PROJECT_DIR=/var/project
ENV PROJECT_DIRECTORY_MODIFY=true
RUN mkdir $REDSTU_HOME
RUN ls -la $JAVA_HOME
COPY target/libs $REDSTU_HOME/libs
COPY target/redstu.jar $REDSTU_HOME/redstu.jar
EXPOSE 8081
WORKDIR $REDSTU_HOME
ENTRYPOINT ["java","-jar","./redstu.jar"]