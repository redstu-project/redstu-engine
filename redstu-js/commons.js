/**
 * Represents an error
 */
class RuntimeException {

    /**
     * @hideconstructor
     */
    constructor() {
    }

    /**
     * Gets the error message
     * @returns {String}
     */
    getMessage(){

    }

    /**
     * Prints the error stack trace to the standard output
     */
    printStackTrace(){

    }

}

/**
 * A type safe enumeration for byte orders.
 */
class ByteOrder {

    /**
     * @hideconstructor
     */
    constructor() {
    }

    /**
     * Constant denoting big-endian byte order.
     * @type {ByteOrder}
     */
    static BIG_ENDIAN = new ByteOrder();

    /**
     * Constant denoting big-endian byte order.
     * @type {ByteOrder}
     */
    static LITTLE_ENDIAN = new ByteOrder();



}


/**
 * Represents a configured repetition
 */
class Repetition {

    /**
     * @hideconstructor
     */
    constructor() {
    }

    /**
     * Requests the interruption of the Repetition
     */
    requestInterruption(){

    }
}

/**
 * Provides API to configure repetition
 */
class Repeat {

    /**
     * @hideconstructor
     */
    constructor() {
    }

    /**
     * Configures a function to be executed repeatedly
     * @param {Number} time - time amount
     * @param {TimeUnit} unit - time unit
     * @param {function(Repetition)} callback - function to be executed repeatedly
     */
    static every(time,unit, callback){

    }

}


/**
 * Provides API to configure delays
 */
class Delay {


    /**
     * @hideconstructor
     */
    constructor() {
    }

    /**
     * Blocks the execution for a specific amount of time
     * @param {Number} time - time amount
     * @param {TimeUnit} unit - time unit
     */
    static until(time, unit){

    }

}

/**
 * Provides API for scheduling
 */
class Postpone {

    /**
     * @hideconstructor
     */
    constructor() {
    }

    /**
     * Postpones the execution of a function by  specific amount of time
     * @param {Number} time - time amount
     * @param {TimeUnit} unit - time unit
     * @param {function} func - the function being postponed
     */
    static by(time, unit, func){

    }

}


/**
 * A file reference
 */
class File {

    /**
     * Returns the name of the file or directory denoted by this abstract pathname. This is just the last name in the pathname's name sequence. If the pathname's name sequence is empty, then the empty string is returned.
     * @returns {String} The name of the file or directory denoted by this abstract pathname, or the empty string if this pathname's name sequence is empty
     */
    getName(){

    }

    /**
     * Returns the absolute pathname string of this abstract pathname
     * If this abstract pathname is already absolute, then the pathname string is simply returned as if by the getPath() method. If this abstract pathname is the empty abstract pathname then the pathname string of the current user directory, which is named by the system property user.dir, is returned. Otherwise this pathname is resolved in a system-dependent way. On UNIX systems, a relative pathname is made absolute by resolving it against the current user directory. On Microsoft Windows systems, a relative pathname is made absolute by resolving it against the current directory of the drive named by the pathname, if any; if not, it is resolved against the current user directory.
     * @returns {String} The absolute pathname string denoting the same file or directory as this abstract pathname
     */
    getAbsolutePath(){

    }

    /**
     * Tests whether the file or directory denoted by this abstract pathname exists.
     * @returns {boolean} true if and only if the file or directory denoted by this abstract pathname exists; false otherwise
     */
    exists(){

    }

    /**
     * Returns the length of the file denoted by this abstract pathname. The return value is unspecified if this pathname denotes a directory.
     * Where it is required to distinguish an I/O exception from the case that 0L is returned, or where several attributes of the same file are required at the same time, then the Files.readAttributes method may be used.
     * @returns {number} The length, in bytes, of the file denoted by this abstract pathname, or 0L if the file does not exist. Some operating systems may return 0L for pathnames denoting system-dependent entities such as devices or pipes.
     */
    length(){

    }

    /**
     * Deletes the file or directory denoted by this abstract pathname. If this pathname denotes a directory, then the directory must be empty in order to be deleted.
     * @returns {boolean} true if and only if the file or directory is successfully deleted; false otherwise
     */
    delete(){

    }

}

/**
 * Represents binary content
 * @interface
 */
class BinaryContent {


    /**
     * Saves the binary content to a temporary file
     * @returns {Promise<File,RuntimeException>} a promise of a File
     */
    saveAsTempFile(){

    }

    /**
     * The size of the binary content in bytes
     * @type {number}
     */
    size = 10;

    /**
     * Gets a Array containing the binary data
     * @returns {Int8Array} an Array containing the binary data
     */
    toArray(){

    }

    /**
     * Tuns the binary data into a hex encoded string
     * @returns {String} hex encoded binary data
     */
    hexEncode(){

    }

    /**
     * Tuns the binary data into a base64 encoded string
     * @returns {String} base64 encoded binary data
     */
    base64Encode(){

    }

}





export { File, BinaryContent,  RuntimeException, ByteOrder, Repeat, Delay, Postpone };