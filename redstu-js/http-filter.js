
import { HttpReq, HttpResp, } from "./http-req";

/**
 * Represents the chain of filters to be executed
 * @interface
 */
class FilterChain {

    /**
     * Proceed to the next filter in the chain
     */
    proceed(){

    }

}


class Filter {
    /**
     * @hideconstructor
     */
    constructor() {
    }

    /**
     * Adds a global Http Requests filter
     * @param {function(HttpReq,HttpResp,FilterChain)} filter - the request filter function
     */
    static all(filter){

    }

    /**
     * Configures an Http Requests filter for specific HTTP verbs and or path
     * @param {?string[]} verbs - http verbs to filter
     * @param {?string} path - path to filter
     * @param {function(HttpReq,HttpResp,FilterChain)} filter - the request filter function
     */
    static only(verbs, path, filter){

    }


}

export { Filter };