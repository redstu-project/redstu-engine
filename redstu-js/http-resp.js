/**
 * Represents an Http Response
 * @interface
 */
class HttpResp {

    /**
     * @hideconstructor
     */
    constructor() {
    }

    /**
     * Sets a response Header
     * @param {string} name - the name of header
     * @param {Array<string>|string} value - header values
     */
    setHeader(name,value){

    }

    /**
     * Sets the response status code
     * @param {int} code - the status code
     */
    setStatus(code){

    }

    /**
     * Sets the response content type
     * @param {string} type - the content type to ser
     */
    setContentType(type){

    }

    /**
     * Sets a response cookie
     * @param {{name: string, value: string, httpOnly: boolean=, domain: string=, path:string=, secured:boolean=, sameSite: string=, maxAge: number=, expires: Date=, comment: string= }} cookie - the cookie to be set
     */
    setCookie(cookie){

    }


    /**
     * Sends response from bytes. If no content type is set, application/octet-stream will be set automatically. If no status code is set, 200 will be set automatically.
     * @param bytes {Int8Array} byte array
     */
    sendBytes(bytes){

    }

    /**
     * Sends a serialized object. Content-type should be set prior calling this method. Only JSON and XML Content types are supported. If no Content-type is set, then JSON will be automatically set.
     * @param {Object | Object<string,Object>} obj - the object to be sent
     */
    sendObject(obj){

    }

    /**
     * Sends a serialized array of object. Content type will be set to JSON automatically
     * @param {Object[]} objects - the array object to be sent
     */
    sendArray(objects){

    }

    /**
     * Sends a text response. If no content type is set, text/plain will be set automatically. If no status code is set, 200 will be set automatically.
     * @param {string} text - the text content
     * @param {string} [charset] - the text charset
     */
    sendText(text, charset){

    }

    /**
     * Sends a response without a body. If no status code is set, 204 will be set automatically.
     */
    send(){

    }


}


export {HttpResp};