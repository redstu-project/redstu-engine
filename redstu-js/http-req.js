
import { HttpCookie, HttpParam } from './http-base';
import { BinaryContent} from './commons';


/**
 * Represents an HTTP Header value
 * @interface
 */
class Header {

    /**
     * Gets the header string value, command separated
     * @type {string}
     */
    value = undefined;

    /**
     * Checks whether the header value contains a token
     * @param token {string} - the token to check if the header contains or not
     * @returns {boolean} true if the token is contained in the header value, false otherwise
     */
    contains(token) {
    }


    /**
     * Gets an array containing all the header values
     * @type {Array<string>} array of header values
     */
    array = undefined;

    /**
     * Checks whether the Header value is multi-value
     * @type {boolean} true if the header value has multiple values, false otherwise.
     */
    multiValue = false;

}


/**
 * Represents a form data entry
 * @interface
 */
class FormEntry {

    /**
     * The form entry name
     * @readonly
     * @type {string} the form-data entry name
     */
    name = "";


    /**
     * Single value
     * @readonly
     * @type {FormValue} the form entry single value. If the entry has multiple values the first one will be returned.
     */
    value;


    /**
     * Tells whether the entry is multi-value
     * @type {Boolean} true if the entry is multi-value, false otherwise
     */
    multiValue;


    /**
     * Array of values
     * @readonly
     * @type {Array<FormValue>}
     */
    array;
    
}

/**
 * Represents the value of a form data entry
 * @interface
 */
class FormValue {

    /**
     * @hideconstructor
     */
    constructor() {
    }


    /**
     * A map of Headers of the form Value
     * @readonly
     * @type {Object.<String,Header>}
     */
    headers = {};

    /**
     * Tells whether the value is a file upload or not
     * @readonly
     * @type {boolean} true if the value is a file upload, false otherwise
     */
    fileUpload = false;

    /**
     * The value content type (based on Header)
     * @readonly
     * @type {?string} returns the content type if set on header, otherwise returns null.
     */
    contentType = "";

    /**
     * The value charset (based on Header)
     * @readonly
     * @type {?string} returns the content charset if set, otherwise returns null.
     */
    charset = "";


    /**
     * The file upload filename
     * @readonly
     * @type {?string} returns null if the form value doesn't correspond a file upload
     */
    filename = "";

    /**
     * Exposes the form value as binary
     * @returns {?BinaryContent} the form value content as binary content
     */
    binary(){
    }


    /**
     * Exposes the form value as string
     * @returns {?string} the form value content as string
     */
    string() {

    }

    /**
     * Form value parsed to number
     * @returns {Number} the parsed number or 0 if the value is not a valid number
     */
    number(){

    }

    /**
     * Form value parsed to boolean
     * @returns {Boolean} the parsed boolean or false if the value is not a valid boolean
     */
    boolean(){

    }


    /**
     * Parse the content into an object. Content must be in either XML or JSON format.
     * @returns {Promise<Object>} a promise of an object representing the content
     */
    parseObject(){

    }

    /**
     * Parse the content into an array of objects. Content must be in JSON format.
     * @returns {Promise<Object[]>} a promise of an array of objects representing the content
     */
    parseArray(){

    }


}


/**
 * Represents a HTTP Form data
 * @interface
 */
class HttpFormData {

    /**
     * @hideconstructor
     */
    constructor() {
    }


    /**
     * The form entries
     * @readonly
     * @type {Object.<string,FormEntry>}
     */
    entries = {};

}



/**
 * Represents an HTTP Request Body
 * @interface
 */
class HttpReqBody {

    /**
     * @hideconstructor
     */
    constructor() {
    }

    /**
     * Gets the Request content type
     * @readonly
     * @type {string}
     */
    contentType = undefined;

    /**
     * Gets the request body binary content
     * @returns {Promise<BinaryContent>} a promise of binary content
     */
    binary(){

    }


    /**
     * Gets the request body text content
     * @returns {Promise<string>} a promise of a string
     */
    text(){

    }

    /**
     * Parse the request body as an object. Only supported for XML and JSON
     * @returns {Promise<String,Object>} a promise of an object
     */
    object(){

    }

    /**
     * Parse the request body as an array of objects. Only supported for JSON
     * @returns {Promise<Object[]>} a promise of objects array
     */
    array(){

    }

    /**
     * Gets the request body Form data
     * @returns {Promise<Object<String,FormEntry>>} a promise of form data map
     */
    form(){

    }

}


/**
 * Represents an Http Request
 */
class HttpReq {

    /**
     * @hideconstructor
     */
    constructor() {
    }

    /**
     * The Request HTTP verb
     * @readonly
     * @type {string}
     */
    method = "GET";


    /**
     * The Request encoding charset
     * @readonly
     * @type {string}
     */
    charset = "UTF-8";


    /**
     * The url pattern matched
     * @readonly
     * @type {string}
     */
    urlPattern = "Unknown";


    /**
     * The full request URL
     * @readonly
     * @type {string}
     */
    url = "http://localhost:8080";


    /**
     * The HTTP request path
     * @readonly
     * @type {string}
     */
    path = "/path";


    /**
     * The HTTP request query string
     * @readonly
     * @type {string}
     */
    queryString = "abc=lorem";

    /**
     * The client address
     * @readonly
     * @type {string}
     */
     clientAddr;

    /**
     * HTTP headers map
     * @readonly
     * @type {Object.<string, Header>}
     */
    headers = {};

    /**
     * Request query params map
     * @readonly
     * @type {Object.<string, HttpParam>}
     */
    queryParams = {};

    /**
     * Request path params map
     * @readonly
     * @type {Object.<string, HttpParam>}
     */
    pathParams = {};

    /**
     * Request cookies map
     * @readonly
     * @type {Object.<string, HttpCookie>}
     */
    cookies = {};


    /**
     * The HTTP request body
     * @readonly
     * @type {HttpReqBody}
     */
    body = undefined;
    
}

export { HttpReqBody, HttpReq};
