import { HttpParam } from './http-base';
/**
 * Represents an event stream client session
 * @interface
 */
class EventSession {

    /**
     * @hideconstructor
     */
    constructor() {
    }

    /**
     * Gets the event session Id
     * @readonly
     * @type {string}
     */
    id = undefined;

    /**
     * Emits an event
     * @param {{data:Object, id: string=, name: string=}} event - the event to be broadcasted
     */
    emit(event){

    }

    /**
     * Gets the connection request path params
     * @readonly
     * @type {Object<String,HttpParam>}
     */
    pathParams = undefined;

    /**
     * Gets the connection query params
     * @readonly
     * @type {Object<String,HttpParam>}
     */
    queryParams = undefined;

    /**
     * Checks whether a session is closed
     * @readonly
     * @type {boolean}
     */
    closed;

    /**
     * Adds a listener for the session closure
     * @param {function(EventSession)} closeCallback - the event handler function
     */
    onClose(closeCallback){

    }

    /**
     * Closes the server-sent event session
     */
    close(){

    }

}

/**
 * Provides API to tap into all active event streams.
 */
class EventStream {

    /**
     * @hideconstructor
     */
    constructor() {
    }

    /**
     * Gets the context of a configured event stream
     * @param {string} path - the event stream path
     * @returns {EventContext}
     */
    static context(path){

    }

}

/**
 * Represents an active event stream
 * @interface
 */
class EventContext {

    /**
     * @hideconstructor
     */
    constructor() {
    }

    /**
     * Broadcasts an event to all active sessions
     * @param {{data:Object, id: string=, name: string=}} event - the event to be broadcasted
     *
     */
    broadcast(event){

    }

    /**
     * Gets all active event sessions
     * @readonly
     * @type {Array<EventSession>}
     */
    sessions = []

}

export { EventSession, EventContext, EventStream };