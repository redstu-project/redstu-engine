/**
 * Object to access and manipulate the in-memory state
 * @type {Object<String,Object>}
 */
let state;

export {state};