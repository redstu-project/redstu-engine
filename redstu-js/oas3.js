
import {HttpReq} from "./http-req";
import {HttpResp} from "./http-resp";


/**
 * Represents an OAS3 Request validation error
 * @interface
 */
class ValidationError {

    /**
     * The request part that contains the invalid input
     * @type {string}
     * @readonly
     */
    part = ReqPart.Body;

    /**
     * The key/name of the input that was deemed invalid
     * @type {string}
     * @readonly
     */
    key = "example";

    /**
     * The message that describes the validation error
     * @type {string}
     * @readonly
     */
    message = "Lorem Ipsum";


}

/**
 * Represent an OAS3 Request validation result
 * @interface
 */
class ValidationResult {

    /**
     * Gets the list of validation errors. Will be empty if there are no validation errors
     * @type {ValidationError[]}
     * @readonly
     */
    errors = [];


    /**
     * Checks if a specific input passed the OAS3 Constraints validation. This function doesn't perform the validation, it simply queries on the validation result data.
     * @param {string} key - the key/name of the input
     * @param {string} repPart - the part of the HTTP Request that holds the input. Possible values: Header, Path, Body, Query, Cookie, MediaType.
     * @param {string=} constraint - the OAS3 constraint to verify. Possible values: minimum, maximum, minLength, maxLength, multipleOf, nullable, pattern, format, type, minItems, maxItems, uniqueItems, required, additionalProperties
     * @returns {boolean} true if the the input is valid, false otherwise
     */
    isValid(key, repPart, constraint){

    }

    /**
     * Generates a response message that describes the validation errors
     * @param {HttpReq} httpReq - the request object to read the accepted content-types from.
     * @param {HttpResp} httpResp - the response object to write to
     */
    sendErrors(httpReq, httpResp){

    }

    /**
     * Prints the validation errors to the console
     */
    printErrors(){

    }

}


/**
 * Represents an OAS3 API Operation definition
 * @interface
 */
class APIOperation {

    /**
     * Validates an HTTP Request against the API Operation
     * @param {HttpReq} httpReq - the HTTP Request to be validated
     * @returns {Promise<ValidationResult>}
     */
    validate(httpReq){

    }

    /**
     * Responds an Http Request with an example defined on the API Operation
     * @param {number} status - the HTTP Response status code
     * @param {string} name - the name of the example
     * @param {HttpReq} httpReq - the HTTP Request to be responded
     * @param {HttpResp} httpResp - the HTTP Response to write to
     */
    sendExample(status, name, httpReq,  httpResp){

    }
}

/**
 * Represents an OAS3 API Definition
 * @interface
 */
class APIDefinition {

    /**
     * A map of the defined API operations, mapped by Id
     * @type {Object<String,APIOperation>}
     * @readonly
     */
    operations = {};

}

/**
 * Open API Specification version 3 support
 */
class OAS3 {

    /**
     * @hideconstructor
     */
    constructor() {
    }

    /**
     * Loads an API specification file
     * @param {string} file - the path to the file to be loaded. Can be local file or URL.
     * @returns {APIDefinition}
     */
    static spec(file){

    }

}

export { OAS3 };