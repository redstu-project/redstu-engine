/**
 * Object is used to log messages that give information regarding the behavior or outcome of the mock.
 */
class Logger {

    /**
     * Emits an information log
     * @param {string|Object} message - the information message to be logged
     */
    info(message){

    }

    /**
     * Emits an error log
     * @param {string|Object} message - the error message to be logged
     */
    error(message){

    }

}

/**
 * Global object used to access the mock logger.
 * @type {Logger}
 */
let log = new Logger();

export { log };