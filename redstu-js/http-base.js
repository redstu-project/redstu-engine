/**
 * HTTP Media type mime constants
 */
class MediaTypes {


    /**
     * @hideconstructor
     */
    constructor() {
    }

    /**
     * JSON Media type
     * @type {string}
     */
    static APPLICATION_JSON = "application/json";

    /**
     * XML Media type
     * @type {string}
     */
    static APPLICATION_XML = "application/xml";

    /**
     * Binary content media type
     * @type {string}
     */
    static APPLICATION_OCTET_STREAM = "application/octet-stream";

    /**
     * Plan text media type
     * @type {string}
     */
    static TEXT_PLAIN = "text/plain";

    /**
     * HTML Media type
     * @type {string}
     */
    static TEXT_HTML = "text/html";

    /**
     * Server sent event media type
     * @type {string}
     */
    static TEXT_EVENT_STREAM = "text/event-stream";


}


/**
 * Represents an Http Cookie
 * @interface
 */
class HttpCookie {

    /**
     * @hideconstructor
     */
    constructor() {
    }

    /**
     * Gets the name of the Cookie
     * @readonly
     * @type {String}
     */
    name = undefined;

    /**
     * Gets the cookie value
     * @readonly
     * @type {String}
     */
    value = undefined;

    /**
     * Returns the Datetime on which the Cookies is set to expire
     * @readonly
     * @type {?Date}
     */
    expires = undefined;

    /**
     * Checks whether cookie is secured
     * @readonly
     * @type {Boolean}
     */
    secured = false;

    /**
     * Checks whether cookie is restricted to Http only
     * @readonly
     * @type {Boolean}
     */
    httpOnly = undefined;

    /**
     * Gets the cookie domain
     * @readonly
     * @type {?String}
     */
    domain = undefined;

    /**
     * Gets the cookie path
     * @readonly
     * @type {?String}
     */
    path = undefined;

    /**
     * Gets the cookie Same Side mode
     * @readonly
     * @type {?String}
     */
    sameSite = undefined;

    /**
     * Gets the number of seconds set for the Cookie to expire
     * @readonly
     * @type {?int}
     */
    maxAge = undefined;

    /**
     * Gets the cookie comment
     * @readonly
     * @type {?String}
     */
    comment = undefined;

}

/**
 * Represents an HTTP Request param
 */
class HttpParam {

    /**
     * Gets the param value in it's raw string format
     * @returns {string} the raw param value
     */
    string(){

    }

    /**
     * Parses the param value as boolean.
     * @returns {number} The parsed number value or 0 if the provided value is not a valid number
     */
    number(){

    }

    /**
     * Parses the param value to boolean.
     * @returns {boolean} The parsed boolean value or false if the provided value is not a valid boolean
     */
    boolean(){

    }

}

export {MediaTypes, HttpCookie, HttpParam};

