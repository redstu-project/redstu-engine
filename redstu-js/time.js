/**
 * A TimeUnit represents time durations at a given unit of granularity and provides utility methods to convert across units, and to perform timing and delay operations in these units. A TimeUnit does not maintain time information, but only helps organize and use time representations that may be maintained separately across various contexts. A nanosecond is defined as one thousandth of a microsecond, a microsecond as one thousandth of a millisecond, a millisecond as one thousandth of a second, a minute as sixty seconds, an hour as sixty minutes, and a day as twenty four hours.
 * A TimeUnit is mainly used to inform time-based methods how a given timing parameter should be interpreted.
 */
class TimeUnit {

    /**
     * @hideconstructor
     */
    constructor() {
    }

    /**
     * Nanoseconds time unit
     * @type {TimeUnit}
     */
    static NANOSECONDS =  new TimeUnit();

    /**
     * Microseconds time unit
     * @type {TimeUnit}
     */
    static MICROSECONDS = new TimeUnit();

    /**
     * Milliseconds time unit
     * @type {TimeUnit}
     */
    static MILLISECONDS = new TimeUnit();

    /**
     * Seconds time unit
     * @type {TimeUnit}
     */
    static SECONDS = new TimeUnit();

    /**
     * Minutes time unit
     * @type {TimeUnit}
     */
    static MINUTES = new TimeUnit();

    /**
     * Hours time unit
     * @type {TimeUnit}
     */
    static HOURS = new TimeUnit();

    /**
     * Days time unit
     * @type {TimeUnit}
     */
    static DAYS = new TimeUnit();

}

export {TimeUnit };