
import {HttpReq} from "./http-req";
import {HttpResp} from "./http-resp";
import {EventSession} from "./sse";

/**
 * Provides API to configure server handlers
 */
class Respond {

    /**
     * @hideconstructor
     */
    constructor() {

    }

    /**
     * Configures a reaction for HTTP GET Requests
     * @param {String} path - the path to listen for GET Requests
     * @param {function(HttpReq,HttpResp)} handler - The function to handle the Requests
     */
    static GET(path, handler){

    }

    /**
     * Configures a reaction for HTTP POST Requests
     * @param {String} path - the path to listen for POST Requests
     * @param {function(HttpReq,HttpResp)} handler - The function to handle the Requests
     */
    static POST(path, handler){

    }

    /**
     * Configures a reaction for HTTP PUT Requests
     * @param {String} path - the path to listen for PUT Requests
     * @param {function(HttpReq,HttpResp)} handler - The function to handle the Requests
     */
    static PUT(path, handler){

    }

    /**
     * Configures a reaction for HTTP PATCH Requests
     * @param {String} path - the path to listen for PATCH Requests
     * @param {function(HttpReq,HttpResp)} handler - The function to handle the Requests
     */
    static PATCH(path, handler){

    }

    /**
     * Configures a reaction for HTTP DELETE Requests
     * @param {String} path - the path to listen for DELETE Requests
     * @param {function(HttpReq,HttpResp)} handler - The function to handle the Requests
     */
    static DELETE(path, handler){

    }

    /**
     * Configures a reaction for HTTP HEAD Requests
     * @param {String} path - the path to listen for HEAD Requests
     * @param {function(HttpReq,HttpResp)} handler - The function to handle the Requests
     */
    static HEAD(path, handler){

    }

    /**
     * Configures a server-sent events handler
     * @param {string} path - the path to listen for server-sent events connections
     * @param {function(EventSession)} handler - The function to handle the sessions
     */
    static SSE(path, handler){

    }

    /**
     * Configures a signal handler
     * @param {{name: string, description: string, parameterized: boolean}} signal - the signal to respond
     * @param {function(Object)} handler - the signal handler function
     */
    static signal(signal, handler){

    }

}

export {Respond}