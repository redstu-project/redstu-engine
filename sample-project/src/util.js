
class Counter {

    constructor(initialValue) {
        this.value = initialValue;
    }

    increment(){
        return ++this.value;
    }

}

export {Counter};