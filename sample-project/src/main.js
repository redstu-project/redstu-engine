import {state, Respond, Filter, OAS3} from '@redstu/redstu-js';
import {Counter} from './util.js';

//Filter all requests
Filter.all((req, resp, chain) => {
    console.log("Example global filter");
    chain.proceed();
});

//Filter /people/0
Filter.only(["GET"], "/people/0", (req, resp, chain) => {
    resp.sendText("Seriously bro?");
});

let personIdCounter = new Counter(state.counters.personId);
//List people
Respond.GET("/people", (req, resp) => {
    resp.sendObject(state["people"]);
});

//Add new person
Respond.POST("/people", async (req, resp) => {
    let object = await req.body.object();
    let person = {name: object.name, email: object.email};
    person.id = personIdCounter.increment();
    state["people"].push(person);
    resp.sendObject(person);
});

//Get person by Id
Respond.GET("/people/{id}", async (req, resp) => {
    let personId = req.pathParams["id"].number();
    let filtered = state["people"].filter(it => it.id === personId);
    if (filtered.length)
        resp.sendObject(filtered[0]);
    else {
        resp.setStatus(404);
        resp.send();
    }
});

//----signals----

//Delete everything
Respond.signal({name: "ClearDataset", description: "Clears the dataset", parameterized: false}, (param) => {
    state["people"] = [];
});

//Delete person by Id
Respond.signal({
    name: "DeletePersonById",
    description: "Deletes a person record matching By Id",
    parameterized: true
}, (param) => {
    if (param.id) {
        let personId = param.id;
        let people = state["people"];
        state["people"] = people.filter(it => it.id !== personId);
    }
});

//----oas3----

Respond.PUT("/houses/{id}/rooms/{number}/content/{item}", async (req, resp) => {
    const specification = OAS3.spec("house.yaml");
    specification.operations['insertItemInRoom'].validate(req).then(result => {
        specification.operations['insertItemInRoom'].sendExample(200, "default", req, resp);
    }, err => {
        console.log(err);
    });

});
